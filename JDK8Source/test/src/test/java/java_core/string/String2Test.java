package java_core.string;

import java.util.*;

public class String2Test {

    public static void main(String[] args) {
//        System.out.println(lengthOfLongestSubstring5_3("aabcas"));
        System.out.println(new Date().getTime());
        System.out.println(System.currentTimeMillis());
    }

    //-----------------------------------------------------------------------------------------------------------------
    /*443. 压缩字符串 https://leetcode-cn.com/problems/string-compression/
    给定一组字符，使用原地算法将其压缩。
    压缩后的长度必须始终小于或等于原数组长度。
    数组的每个元素应该是长度为1 的字符（不是 int 整数类型）。
    在完成原地修改输入数组后，返回数组的新长度。
    进阶：
    你能否仅使用O(1) 空间解决问题？
    示例 1：
    输入：
            ["a","a","b","b","c","c","c"]
    输出：
    返回6，输入数组的前6个字符应该是：["a","2","b","2","c","3"]*/
    public int compress_443(char[] chars) {
        return 1;
    }

    public int compress2_443(char[] chars) {
        int anchor = 0, write = 0;
        for (int read = 0; read < chars.length; read++) {
            if (read + 1 == chars.length || chars[read + 1] != chars[read]) {
                chars[write++] = chars[anchor];
                if (read > anchor) {
                    for (char c : ("" + (read - anchor + 1)).toCharArray()) {
                        chars[write++] = c;
                    }
                }
                anchor = read + 1;
            }
        }
        return write;
    }

    public int compress3_443(char[] chars) {
        int top = 0;//栈顶索引
        int ctop = 0;//字符栈顶索引
        int count = 1;
        //chars[ctop] = chars[0];       //入栈一个初始元素(其实不需要这一步)
        for (int i = 1; i < chars.length; i++) {
            if (chars[ctop] != chars[i]) {//比较字符栈顶与遍历到的字符
                top++;
                chars[top] = chars[i]; //不同，入栈
                ctop = top;            //记录新的字符栈顶
                count = 1;
            } else {
                count++;                //相同，计数+1
                if (count > 1) {        //count大于1，需要让count入栈
                    top = ctop;         //先让栈顶回到字符栈顶
                    for (char c : String.valueOf(count).toCharArray()) {
                        top++;
                        chars[top] = c; //将count分成字符入栈
                    }
                }
            }
        }
        return top + 1; //top+1就是栈的size
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*1221. 分割平衡字符串 https://leetcode-cn.com/problems/split-a-string-in-balanced-strings/
    在一个「平衡字符串」中，'L' 和 'R' 字符的数量是相同的。
    给出一个平衡字符串 s，请你将它分割成尽可能多的平衡字符串。
    返回可以通过分割得到的平衡字符串的最大数量。
    输入：s = "RLRRLLRLRL"
    输出：4
    解释：s 可以分割为 "RL", "RRLL", "RL", "RL", 每个子字符串中都包含相同数量的 'L' 和 'R'。*/
    public int balancedStringSplit_1221(String s) {
        return 0;
    }

    public int balancedStringSplit2_1221(String s) {
        Stack<Character> stack = new Stack<Character>();
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (stack.isEmpty() || c == stack.peek()) {
                stack.push(c);
            } else {
                stack.pop();
            }
            if (stack.isEmpty()) {
                count++;
            }
        }
        return count;
    }

    public int balancedStringSplit3_1221(String s) {
        int stack = 0;
        int cnt = 0;
        for (char c : s.toCharArray()) {
            if (c == 'R') {
                stack--;
            } else {
                stack++;
            }
            if (stack == 0) {
                cnt++;
            }
        }
        return cnt;
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*541. 反转字符串 II https://leetcode-cn.com/problems/reverse-string-ii/
    给定一个字符串 s 和一个整数 k，你需要对从字符串开头算起的每隔 2k 个字符的前 k 个字符进行反转。
    如果剩余字符少于 k 个，则将剩余字符全部反转。
    如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。
    示例:
    输入: s = "abcdefg", k = 2
    输出: "bacdfeg"*/
    public String reverseStr_541(String s, int k) {
        return null;
    }

    public String reverseStr2_541(String s, int k) {
        char[] a = s.toCharArray();
        for (int start = 0; start < a.length; start += 2 * k) {
            int i = start, j = Math.min(start + k - 1, a.length - 1);
            while (i < j) {
                char tmp = a[i];
                a[i++] = a[j];
                a[j--] = tmp;
            }
        }
        return new String(a);
    }

    public String reverseStr3_541(String s, int k) {
        int i = 0;
        char[] ch = s.toCharArray();
        while (i < ch.length) {
            if (i % (2 * k) == 0) {
                if (i + k - 1 < ch.length) {
                    for (int j = 0; j < k / 2; j++) {
                        char mid = ch[i + j];
                        ch[i + j] = ch[i + k - 1 - j];
                        ch[i + k - 1 - j] = mid;
                    }
                } else {
                    for (int j = 0; j < (ch.length - i) / 2; j++) {
                        char mid = ch[i + j];
                        ch[i + j] = ch[ch.length - 1 - j];
                        ch[ch.length - 1 - j] = mid;
                    }
                }
            }
            i += k;
        }
        return new String(ch);
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*917. 仅仅反转字母 https://leetcode-cn.com/problems/reverse-only-letters/
    给定一个字符串 S，返回 “反转后的” 字符串，其中不是字母的字符都保留在原地，而所有字母的位置发生反转。
    示例 1：
    输入："ab-cd"
    输出："dc-ba"
    示例 2：
    输入："a-bC-dEf-ghIj"
    输出："j-Ih-gfE-dCba"
    示例 3：
    输入："Test1ng-Leet=code-Q!"
    输出："Qedo1ct-eeLg=ntse-T!"
*/
    //双指针,遇到字母就反转
    private String reverseOnlyLetters_917(String S) {
        return null;
    }

    private String reverseOnlyLetters2_917(String S) {
        Stack<Character> letters = new Stack();
        for (char c : S.toCharArray())
            if (Character.isLetter(c))
                letters.push(c);

        StringBuilder ans = new StringBuilder();
        for (char c : S.toCharArray()) {
            if (Character.isLetter(c))
                ans.append(letters.pop());
            else
                ans.append(c);
        }

        return ans.toString();
    }

    private String reverseOnlyLetters3_917(String S) {
        StringBuilder ans = new StringBuilder();
        int j = S.length() - 1;
        for (int i = 0; i < S.length(); ++i) {
            if (Character.isLetter(S.charAt(i))) {
                while (!Character.isLetter(S.charAt(j)))
                    j--;
                ans.append(S.charAt(j--));
            } else {
                ans.append(S.charAt(i));
            }
        }

        return ans.toString();
    }


    private String reverseOnlyLetters4_917(String S) {
        char[] arr = S.toCharArray();
        int len = arr.length, l = -1, r = len;
        if (len < 2) return S;
        while (l++ < r--) {
            while (l < r && !valid(arr[l])) l++;
            while (l < r && !valid(arr[r])) r--;
            if (l < r) {
                arr[l] ^= arr[r];
                arr[r] ^= arr[l];
                arr[l] ^= arr[r];
            }
        }
        return new String(arr);
    }

    private boolean valid(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*459. 重复的子字符串 https://leetcode-cn.com/problems/repeated-substring-pattern/
    给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。给定的字符串只含有小写英文字母，并且长度不超过10000。
    示例 1:
    输入: "abab"
    输出: True
    解释: 可由子字符串 "ab" 重复两次构成。*/
    public boolean repeatedSubstringPattern1_459(String s) {
        for (int i = 1; i <= s.length() / 2; i++) {
            if (s.length() % i == 0) {

            }
        }


        return false;
    }


    public boolean repeatedSubstringPattern2_459(String s) {

        String str = s + s;
        return str.substring(1, str.length() - 1).contains(s);
    }

    public boolean repeatedSubstringPattern3_459(String s) {
        for (int i = 1; i < s.length(); i++) {
            if (s.length() % i == 0) {
                if (judge(s.substring(0, i), s)) return true;
            }
        }
        return false;
    }

    public boolean judge(String sub, String S) {
        int len = sub.length();
        for (int i = 0; i < S.length(); i++) {
            if (S.charAt(i) != sub.charAt(i % len)) return false;
        }
        return true;
    }

    //-----------------------------------------------------------------------------------------------------------------

    /*20. 有效的括号
    给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。
    有效字符串需满足：
    左括号必须用相同类型的右括号闭合。
    左括号必须以正确的顺序闭合。
    注意空字符串可被认为是有效字符串。*/
    public boolean isValid_20(String s) {
        return false;
    }

    class Solution {
        private HashMap<Character, Character> mappings;

        public Solution() {
            this.mappings = new HashMap<Character, Character>();
            this.mappings.put(')', '(');
            this.mappings.put('}', '{');
            this.mappings.put(']', '[');
        }

        public boolean isValid(String s) {
            Stack<Character> stack = new Stack<Character>();
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (this.mappings.containsKey(c)) {
                    char topElement = stack.empty() ? '#' : stack.pop();
                    if (topElement != this.mappings.get(c)) {
                        return false;
                    }
                } else {
                    stack.push(c);
                }
            }
            return stack.isEmpty();
        }
    }

    public boolean isValid2_20(String s) {
        char[] charArray = new char[s.length() + 1];

        int p = 1;

        for (char c : s.toCharArray()) {
            if (c == '(' || c == '{' || c == '[') {
                charArray[p++] = c;
            } else {
                p--;
                if (c == ')' && charArray[p] != '(') {
                    return false;
                }
                if (c == '}' && charArray[p] != '{') {
                    return false;
                }
                if (c == ']' && charArray[p] != '[') {
                    return false;
                }
            }
        }
        return p == 1; // 如果左括号还有剩余 括号没有一一对应，属于无效情况
    }

    //-----------------------------------------------------------------------------------------------------------------
    /*415. 字符串相加 https://leetcode-cn.com/problems/add-strings/solution/add-strings-shuang-zhi-zhen-fa-by-jyd/
    给定两个字符串形式的非负整数 num1 和num2 ，计算它们的和。
    注意：
    num1 和num2 的长度都小于 5100.
    num1 和num2 都只包含数字 0-9.
    num1 和num2 都不包含任何前导零。
    你不能使用任何內建 BigInteger 库， 也不能直接将输入的字符串转换为整数形式。*/
    //me ok
    private static String addStrings1_415(String num1, String num2) {
        //从后往前遍历
        int i = num1.length() - 1;
        int j = num2.length() - 1;
        int carry = 0;//进位
        StringBuilder stringBuilder = new StringBuilder();
        while (i >= 0 || j >= 0) {
            //获取num1和num2当前位数的值，如果某个已经遍历完成，则按0处理
            int one = i >= 0 ? num1.charAt(i) - '0' : 0;
            int two = j >= 0 ? num2.charAt(j) - '0' : 0;
            int result = one + two + carry;//要加上进位的值

            carry = result / 10;//计算出进位值

            stringBuilder.append(result % 10);
            i--;
            j--;
        }
        //如果进位还大于0，说明要再加一位
        if (carry > 0) {
            stringBuilder.append(1);
        }

        //反转
        return stringBuilder.reverse().toString();
    }

    private String addStrings2_415(String num1, String num2) {
        StringBuilder res = new StringBuilder("");
        int i = num1.length() - 1, j = num2.length() - 1, carry = 0;
        while (i >= 0 || j >= 0) {
            int n1 = i >= 0 ? num1.charAt(i) - '0' : 0;
            int n2 = j >= 0 ? num2.charAt(j) - '0' : 0;
            int tmp = n1 + n2 + carry;
            carry = tmp / 10;
            res.append(tmp % 10);
            i--;
            j--;
        }
        if (carry == 1) res.append(1);
        return res.reverse().toString();
    }

    private String addStrings3_415(String num1, String num2) {
        // 用于拼接
        StringBuilder sb = new StringBuilder();
        // 进位处理
        int carry = 0;
        // 从低位往高位计算 最后反转
        for (int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0; i--, j--) {
            // 判断是否越界 越界就是0 不越界的话就取那个数
            int x = i >= 0 ? num1.charAt(i) - '0' : 0;
            int y = j >= 0 ? num2.charAt(j) - '0' : 0;
            // 求和
            int sum = x + y + carry;
            // 拼接
            sb.append(sum % 10);
            // 进位标志
            carry = sum / 10;

        }
        if (carry == 1) {
            sb.append("1");
        }
        // 反转
        return sb.reverse().toString();
    }

    //-----------------------------------------------------------------------------------------------------------------

    /*    面试题 01.02. 判定是否互为字符重排 https://leetcode-cn.com/problems/check-permutation-lcci/
        给定两个字符串 s1 和 s2，请编写一个程序，确定其中一个字符串的字符重新排列后，能否变成另一个字符串。
        示例 1：
        输入: s1 = "abc", s2 = "bca"
        输出: true
        示例 2：
        输入: s1 = "abc", s2 = "bad"
        输出: false*/

    //利用map ok
    private static boolean CheckPermutation1_0102(String s1, String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        Map<Character, Integer> map = new HashMap<>(s1.length());
        for (int i = 0; i < s1.length(); i++) {
            if (map.get(s1.charAt(i)) == null) {

                map.put(s1.charAt(i), 1);
            } else {
                map.put(s1.charAt(i), map.get(s1.charAt(i)) + 1);

            }
        }
        for (int i = 0; i < s2.length(); i++) {
            if (map.get(s2.charAt(i)) == null) {
                return false;
            } else {
                map.put(s2.charAt(i), map.get(s2.charAt(i)) - 1);
                if (map.get(s2.charAt(i)) == 0) {
                    map.remove(s2.charAt(i));
                }
            }


        }
        return true;
    }

    //bitmap 的思想
    private static boolean CheckPermutation2_0102(String s1, String s2) {
        int l1 = s1.length(), l2 = s2.length();
        if (l1 != l2)
            return false;
        int[] index = new int[128];
        for (int i = 0; i < l1; i++) {
            index[s1.charAt(i)]++;
            index[s2.charAt(i)]--;
        }
        for (int i = 0; i < 128; i++) {
            if (index[i] != 0)
                return false;
        }
        return true;
    }

    //排序再比较 够暴力
    private boolean CheckPermutation3_0102(String s1, String s2) {
        char[] c1 = s1.toCharArray();
        Arrays.sort(c1);
        char[] c2 = s2.toCharArray();
        Arrays.sort(c2);
        return new String(c1).equals(new String(c2));
    }


    //-----------------------------------------------------------------------------------------------------------------

    /*3. 无重复字符的最长子串 https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/
    给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。*/

    //me 超时
    private static int lengthOfLongestSubstring1_3(String s) {
        if (s.equals("")) {
            return 0;
        }
        for (int i = s.length(); i >= 1; i--) {
            for (int j = 0; j <= s.length() - i; j++) {

                if (!isRepeat(s.substring(j, i + j))) {
                    return i;
                }
            }
        }
        return 1;
    }

    //判断一个字符串是否包含重复字符
    private static boolean isRepeat(String s) {
        int[] index = new int[128];
        for (int i = 0; i < s.length(); i++) {
            index[s.charAt(i)] = index[s.charAt(i)] + 1;
            if (index[s.charAt(i)] > 1) {
                return true;
            }
        }
        return false;
    }

    //滑动窗口+bitmap方式 ok
    private static int lengthOfLongestSubstring5_3(String s) {
        if (s.equals("")) {
            return 0;
        }
        int end = 0;
        int maxSize = 0;
        int[] index = new int[128];//bitmap判断是否重复
        for (int start = 0; start < s.length(); start++) {
            //重复，移动左指针，并将之前保存的字符删除
            if (start != 0) {
                index[s.charAt(start - 1)] = 0;
            }
            //不重复，右移end
            while (end < s.length() && index[s.charAt(end)] == 0) {
                index[s.charAt(end)]++;
                end++;
            }
            maxSize = Math.max(maxSize, end - start);

        }
        return maxSize;
    }

    private int lengthOfLongestSubstring2_3(String s) {
        // 哈希集合，记录每个字符是否出现过
        Set<Character> occ = new HashSet<Character>();
        int n = s.length();
        // 右指针，初始值为 -1，相当于我们在字符串的左边界的左侧，还没有开始移动
        int rk = -1, ans = 0;
        for (int i = 0; i < n; ++i) {
            if (i != 0) {
                // 左指针向右移动一格，移除一个字符
                occ.remove(s.charAt(i - 1));
            }
            while (rk + 1 < n && !occ.contains(s.charAt(rk + 1))) {
                // 不断地移动右指针
                occ.add(s.charAt(rk + 1));
                ++rk;
            }
            // 第 i 到 rk 个字符是一个极长的无重复字符子串
            ans = Math.max(ans, rk - i + 1);
        }
        return ans;
    }

    //采取滑动方式定位子序列
    //若出现重复字符，则以当前重复节点开始，可能出现的后续无重复子序列，必然在当前重复节点的前重复节点之后
    //因而可以采取滑动方式
    private int lengthOfLongestSubstring3_3(String s) {
        char[] chars = s.toCharArray();
        HashMap<Character, Integer> map = new HashMap();
        //最大长度
        int maxLength = 0;
        //存取当前定位的无重复节点子序列长度
        int tempLength = 0;
        //当前定位的无重复节点子序列的起始index
        int preIndex = 0;
        for (int i = 0; i < chars.length; i++) {
            //当前置所有节点不包含当前节点
            // 或包含当前节点，但是该节点在前定位的无重复节点子序列起始index之前
            //继续滑动，当前定位的无重复节点子序列长度+1
            if (!map.containsKey(chars[i]) || (map.containsKey(chars[i]) && map.get(chars[i]) < preIndex)) {
                tempLength++;
            } else {
                //获取与当前节点重复的前置节点下标
                int temp = map.get(chars[i]);
                //比较当前子序列长度和之前存储的最大长度
                if (tempLength > maxLength) {
                    maxLength = tempLength;
                }
                //修改下个子序列的起始下标
                preIndex = temp + 1;
                tempLength = i - preIndex + 1;
            }
            map.put(chars[i], i);
        }
        return maxLength > tempLength ? maxLength : tempLength;
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*409. 最长回文串 https://leetcode-cn.com/problems/longest-palindrome/solution/zui-chang-hui-wen-chuan-by-leetcode-solution/
    给定一个包含大写字母和小写字母的字符串，找到通过这些字母构造成的最长的回文串。
    在构造过程中，请注意区分大小写。比如 "Aa" 不能当做一个回文字符串。
    注意:
    假设字符串的长度不会超过 1010。
    输入:
            "abccccdd"
    输出:
            7
    解释:
    我们可以构造的最长的回文串是"dccaccd", 它的长度是 7。*/

    //判断能构成的最大的回文串,主要就是找出成对字符的数量
    //me ok:放入map中
    private static int longestPalindrome1_409(String s) {
        if (s.equals("")) {
            return 0;
        }
        Map<Character, Integer> map = new HashMap<>(s.length());
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        int count = 0;
        for (Character character : map.keySet()) {
            if (map.get(character) >= 2) {
                count += 2 * (map.get(character) / 2);
            }
        }
        if (count == s.length()) {
            return count;
        } else {
            return count + 1;
        }
    }

    //me 放入数组中
    private static int longestPalindrome4_409(String s){
        int[] index = new int[128];
        for (int i = 0; i < s.length(); i++) {
            index[s.charAt(i)]++;
        }
        int count = 0;
        for (int i = 0; i < index.length; i++) {
            count += index[i] / 2 * 2;
        }
        if (count == s.length()) {
            return count;
        } else {
            return count + 1;
        }
    }



    private int longestPalindrome3_409(String s) {
        int[] cnt = new int[58];
        for (char c : s.toCharArray()) {
            cnt[c - 'A'] += 1;
        }
        int ans = 0;
        for (int x : cnt) {
            // 字符出现的次数最多用偶数次。
            ans += x - (x & 1);
        }
        // 如果最终的长度小于原字符串的长度，说明里面某个字符出现了奇数次，那么那个字符可以放在回文串的中间，所以额外再加一。
        return ans < s.length() ? ans + 1 : ans;
    }


    //-----------------------------------------------------------------------------------------------------------------
    /*5. 最长回文子串 https://leetcode-cn.com/problems/longest-palindromic-substring/
    给定一个字符串 s，找到 s 中最长的回文子串。你可以假设 s 的最大长度为 1000。
    示例 1：
    输入: "babad"
    输出: "bab"
    注意: "aba" 也是一个有效答案。
    示例 2：
    输入: "cbbd"
    输出: "bb"*/
    public String longestPalindrome1_5(String s) {

        return null;
    }

    //-----------------------------------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------------------------------


    //-----------------------------------------------------------------------------------------------------------------


}
