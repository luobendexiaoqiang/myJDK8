package java_core.two_array;

import java.util.Arrays;

/* 移动零 https://leetcode-cn.com/problems/move-zeroes/solution/dong-hua-yan-shi-283yi-dong-ling-by-wang_ni_ma/
给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
示例:
输入: [0,1,0,3,12]
输出: [1,3,12,0,0]
说明:
必须在原数组上操作，不能拷贝额外的数组。
尽量减少操作次数。*/
public class YiDongLing {

    public static void main(String[] args) {
        int[] arr = {0,1,0,3,12};
        mymoveZeroes(arr);
    }

    public static void mymoveZeroes(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        } else {
            int index = 0;
            int count = 0;//0的个数
            for (int i = 0; i < nums.length; i++) {
                if (nums[i] != 0) {

                    nums[index++] = nums[i];
                } else {
                    count++;
                }
            }
            for (int i = nums.length-1; i > nums.length-1-count; i--) {
                nums[i] = 0;
            }

        }
        System.out.println(Arrays.toString(nums));

    }

    class Solution {
        public void moveZeroes(int[] nums) {
            if (nums == null) {
                return;
            }
            //两个指针i和j
            int j = 0;
            for (int i = 0; i < nums.length; i++) {
                //当前元素!=0，就把其交换到左边，等于0的交换到右边
                if (nums[i] != 0) {
                    int tmp = nums[i];
                    nums[i] = nums[j];
                    nums[j++] = tmp;
                }
            }
        }
    }


}
