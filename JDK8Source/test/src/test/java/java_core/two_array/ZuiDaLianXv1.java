package java_core.two_array;

/*485. 最大连续1的个数 https://leetcode-cn.com/problems/max-consecutive-ones/solution/zui-da-lian-xu-1de-ge-shu-by-leetcode/
给定一个二进制数组， 计算其中最大连续1的个数。
示例 1:
输入: [1,1,0,1,1,1]
输出: 3
解释: 开头的两位和最后的三位都是连续1，所以最大连续1的个数是 3.
注意：
输入的数组只包含 0 和1。
输入数组的长度是正整数，且不超过 10,000。*/
public class ZuiDaLianXv1 {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0};
        System.out.println(myFindMaxConsecutiveOnes(arr));

    }

    public static int myFindMaxConsecutiveOnes(int[] nums) {
        int count = 0;
        int maxCount = 0;
        if (nums == null || nums.length == 0) {
            return 0;
        } else {
            for (int num : nums) {
                if (num == 1) {
                    count++;
                    maxCount = Math.max(maxCount, count);
                } else {
                    count = 0;

                }
            }
        }
        return maxCount;
    }

    public static int findMaxConsecutiveOnes2(int[] nums) {
        int count = 0;
        int maxCount = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1) {
                // Increment the count of 1's by one.
                count += 1;
            } else {
                // Find the maximum till now.
                maxCount = Math.max(maxCount, count);
                // Reset count of 1.
                count = 0;
            }
        }
        return Math.max(maxCount, count);
    }

    public static int findMaxConsecutiveOnes3(int[] nums) {
        int length = nums.length;
        int max = 0;
        int count = 0;

        // 遍历数组
        for (int i = 0; i < length; i++) {
            if (nums[i] == 1) {
                // 记录连续1的个数
                count++;
            } else {
                // 比较是否是当前最大个数
                if (count > max)
                    max = count;
                // 归零，寻找下一个连续序列
                count = 0;
            }
        }

        // 因为最后一次连续序列在循环中无法比较，所以在循环外进行比较
        return max > count ? max : count;
    }


}
