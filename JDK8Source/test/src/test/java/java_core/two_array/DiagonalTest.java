package java_core.two_array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/*498. 对角线遍历
给定一个含有 M x N 个元素的矩阵（M 行，N 列），
请以对角线遍历的顺序返回这个矩阵中的所有元素，对角线遍历如下图所示。
输入:
[
[ 1, 2, 3 ],
[ 4, 5, 6 ],
[ 7, 8, 9 ]
]
输出:  [1,2,4,7,5,3,6,8,9]
解释:
说明:
(0,0),(0,1),(1,0),(2,0),(1,1),(0,2),(1,2),(2,1),(2,2)
给定矩阵中的元素总数不会超过 100000 。*/
public class DiagonalTest {

/*    public static void main(String[] args) {
        int[][] arr = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9,10,11,12}
        };

        System.out.println(Arrays.toString(findDiagonalOrder2(arr)));


    }*/

    public int[] findDiagonalOrder1(int[][] matrix) {

        return null;

    }

    public static int[] findDiagonalOrder(int[][] matrix) {

        // Check for empty matrices
        if (matrix == null || matrix.length == 0) {
            return new int[0];
        }

        // Variables to track the size of the matrix
        int N = matrix.length;
        int M = matrix[0].length;

        // The two arrays as explained in the algorithm
        int[] result = new int[N * M];
        int k = 0;
        ArrayList<Integer> intermediate = new ArrayList<Integer>();

        // We have to go over all the elements in the first
        // row and the last column to cover all possible diagonals
        for (int d = 0; d < N + M - 1; d++) {

            // Clear the intermediate array every time we start
            // to process another diagonal
            intermediate.clear();

            // We need to figure out the "head" of this diagonal
            // The elements in the first row and the last column
            // are the respective heads.
            int r = d < M ? 0 : d - M + 1;
            int c = d < M ? d : M - 1;

            // Iterate until one of the indices goes out of scope
            // Take note of the index math to go down the diagonal
            while (r < N && c > -1) {

                intermediate.add(matrix[r][c]);
                ++r;
                --c;
            }

            // Reverse even numbered diagonals. The
            // article says we have to reverse odd
            // numbered articles but here, the numbering
            // is starting from 0 :P
            if (d % 2 == 0) {
                Collections.reverse(intermediate);
            }

            for (int i = 0; i < intermediate.size(); i++) {
                result[k++] = intermediate.get(i);
            }
        }
        return result;
    }


        public static int[] findDiagonalOrder2(int[][] matrix) {
            //考虑边界 []
            if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
                return new int[0];
            }
            //m行 n列
            int m = matrix.length;
            int n = matrix[0].length;
            //用来记录加入到结果数组的元素的下标
            int count = 0;
            //用来保存结果的数组 大小为矩阵的元素个数
            int[] result = new int[m * n];

            for (int i = 0; i < m + n - 1; i++) {
                //i为0或偶数时 沿对角线向上遍历
                if (i == 0 || i % 2 == 0) {
                    //发现规律x+y = i 然后找边界（x坐标超过主对角线时的情况）
                    int x = i < m ? i : m - 1;
                    int y = i - x;
                    //开始遍历 x坐标不断减 y坐标不断加 当x减到0 或者 y加到列n的值
                    while (x >= 0 && y < n) {
                        result[count] = matrix[x][y];
                        x--;
                        y++;
                        count++;
                    }
                } else {
                    //沿对角线向下遍历
                    int y = i < n ? i : n - 1;
                    int x = i - y;
                    //y坐标不断减 x坐标不断加 当y减到0 或者 x加到行m的值
                    while (y >= 0 && x < m) {
                        result[count] = matrix[x][y];
                        y--;
                        x++;
                        count++;
                    }
                }
            }
            return result;
        }
}


