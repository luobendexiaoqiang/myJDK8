package java_core.digui;

/**
 * 递归
 */
public class DiGuiTest {

    /*24. 两两交换链表中的节点 https://leetcode-cn.com/problems/swap-nodes-in-pairs/
    给定一个链表，两两交换其中相邻的节点，并返回交换后的链表。
    你不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。
    示例:
    给定 1->2->3->4, 你应该返回 2->1->4->3.*/
    public ListNode swapPairs1_24(ListNode head) {

        return null;
    }

    //ok
    public ListNode swapPairs2_24(ListNode head) {

        // If the list has no node or has only one node left.
        if ((head == null) || (head.next == null)) {
            return head;
        }

        ListNode firstNode = head;
        ListNode secondNode = head.next;

        firstNode.next = swapPairs2_24(secondNode.next);
        secondNode.next = firstNode;

        return secondNode;
    }

    //ok
    public ListNode swapPairs3_24(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode next = head.next;
        head.next = swapPairs3_24(next.next);
        next.next = head;
        return next;
    }
    //===============================================================================================


    /*206. 反转链表 https://leetcode-cn.com/problems/reverse-linked-list/
    反转一个单链表。
    示例:
    输入: 1->2->3->4->5->NULL
    输出: 5->4->3->2->1->NULL
    进阶:
    你可以迭代或递归地反转链表。你能否用两种方法解决这道题？*/
    public ListNode reverseList1_206(ListNode head) {
        return null;
    }

    public ListNode reverseList2_206(ListNode head) {
        if (head == null || head.next == null) return head;
        ListNode p = reverseList2_206(head.next);
        head.next.next = head;
        head.next = null;
        return p;
    }

    public ListNode reverseList3_206(ListNode head) {
        //递归终止条件是当前为空，或者下一个节点为空
        if (head == null || head.next == null) {
            return head;
        }
        //这里的cur就是最后一个节点
        ListNode cur = reverseList3_206(head.next);
        //这里请配合动画演示理解
        //如果链表是 1->2->3->4->5，那么此时的cur就是5
        //而head是4，head的下一个是5，下下一个是空
        //所以head.next.next 就是5->4
        head.next.next = head;
        //防止链表循环，需要将head.next设置为空
        head.next = null;
        //每层递归函数都返回cur，也就是最后一个节点
        return cur;
    }


    //===============================================================================================
    /*70. 爬楼梯 https://leetcode-cn.com/problems/climbing-stairs/
    假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
    每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
    注意：给定 n 是一个正整数。
    示例 1：
    输入： 2
    输出： 2
    解释： 有两种方法可以爬到楼顶。
    1.  1 阶 + 1 阶
    2.  2 阶
    示例 2：
    输入： 3
    输出： 3
    解释： 有三种方法可以爬到楼顶。
    1.  1 阶 + 1 阶 + 1 阶
    2.  1 阶 + 2 阶
    3.  2 阶 + 1 阶*/

    //斐波那契数列
    public int climbStairs1_70(int n) {
        return 0;
    }

    public int climbStairs2_70(int n) {
        int memo[] = new int[n + 1];
        return climb_Stairs(0, n, memo);
    }

    public int climb_Stairs(int i, int n, int memo[]) {
        if (i > n) {
            return 0;
        }
        if (i == n) {
            return 1;
        }
        if (memo[i] > 0) {
            return memo[i];
        }
        memo[i] = climb_Stairs(i + 1, n, memo) + climb_Stairs(i + 2, n, memo);
        return memo[i];
    }


    //===============================================================================================
    /*104. 二叉树的最大深度
    给定一个二叉树，找出其最大深度。
    二叉树的深度为根节点到最远叶子节点的最长路径上的节点数。
    说明: 叶子节点是指没有子节点的节点。
    示例：
    给定二叉树 [3,9,20,null,null,15,7]，
    3
    / \
    9  20
    /  \
    15   7
    返回它的最大深度 3 。*/
    public int maxDepth1_104(TreeNode root) {
        return 1;
    }


    public int maxDepth3_104(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int left_height = maxDepth3_104(root.left);
            int right_height = maxDepth3_104(root.right);
            return java.lang.Math.max(left_height, right_height) + 1;
        }
    }

    /**
     * 递归实现二叉树最大深度
     * 时间复杂度O(n)
     * 空间复杂度:线性表最差O(n)、二叉树完全平衡最好O(logn)
     *
     * @param root 根节点
     * @return 最大深度
     */
    private static int maxDepth2_107(TreeNode root) {
        //递归退出条件，到叶子节点
        if (root == null) {
            return 0;
        }
        //计算左子树最大深度
        int leftMaxDepth = maxDepth2_107(root.left);
        //计算右子树最大深度
        int rightMaxDepth = maxDepth2_107(root.right);
        //以某个节点为根节点的数的最大深度为max
        //max=max(leftMaxDepth,rightMaxDepth)+1
        return Math.max(leftMaxDepth, rightMaxDepth) + 1;
    }


    //===============================================================================================
    /*50. Pow(x, n)
    实现 pow(x, n) ，即计算 x 的 n 次幂函数。
    示例 1:
    输入: 2.00000, 10
    输出: 1024.00000
    示例 2:
    输入: 2.10000, 3
    输出: 9.26100
    示例 3:
    输入: 2.00000, -2
    输出: 0.25000
    解释: 2-2 = 1/22 = 1/4 = 0.25*/
    public double myPow1_50(double x, int n) {
        return 0;
    }


    public double myPow2_50(double x, int n) {
        long N = n;
        if (N < 0) {
            x = 1 / x;
            N = -N;
        }

        return fastPow(x, N);
    }
    private double fastPow(double x, long n) {
        if (n == 0) {
            return 1.0;
        }
        double half = fastPow(x, n / 2);
        if (n % 2 == 0) {
            return half * half;
        } else {
            return half * half * x;
        }
    }


    //===============================================================================================
    /*779.第K个语法符号 https://leetcode-cn.com/problems/k-th-symbol-in-grammar/
    在第一行我们写上一个 0。接下来的每一行，将前一行中的0替换为01，1替换为10。
    给定行数 N 和序数 K，返回第 N 行中第 K个字符。（K从1开始）
    例子:
    输入: N = 1, K = 1
    输出: 0
    输入: N = 2, K = 1
    输出: 0
    输入: N = 2, K = 2
    输出: 1
    输入: N = 4, K = 5
    输出: 1
    解释:
    第一行: 0
    第二行: 01
    第三行: 0110
    第四行: 01101001*/

    // 解题: https://leetcode-cn.com/explore/featured/card/recursion-i/260/conclusion/1232/
    public int kthGrammar1_779(int N, int K) {
        return 1;
    }

    //===============================================================================================


    static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
            next = null;
        }
    }

    static public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
