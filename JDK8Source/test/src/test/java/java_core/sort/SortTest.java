package java_core.sort;

import java.util.Arrays;

public class SortTest {
    //1. 冒泡排序

    public static void main(String[] args) {
        int[] arr = new int[150000];
        for (int i = 0; i < 150000; i++) {
            arr[i] = (int) (Math.random() * 150000);
        }

        System.out.println(Arrays.toString(arr));
        long start = System.currentTimeMillis();
        //选择排序
//        System.out.println(Arrays.toString(mySelectSort(arr)));
//        System.out.println(Arrays.toString(otherSelectionSort(arr)));
        //快速排序
        System.out.println(Arrays.toString(QuickSort(arr, 0, arr.length - 1)));
        System.out.println(System.currentTimeMillis() - start);
    }
    //2. 选择排序:选择最小的元素与当前元素交换

    private static int[] mySelectSort(int[] array) {
        if (array == null || array.length <= 1) {
            return array;
        } else {
            for (int i = 0; i < array.length - 1; i++) {
                int minIndex = i;
                int thisOne = array[i];
                int changeOne;
                for (int j = i + 1; j < array.length; j++) {
                    if (array[j] < thisOne) {
                        minIndex = j;
                        thisOne = array[j];

                    }
                }
                if (minIndex != i) {
                    changeOne = array[i];
                    array[i] = array[minIndex];
                    array[minIndex] = changeOne;
                }
            }
        }
        return array;
    }

    /**
     * 选择排序
     * @param array
     * @return
     */
    public static int[] otherSelectionSort(int[] array) {
        if (array.length == 0)
            return array;
        for (int i = 0; i < array.length; i++) {
            int minIndex = i;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[minIndex]) //找到最小的数
                    minIndex = j; //将最小数的索引保存
            }
            int temp = array[minIndex];
            array[minIndex] = array[i];
            array[i] = temp;
        }
        return array;
    }

    //3. 快速排序
    // 快速排序start-------------------------------------------------------------------------
/*
    快速排序 的基本思想：通过一趟排序将待排记录分隔成独立的两部分，其中一部分记录的关键字均比另一部分的关键字小，
则可分别对这两部分记录继续进行排序，以达到整个序列有序。

6.1 算法描述

快速排序使用分治法来把一个串（list）分为两个子串（sub-lists）。具体算法描述如下：
步骤1：从数列中挑出一个元素，称为 “基准”（pivot ）；
步骤2：重新排序数列，所有元素比基准值小的摆放在基准前面，所有元素比基准值大的摆在基准的后面（相同的数可以到任一边）。
在这个分区退出之后，该基准就处于数列的中间位置。这个称为分区（partition）操作；
步骤3：递归地（recursive）把小于基准值元素的子数列和大于基准值元素的子数列排序。
*/

    public static int[] QuickSort(int[] array, int start, int end) {
        if (array.length < 1 || start < 0 || end >= array.length || start > end) return null;
        int smallIndex = partition(array, start, end);
        if (smallIndex > start)
            QuickSort(array, start, smallIndex - 1);
        if (smallIndex < end)
            QuickSort(array, smallIndex + 1, end);
        return array;
    }

    /**
     * 快速排序算法——partition
     */
    public static int partition(int[] array, int start, int end) {
        //获取基准数
        int pivot = (int) (start + Math.random() * (end - start + 1));
        int smallIndex = start - 1;
        //将基准数的值与arr[end]交换
        swap(array, pivot, end);
        for (int i = start; i <= end; i++)
            if (array[i] <= array[end]) {
                smallIndex++;
                if (i > smallIndex)
                    swap(array, i, smallIndex);
            }
        return smallIndex;
    }

    /**
     * 交换数组内两个元素
     */
    public static void swap(int[] array, int i, int j) {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    // 快速排序end------------------------------------------------------------------
}
