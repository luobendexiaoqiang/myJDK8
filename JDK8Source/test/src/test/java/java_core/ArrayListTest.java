package java_core;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ArrayListTest {
    public static void main(String[] args) {
    }

    /**
     * ArrayList删除元素存在多个时,默认删除第一个
     */
    @Test
    public void test1() {
        List<String> stringList = new ArrayList<>();
        stringList.add("1");
        stringList.add("2");
        stringList.add("3");
        stringList.add("1");
        System.out.println(stringList.toString());
        stringList.remove("1");
        System.out.println(stringList.toString());
    }

    @Test
    public void test2() {
        List<String> stringList = new ArrayList<>();
        System.out.println(stringList.size());
        stringList.add("1");
        stringList.add(null);
        stringList.add(null);
        stringList.add(null);
        stringList.add(null);
        System.out.println(stringList.toString());
    }
    @Test
    public void test3() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.remove(1);
        list.remove(new Integer(1));
        System.out.println(list.toString());
    }
    @Test
    public void test4() {

    }
    @Test
    public void test5() {

    }
    @Test
    public void test6() {

    }

}
