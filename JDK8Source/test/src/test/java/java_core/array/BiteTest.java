package java_core.array;

import java.util.Arrays;

/*
二进制求和
给你两个二进制字符串，返回它们的和（用二进制表示）。
输入为 非空 字符串且只包含数字 1 和 0。
示例 1:
输入: a = "11", b = "1"
输出: "100"
示例 2:
输入: a = "1010", b = "1011"
输出: "10101"
提示：
每个字符串仅由字符 '0' 或 '1' 组成。
1 <= a.length, b.length <= 10^4
字符串如果不是 "0" ，就都不含前导零。
*/
public class BiteTest {

    public static String addBinary(String a, String b) {
        Integer integerA = Integer.valueOf(a);
        Integer integerB = Integer.valueOf(b);
        String str = integerB + integerA + "";
        char[] chars = str.toCharArray();
        int i = chars.length - 1;
        boolean f = false;
        while (i >= 0) {
            if ((chars[i] - 48) >= 2) {
                chars[i] = (char) ((chars[i]) % 2 + 48);
                if (i - 1 >= 0) {
                    chars[i - 1] = (char) (chars[i - 1] + 1);
                } else {
                    f = true;
                }
            }
            i--;
        }
        long[] arr = null;
        if (f) {
            arr = new long[chars.length + 1];
            arr[0] = 1;
            for (int j = 0; j < chars.length; j++) {
                arr[j + 1] = chars[j] - 48;

            }
        } else {
            arr = new long[chars.length];
            for (int j = 0; j < chars.length; j++) {
                arr[j] = chars[j] - 48;
            }
        }
        str = "";
        for (long j : arr) {
            str += j;
        }
        return str;
    }

    public String addBinary2(String a, String b) {
        StringBuilder ans = new StringBuilder();
        int ca = 0;
        for(int i = a.length() - 1, j = b.length() - 1;i >= 0 || j >= 0; i--, j--) {
            int sum = ca;
            sum += i >= 0 ? a.charAt(i) - '0' : 0;
            sum += j >= 0 ? b.charAt(j) - '0' : 0;
            ans.append(sum % 2);
            ca = sum / 2;
        }
        ans.append(ca == 1 ? ca : "");
        return ans.reverse().toString();
    }

    public String addBinary3(String a, String b) {
        int n = a.length(), m = b.length();
        if (n < m) return addBinary(b, a);
        int L = Math.max(n, m);

        StringBuilder sb = new StringBuilder();
        int carry = 0, j = m - 1;
        for(int i = L - 1; i > -1; --i) {
            if (a.charAt(i) == '1') ++carry;
            if (j > -1 && b.charAt(j--) == '1') ++carry;

            if (carry % 2 == 1) sb.append('1');
            else sb.append('0');

            carry /= 2;
        }
        if (carry == 1) sb.append('1');
        sb.reverse();

        return sb.toString();

    }

/*    public static void main(String[] args) {
        System.out.println(addBinary("1111111111", "1111111111"));
        System.out.println((int) ((3) % 2));
    }*/
}
