package java_core.array;

import java.util.Arrays;

/*  加一
给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
你可以假设除了整数 0 之外，这个整数不会以零开头。
示例 1:

输入: [1,2,3]
输出: [1,2,4]
解释: 输入数组表示数字 123。
示例 2:

输入: [4,3,2,1]
输出: [4,3,2,2]
解释: 输入数组表示数字 4321。*/
public class PlusOneTest {
    public static void main(String[] args) {
        int[] arr = {9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 7};
        System.out.println(Arrays.toString(addOne(arr)));
    }

    private static int[] addOne(int[] digits) {
        if (digits == null) {

            return digits;
        } else {
            int i = digits.length - 1;
            while (i >= 0) {
                if (digits[i] == 9) {
                    digits[i] = 0;
                    i--;
                } else {
                    digits[i] = digits[i] + 1;
                    break;
                }
            }
            if (digits[0] == 0) {
                int[] arr = new int[digits.length + 1];
                arr[0] = 1;
                return arr;
            }
            return digits;

        }

    }

    private int[] addOne(int[] arr, int index) {
        if (arr.length - 1 == index) {
            if (arr[index] == 9) {
                arr[index] = 0;
                if (index > 0) {

                    addOne(arr, index - 1);
                } else {
                    return new int[]{1, 0};
                }
            } else {
                arr[index] = arr[index] + 1;
            }
        } else {

        }

        return null;
    }
}
