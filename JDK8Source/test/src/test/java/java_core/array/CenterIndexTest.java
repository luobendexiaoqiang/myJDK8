package java_core.array;

import java.util.Arrays;

/*给定一个整数类型的数组 nums，请编写一个能够返回数组“中心索引”的方法。
我们是这样定义数组中心索引的：数组中心索引的左侧所有元素相加的和等于右侧所有元素相加的和。
如果数组不存在中心索引，那么我们应该返回 -1。如果数组有多个中心索引，那么我们应该返回最靠近左边的那一个。
示例 1:
输入:
nums = [1, 7, 3, 6, 5, 6]
输出: 3

[-1,-1,-1,0,1,1]
输出: 0
[-1,-1,1,0,1,0]
输出:5
解释:
索引3 (nums[3] = 6) 的左侧数之和(1 + 7 + 3 = 11)，与右侧数之和(5 + 6 = 11)相等。
同时, 3 也是第一个符合要求的中心索引。*/
public class CenterIndexTest {
    public static void main(String[] args) {
        int[] nums = {-1, -1, 0, 1, 1, 0};
        System.out.println(findCenterIndex(nums));

    }

    private static int findCenterIndex(int[] nums) {
        if (nums == null) {
            return -1;
        } else {
            for (int i = 0; i < nums.length; i++) {
                // 处理[-1,-1,-1,0,1,1]和[-1,-1,1,0,1,0]的情况
                if ((i == 0 && sumArray(Arrays.copyOfRange(nums, i + 1, nums.length)) == 0) ||
                        (i == nums.length - 1 && sumArray(Arrays.copyOfRange(nums, 0, i)) == 0)) {
                    return i;
                }

                if (i >= 1 && i < nums.length - 1) {
                    if (sumArray(Arrays.copyOfRange(nums, 0, i)) == sumArray(Arrays.copyOfRange(nums, i + 1, nums.length))) {
                        return i;
                    }
                }
            }
            return -1;
        }
    }

    private static int sumArray(int[] arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return sum;
    }


}
