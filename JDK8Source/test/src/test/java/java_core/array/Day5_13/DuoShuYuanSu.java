package java_core.array.Day5_13;

import java.util.Arrays;
import java.util.Map;

/*169. 多数元素 https://leetcode-cn.com/problems/majority-element/
给定一个大小为 n 的数组，找到其中的多数元素。多数元素是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。
你可以假设数组是非空的，并且给定的数组总是存在多数元素。
示例 1:
输入: [3,2,3]
输出: 3
示例 2:
输入: [2,2,1,1,1,2,2]
输出: 2*/
public class DuoShuYuanSu {
    public static void main(String[] args) {
        DuoShuYuanSu duoShuYuanSu = new DuoShuYuanSu();
        duoShuYuanSu.c();

    }

    private void c() {
        b();

    }

    private void b() {
        a();
    }

    private void a() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            System.out.println(stackTraceElement.getClassName()+"--"+stackTraceElement.getMethodName());
        }
    }

    public int majorityElement(int[] nums) {
        Arrays.sort(nums);

        return nums[nums.length / 2];

    }

}
