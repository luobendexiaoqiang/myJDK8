package java_core.array.Day5_13;

import java.util.Arrays;

/*268. 缺失数字 https://leetcode-cn.com/problems/missing-number/
        给定一个包含 0, 1, 2, ..., n 中 n 个数的序列，找出 0 .. n 中没有出现在序列中的那个数。

        示例 1:

        输入: [3,0,1]
        输出: 2
        示例 2:

        输入: [9,6,4,2,3,5,7,0,1]
        输出: 8
        说明:
        你的算法应具有线性时间复杂度。你能否仅使用额外常数空间来实现?*/
public class QueShiShuZi {
    public static void main(String[] args) {
        int arr [] = {1,2};
        System.out.println(missingNumber(arr));

    }

    public static int missingNumber(int[] nums) {
        if (nums.length == 1) {
            return nums[0] == 1 ? 0 : 1;
        }
        Arrays.sort(nums);
        if (nums[0] == 1) {
            return 0;
        } else if (nums[nums.length - 1] != nums.length) {
            return nums.length;
        }
        int left = 0;
        int right = nums.length - 1;
        while (left <= right) {
            if (nums[left] != nums[left + 1] - 1) {
                return nums[left] + 1;
            }
            if (nums[right] != nums[right - 1] + 1) {
                return nums[right] - 1;
            }

            left++;
            right--;
        }
        return 0;
    }


}
