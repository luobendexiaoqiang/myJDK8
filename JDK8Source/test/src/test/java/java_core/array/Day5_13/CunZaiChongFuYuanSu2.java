package java_core.array.Day5_13;

/*219. 存在重复元素 II https://leetcode-cn.com/problems/contains-duplicate-ii/
给定一个整数数组和一个整数 k，判断数组中是否存在两个不同的索引 i 和 j，使得 nums [i] = nums [j]，并且 i 和 j 的差的 绝对值 至多为 k。
示例 1:
输入: nums = [1,2,3,1], k = 3
输出: true
示例 2:

输入: nums = [1,0,1,1], k = 1
输出: true
示例 3:

输入: nums = [1,2,3,1,2,3], k = 2
输出: false*/
public class CunZaiChongFuYuanSu2 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 9};
        System.out.println(containsNearbyDuplicate(arr, 3));
    }

    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        if (nums == null || nums.length < 2) {

            return false;
        } else {
            //扫到倒数第二个
            for (int i = 0; i < nums.length - 1; i++) {

                //防止越界
                for (int j = i + 1; j < Math.min(i + k + 1, nums.length); j++) {
                    if (nums[i] == nums[j]) {
                        return true;
                    }
                }
            }
            return false;

        }

    }

}
