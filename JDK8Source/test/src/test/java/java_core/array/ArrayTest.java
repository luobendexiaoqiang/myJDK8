package java_core.array;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.util.*;

public class ArrayTest {


    //448 找出所有数组中消失的数字 ok
    private static List<Integer> findDisappearedNumbers1_448(int[] nums) {
        //bitmap方式
        if (nums == null || nums.length == 0) {
            return new ArrayList<>();
        }
        List<Integer> list = new ArrayList<>();
        int[] arr = new int[nums.length + 1];
        for (int i = 0; i < nums.length; i++) {
            arr[nums[i]]++;
        }
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] == 0) {
                list.add(i);
            }
        }

        return list;
    }

    private List<Integer> findDisappearedNumbers(int[] nums) {
        HashMap<Integer, Boolean> hashTable = new HashMap<Integer, Boolean>();
        for (int i = 0; i < nums.length; i++) {
            hashTable.put(nums[i], true);
        }
        List<Integer> result = new LinkedList<Integer>();
        for (int i = 1; i <= nums.length; i++) {
            if (!hashTable.containsKey(i)) {
                result.add(i);
            }
        }
        return result;
    }

    //53-II 0~~n-1 中缺失的数字，排序数组，长度为n-1, 数字值在0~~n-1之间 ok
    private static int findDisappearedNumbers1_53II(int[] nums) {
        if (nums.length == 1) {
            return nums[0] == 1 ? 0 : 1;
        }
        if (nums[0] != 0) {
            return 0;
        }
        if (nums[nums.length - 1] > nums[0]) {
            for (int i = 0; i < nums.length - 1; i++) {
                if (nums[i + 1] - nums[i] == 2) {
                    return nums[i + 1] - 1;
                }
            }
        } else {
            for (int i = 0; i < nums.length - 1; i++) {
                if (nums[i + 1] - nums[i] == -2) {
                    return nums[i + 1] + 1;
                }
            }
        }

        return nums.length;

    }

    //1299 将每个元素替换成右侧最大元素 ok
    private static int[] aiugdagdka(int[] arr) {
        int max = Integer.MIN_VALUE;
        int max2;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (i == arr.length - 1) {
                max = arr[arr.length - 1];
                arr[arr.length - 1] = -1;
            } else {
                max2 = max;
                max = Math.max(arr[i], max2);
                arr[i] = max2;
            }
        }
        return arr;
    }


    //1374 生成每种字符都是奇数个的字符串 ok
    private String getSingalChar(int n) {
        StringBuilder stringBuilder = new StringBuilder();
        if (n % 2 == 0) {
            for (int i = 0; i < n - 1; i++) {
                stringBuilder.append('a');
            }
            return stringBuilder.append('b').toString();
        } else {
            for (int i = 0; i < n; i++) {
                stringBuilder.append('a');
            }
            return stringBuilder.toString();
        }

    }

    //面试题03 找出数组中重复的数字 ok
    private int iuadgda(int[] nums) {
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                return nums[i];
            }
        }
        return 0;
    }

    //905 按奇偶排序数组，基数全部在偶数后面 ok
    private int[] sortArrayByParity(int[] A) {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) {
                list1.add(A[i]);
            } else {
                list2.add(A[i]);
            }
        }
        list1.addAll(list2);
        for (int i = 0; i < list1.size(); i++) {
            A[i] = list1.get(i);
        }

        return A;
    }

    //922 按奇偶排序数组II A[i]和i都是基数或者偶数
    private static int[] sortArrayByParity2(int[] A) {
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) {
                list1.add(A[i]);
            } else {
                list2.add(A[i]);
            }
        }
        for (int i = 0; i < A.length; i++) {
            if (i % 2 == 0) {
                A[i] = list1.get(i / 2);
            } else {
                A[i] = list2.get(i / 2);

            }
        }

        return A;
    }

    //287 寻找重复数，不能改变原数组，只能使用O(1)空间，时间复杂度小于O(n`2)
    private static int gauidiuabd(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums.length; j++) {
                if (i != j && nums[i] == nums[j]) {
                    return nums[i];
                }
            }
        }
        return 0;
    }

    /*面试题 17.10. 主要元素 https://leetcode-cn.com/problems/find-majority-element-lcci/
    数组中占比超过一半的元素称之为主要元素。给定一个整数数组，找到它的主要元素。若没有，返回-1。
    示例 1：
    输入：[1,2,5,9,5,9,5,5,5]
    输出：5
    示例 2：
    输入：[3,2]
    输出：-1*/
    private int majorityElement(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                map.put(nums[i], map.get(nums[i]) + 1);
            } else {
                map.put(nums[i], 1);
            }
        }
        for (Integer integer : map.keySet()) {
            if (map.get(integer) * 2 > nums.length) {
                return integer;
            }
        }

        return -1;
    }

    /*面试题 16.17. 连续数列 https://leetcode-cn.com/problems/contiguous-sequence-lcci/
    给定一个整数数组，找出总和最大的连续数列，并返回总和。
    示例：
    输入： [-2,1,-3,4,-1,2,1,-5,4]
    输出： 6
    解释： 连续子数组 [4,-1,2,1] 的和最大，为 6。*/
    private int maxSubArray(int[] nums) {
        int sum = 0;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < nums.length; i++) {
            sum = nums[i];
            max = Math.max(sum, max);
            for (int j = i + 1; j < nums.length; j++) {
                sum += nums[j];
                max = Math.max(sum, max);
            }

        }
        return max;
    }

    /*面试题 08.03. 魔术索引 https://leetcode-cn.com/problems/magic-index-lcci/
    魔术索引。 在数组A[0...n-1]中，有所谓的魔术索引，满足条件A[i] = i。给定一个有序整数数组，
    编写一种方法找出魔术索引，若有的话，在数组A中找出一个魔术索引，如果没有，则返回-1。若有多个魔术索引，返回索引值最小的一个。
    示例1:
    输入：nums = [0, 2, 3, 4, 5]
    输出：0
    说明: 0下标的元素为0
    示例2:
    输入：nums = [1, 1, 1]
    输出：1*/
    private int findMagicIndex(int[] nums) {
        if (nums == null || nums.length == 0) {
            return -1;
        }

        for (int i = 0; i < nums.length; i++) {
            if (i == nums[i]) {
                return i;
            }
        }
        return -1;
    }

    /*面试题 01.06. 字符串压缩 https://leetcode-cn.com/problems/compress-string-lcci/
    字符串压缩。利用字符重复出现的次数，编写一种方法，实现基本的字符串压缩功能。比如，字符串aabcccccaaa会变为a2b1c5a3。
    若“压缩”后的字符串没有变短，则返回原先的字符串。你可以假设字符串中只包含大小写英文字母（a至z）。
    示例1:
    输入："aabcccccaaa"
    输出："a2b1c5a3"
    示例2:
    输入："abbccd"
    输出："abbccd"
    解释："abbccd"压缩后为"a1b2c2d1"，比原字符串长度更长。*/
    private static String compressString(String S) {
        if (S == null || S.length() == 0) {
            return "";
        }
        char now = S.charAt(0);
        char next;
        int count = 1;
        boolean isNeedToAddLast = true;
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < S.length() - 1; i++) {
            now = S.charAt(i);
            next = S.charAt(i + 1);
            if (now != next) {
                stringBuilder.append(now).append(count);
                if (i + 1 == S.length() - 1) {
                    stringBuilder.append(next).append(1);
                    isNeedToAddLast = false;
                }
                count = 1;
            } else {
                count++;
            }
        }
        if (isNeedToAddLast) {
            stringBuilder.append(now).append(count);
        }
        return stringBuilder.length() >= S.length() ? S : stringBuilder.toString();
    }

    /*1413. 逐步求和得到正数的最小值 https://leetcode-cn.com/problems/minimum-value-to-get-positive-step-by-step-sum/
    给你一个整数数组 nums 。你可以选定任意的 正数 startValue 作为初始值。
    你需要从左到右遍历 nums 数组，并将 startValue 依次累加上 nums 数组中的值。
    请你在确保累加和始终大于等于 1 的前提下，选出一个最小的 正数 作为 startValue 。
    示例 1：
    输入：nums = [-3,2,-3,4,2]
    输出：5
    解释：如果你选择 startValue = 4，在第三次累加时，和小于 1 。
    示例 2：
    输入：nums = [1,2]
    输出：1
    解释：最小的 startValue 需要是正数。
    示例 3：
    输入：nums = [1,-2,-3]
    输出：5*/
    private int minStartValue(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 1;
        }
        int min = Integer.MAX_VALUE;
        int sum = 0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            min = Math.min(sum, min);
        }
        return min < 0 ? 1 - min : 1;
    }

    /*1380. 矩阵中的幸运数 https://leetcode-cn.com/problems/lucky-numbers-in-a-matrix/
    给你一个 m * n 的矩阵，矩阵中的数字 各不相同 。请你按 任意 顺序返回矩阵中的所有幸运数。
    幸运数是指矩阵中满足同时下列两个条件的元素：
    在同一行的所有元素中最小,在同一列的所有元素中最大
    示例 1：
    输入：matrix = [[3,7,8],[9,11,13],[15,16,17]]
    输出：[15]
    解释：15 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。
    示例 2：
    输入：matrix = [[1,10,4,2],[9,3,8,7],[15,16,17,12]]
    输出：[12]
    解释：12 是唯一的幸运数，因为它是其所在行中的最小值，也是所在列中的最大值。*/
    public List<Integer> luckyNumbers(int[][] matrix) {
        return null;
    }

    /*1385. 两个数组间的距离值 https://leetcode-cn.com/problems/find-the-distance-value-between-two-arrays
    给你两个整数数组 arr1 ， arr2 和一个整数 d ，请你返回两个数组之间的 距离值 。
    「距离值」 定义为符合此描述的元素数目：对于元素 arr1[i] ，不存在任何元素 arr2[j] 满足 |arr1[i]-arr2[j]| <= d 。
    示例 1：
    输入：arr1 = [4,5,8], arr2 = [10,9,1,8], d = 2
    输出：2
    示例 2：
    输入：arr1 = [1,4,2,3], arr2 = [-4,-3,6,10,20,30], d = 3
    输出：2
    示例 3：
    输入：arr1 = [2,1,100,3], arr2 = [-5,-2,10,-3,7], d = 6
    输出：1*/
    private static int findTheDistanceValue(int[] arr1, int[] arr2, int d) {
        int count = 0;
        for (int i = 0; i < arr1.length; i++) {
            boolean flag = true;
            for (int j = 0; j < arr2.length; j++) {
                if (Math.abs(arr1[i] - arr2[j]) <= d) {
                    flag = false;
                }

            }
            if (flag) {
                count++;
            }
        }
        return count;
    }

    /*1346. 检查整数及其两倍数是否存在 https://leetcode-cn.com/problems/check-if-n-and-its-double-exist/
    给你一个整数数组 arr，请你检查是否存在两个整数 N 和 M，满足 N 是 M 的两倍（即，N = 2 * M）。
    更正式地，检查是否存在两个下标 i 和 j 满足：
    i != j
    0 <= i, j < arr.length
    arr[i] == 2 * arr[j]
    示例 1：
    输入：arr = [10,2,5,3]
    输出：true
    解释：N = 10 是 M = 5 的两倍，即 10 = 2 * 5 。
    [-2,0,10,-19,4,6,-8]
    */
    private static boolean checkIfExist(int[] arr) {
        if (arr == null || arr.length < 2) {
            return false;
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (j != i && (arr[i] * 2 == arr[j] || arr[i] == arr[j] * 2)) {

                    return true;
                }
            }
        }
        return false;
    }

    /*1304. 和为零的N个唯一整数 https://leetcode-cn.com/problems/find-n-unique-integers-sum-up-to-zero/
    给你一个整数 n，请你返回 任意 一个由 n 个 各不相同 的整数组成的数组，并且这 n 个数相加和为 0 。
    示例 1：
    输入：n = 5
    输出：[-7,-1,1,3,4]
    解释：这些数组也是正确的 [-5,-1,1,2,3]，[-3,-1,2,-2,4]。
    示例 2：
    输入：n = 3
    输出：[-1,0,1]*/
    private static int[] sumZero(int n) {
        if (n == 1) {
            return new int[]{0};
        }
        int[] arr = new int[n];
        for (int i = 1; i <= n / 2; i++) {
            arr[i - 1] = i;
            if (n % 2 == 0) {
                arr[arr.length - i] = -i;
            } else {
                arr[arr.length - 1 - i] = -i;
            }
        }
        if (n % 2 != 0) {
            arr[arr.length - 1] = 0;
        }
        return arr;
    }

    /*1232. 缀点成线 https://leetcode-cn.com/problems/check-if-it-is-a-straight-line/
    在一个 XY 坐标系中有一些点，我们用数组 coordinates 来分别记录它们的坐标，
    其中 coordinates[i] = [x, y] 表示横坐标为 x、纵坐标为 y 的点。
    请你来判断，这些点是否在该坐标系中属于同一条直线上，是则返回 true，否则请返回 false。*/
    public boolean checkStraightLine(int[][] coordinates) {
        return false;
    }


    /*1200. 最小绝对差 https://leetcode-cn.com/problems/minimum-absolute-difference/
    给你个整数数组 arr，其中每个元素都 不相同。
    请你找到所有具有最小绝对差的元素对，并且按升序的顺序返回。
    示例 1：
    输入：arr = [4,2,1,3]
    输出：[[1,2],[2,3],[3,4]]
    示例 2：
    输入：arr = [1,3,6,10,15]
    输出：[[1,3]]*/
    private static List<List<Integer>> minimumAbsDifference(int[] arr) {
        Arrays.sort(arr);
        List<List<Integer>> lists = new ArrayList<>();
        int min = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length - 1; i++) {
            min = Math.max(arr[i] - arr[i + 1], min);
        }

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] - arr[i + 1] == min) {
                List<Integer> list = new ArrayList<>(2);
                list.add(arr[i]);
                list.add(arr[i + 1]);
                lists.add(list);

            }
        }

        return lists;
    }

    /*1331. 数组序号转换 https://leetcode-cn.com/problems/rank-transform-of-an-array/
    给你一个整数数组 arr ，请你将数组中的每个元素替换为它们排序后的序号。
    序号代表了一个元素有多大。序号编号的规则如下：
    序号从 1 开始编号。
    一个元素越大，那么序号越大。如果两个元素相等，那么它们的序号相同。
    每个数字的序号都应该尽可能地小。
    示例 1：
    输入：arr = [40,10,20,30]
    输出：[4,1,2,3]
    解释：40 是最大的元素。 10 是最小的元素。 20 是第二小的数字。 30 是第三小的数字。
    示例 2：
    输入：arr = [100,100,100]
    输出：[1,1,1]
    解释：所有元素有相同的序号。*/

    //超时
    public static int[] arrayRankTransform(int[] arr) {
        Set<Integer> set = new TreeSet<>();
        for (int i = 0; i < arr.length; i++) {
            set.add(arr[i]);
        }
        Integer[] nums = new Integer[set.size()];

        nums = set.toArray(nums);

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < nums.length; j++) {

                if (arr[i] == nums[j]) {
                    arr[i] = j + 1;
                    break;
                }
            }
        }

        return arr;
    }

    /*1287. 有序数组中出现次数超过25%的元素 https://leetcode-cn.com/problems/element-appearing-more-than-25-in-sorted-array/
    给你一个非递减的 有序 整数数组，已知这个数组中恰好有一个整数，它的出现次数超过数组元素总数的 25%。
    请你找到并返回这个整数
    示例：
    输入：arr = [1,2,2,6,6,6,6,7,10]
    输出：6*/
    private static int findSpecialInteger(int[] arr) {
        if (arr.length == 1) {
            return arr[0];
        }
        int now;
        int next;
        int count = 1;
        for (int i = 0; i < arr.length - 1; i++) {
            now = arr[i];
            next = arr[i + 1];
            if (now == next) {
                count++;
                if (4 * count > arr.length) {
                    return now;
                }
            } else {
                count = 1;
            }
        }
        return -1;
    }

    /*1122. 数组的相对排序 https://leetcode-cn.com/problems/relative-sort-array/
    给你两个数组，arr1 和 arr2，
    arr2 中的元素各不相同
    arr2 中的每个元素都出现在 arr1 中
    对 arr1 中的元素进行排序，使 arr1 中项的相对顺序和 arr2 中的相对顺序相同。
    未在 arr2 中出现过的元素需要按照升序放在 arr1 的末尾。
    示例：
    输入：arr1 = [2,3,1,3,2,4,6,7,9,2,19], arr2 = [2,1,4,3,9,6]
    输出：[2,2,2,1,4,3,3,9,6,7,19]*/
    private static int[] relativeSortArray(int[] arr1, int[] arr2) {
        int[] nums = new int[arr1.length];
        int index = 0;
        int end = arr1.length - 1;
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < arr2.length; i++) {
            set.add(arr2[i]);
        }
        for (int i = 0; i < arr1.length; i++) {
            if (!set.contains(arr1[i])) {
                nums[end--] = arr1[i];
            }
        }

        for (int j = 0; j < arr2.length; j++) {

            for (int i = 0; i < arr1.length; i++) {
                if (arr1[i] == arr2[j]) {
                    nums[index++] = arr1[i];
                }
            }

        }
        Arrays.sort(nums, index, nums.length);

        return nums;
    }

    /* 1089. 复写零 https://leetcode-cn.com/problems/duplicate-zeros/
     给你一个长度固定的整数数组 arr，请你将该数组中出现的每个零都复写一遍，并将其余的元素向右平移。
     注意：请不要在超过该数组长度的位置写入元素。
     要求：请对输入的数组 就地 进行上述修改，不要从函数返回任何东西。
     示例 1：
     输入：[1,0,2,3,0,4,5,0]
     输出：null
     解释：调用函数后，输入的数组将被修改为：[1,0,0,2,3,0,0,4]
     示例 2：
     输入：[1,2,3]
     输出：null
     解释：调用函数后，输入的数组将被修改为：[1,2,3]*/
    private static void duplicateZeros(int[] arr) {
        int[] nums = new int[arr.length];
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                nums[index++] = arr[i];
                if (arr.length == index) {
                    break;
                }
                nums[index++] = arr[i];
            } else {
                nums[index++] = arr[i];
            }
            if (index == arr.length) {
                break;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = nums[i];
        }
        System.out.println(Arrays.toString(nums));

    }

    /*1018. 可被 5 整除的二进制前缀 https://leetcode-cn.com/problems/binary-prefix-divisible-by-5/
    给定由若干 0 和 1 组成的数组 A。我们定义 N_i：从 A[0] 到 A[i] 的第 i 个子数组被解释为一个二进制数（从最高有效位到最低有效位）。
    返回布尔值列表 answer，只有当 N_i 可以被 5 整除时，答案 answer[i] 为 true，否则为 false。
    示例 1：
    输入：[[1,0,0,1,0,1,0,0,1,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1]]
    输出：[true,false,false]
    解释：
    输入数字为 0, 01, 011；也就是十进制中的 0, 1, 3 。只有第一个数可以被 5 整除，因此 answer[0] 为真。
    示例 2：
    输入：[1,1,1]
    输出：[false,false,false]*/

    //右移一位再加上当前位数的值
    private static List<Boolean> prefixesDivBy5(int[] A) {
        List<Boolean> list = new ArrayList<>();
        int sum = 0;
        for (int i = 0; i < A.length; i++) {
            sum = A[i] + sum * 2;
            //取余，防止数据溢出
            //取余和不取余结果是一样的，比如sum=16  nextSum=4  sum%5==1  (1+4)%5==(16+4)%5
            if ((sum = sum % 5) == 0) {
                list.add(true);

            } else {
                list.add(false);

            }
        }
        return list;
    }


    /*1010. 总持续时间可被 60 整除的歌曲 https://leetcode-cn.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/
    在歌曲列表中，第 i 首歌曲的持续时间为 time[i] 秒。
    返回其总持续时间（以秒为单位）可被 60 整除的歌曲对的数量。形式上，我们希望索引的数字 i 和 j 满足  i < j 且有 (time[i] + time[j]) % 60 == 0。
    示例 1：
    输入：[30,20,150,100,40]
    输出：3
    解释：这三对的总持续时间可被 60 整数：
    (time[0] = 30, time[2] = 150): 总持续时间 180
    (time[1] = 20, time[3] = 100): 总持续时间 120
    (time[1] = 20, time[4] = 40): 总持续时间 60
    示例 2：
    输入：[60,60,60]
    输出：3
    解释：所有三对的总持续时间都是 120，可以被 60 整数。*/

    //超时
    public int numPairsDivisibleBy60(int[] time) {
        int count = 0;
        for (int i = 0; i < time.length; i++) {
            for (int j = i + 1; j < time.length; j++) {
                if ((time[i] + time[j]) % 60 == 0) {
                    count++;
                }
            }
        }
        return count;
    }

    /*1002. 查找常用字符 https://leetcode-cn.com/problems/find-common-characters/
    给定仅有小写字母组成的字符串数组 A，返回列表中的每个字符串中都显示的全部字符（包括重复字符）组成的列表。
    例如，如果一个字符在每个字符串中出现 3 次，但不是 4 次，则需要在最终答案中包含该字符 3 次。
    你可以按任意顺序返回答案。
    示例 1：
    输入：["bella","label","roller"]
    输出：["e","l","l"]
    示例 2：
    输入：["cool","lock","cook"]
    输出：["c","o"]*/
    private static List<String> commonChars(String[] A) {
        List<String> list = new ArrayList<>();
        //从a到z遍历
        for (int i = 0; i < 26; i++) {
            int minCount = Integer.MAX_VALUE;
            for (int j = 0; j < A.length; j++) {
                int count = 0;
                //遍历各个字符串获取字符出现的次数
                for (int z = 0; z < A[j].length(); z++) {
                    if (A[j].charAt(z) == (i + 'a')) {
                        count++;
                    }
                }
                //字符出现次数
                minCount = Math.min(minCount, count);
            }

            for (int j = 0; j < minCount; j++) {
                list.add((char) (i + 'a') + "");
            }

        }
        return list;

    }

    /*989. 数组形式的整数加法 https://leetcode-cn.com/problems/add-to-array-form-of-integer/
    对于非负整数 X 而言，X 的数组形式是每位数字按从左到右的顺序形成的数组。例如，如果 X = 1231，那么其数组形式为 [1,2,3,1]。
    给定非负整数 X 的数组形式 A，返回整数 X+K 的数组形式。
    示例 1：
    输入：A = [1,2,0,0], K = 34
    输出：[1,2,3,4]
    解释：1200 + 34 = 1234
    示例 2：
    输入：A = [2,7,4], K = 181
    输出：[4,5,5]
    解释：274 + 181 = 455*/
    public List<Integer> addToArrayForm(int[] A, int K) {
        return null;
    }

    /*941. 有效的山脉数组 https://leetcode-cn.com/problems/valid-mountain-array/
    给定一个整数数组 A，如果它是有效的山脉数组就返回 true，否则返回 false。
    让我们回顾一下，如果 A 满足下述条件，那么它是一个山脉数组：
    A.length >= 3
    在 0 < i < A.length - 1 条件下，存在 i 使得：
    A[0] < A[1] < ... A[i-1] < A[i]
    A[i] > A[i+1] > ... > A[A.length - 1]
    示例 1：
    输入：[2,1]
    输出：false
    示例 2：
    输入：[3,5,5]
    输出：false*/
    private static boolean validMountainArray(int[] A) {
        if (A == null || A.length < 3) {
            return false;
        }
        int max = Integer.MIN_VALUE;
        int index = 0;
        for (int i = 0; i < A.length; i++) {
            if (max < A[i]) {
                max = A[i];
                index = i;
            }
        }
        if (index == 0 || index == A.length - 1) {
            return false;
        }
        int left = index - 1;
        int right = index + 1;
        int maxLeft = max;
        int maxRight = max;
        while (left >= 0 || right < A.length) {
            if (left >= 0) {
                if (A[left] >= maxLeft) {

                    return false;
                } else {

                    maxLeft = A[left--];
                }
            }

            if (right < A.length) {
                if (A[right] >= maxRight) {

                    return false;
                } else {
                    maxRight = A[right++];
                }
            }

        }

        return true;
    }

    /*896. 单调数列 https://leetcode-cn.com/problems/monotonic-array/
    如果数组是单调递增或单调递减的，那么它是单调的。
    如果对于所有 i <= j，A[i] <= A[j]，那么数组 A 是单调递增的。 如果对于所有 i <= j，A[i]> = A[j]，那么数组 A 是单调递减的。
    当给定的数组 A 是单调数组时返回 true，否则返回 false。
    示例 1：
    输入：[1,2,2,3]
    输出：true
    示例 2：
    输入：[6,5,4,4]
    输出：true*/
    private static boolean isMonotonic(int[] A) {
        if (A == null || A.length == 0) {
            return false;
        }
        if (A.length == 1) {
            return true;
        }
        if (A[0] < A[A.length - 1]) {
            for (int i = 0; i < A.length - 1; i++) {
                if (A[i] > A[i + 1]) {
                    return false;
                }
            }
        } else if (A[0] > A[A.length - 1]) {
            for (int i = 0; i < A.length - 1; i++) {
                if (A[i] < A[i + 1]) {
                    return false;
                }
            }
        } else {
            for (int i = 0; i < A.length - 1; i++) {
                if (A[i] != A[i + 1]) {
                    return false;
                }
            }
        }

        return true;
    }

    /*849. 到最近的人的最大距离 https://leetcode-cn.com/problems/maximize-distance-to-closest-person/
    在一排座位（ seats）中，1 代表有人坐在座位上，0 代表座位上是空的。
    至少有一个空座位，且至少有一人坐在座位上。
    亚历克斯希望坐在一个能够使他与离他最近的人之间的距离达到最大化的座位上。
    返回他到离他最近的人的最大距离。
    示例 1：
    输入：[1,0,0,0,1,0,1]
    输出：2
    解释：
    如果亚历克斯坐在第二个空位（seats[2]）上，他到离他最近的人的距离为 2 。
    如果亚历克斯坐在其它任何一个空位上，他到离他最近的人的距离为 1 。
    因此，他到离他最近的人的最大距离是 2 。
    示例 2：
    输入：[1,0,0,0]
    输出：3
    解释：
    如果亚历克斯坐在最后一个座位上，他离最近的人有 3 个座位远。
    这是可能的最大距离，所以答案是 3 。*/
    public int maxDistToClosest(int[] seats) {
        int maxLeft = 0;
        int maxRight = 0;
        int maxLength = 0;
        for (int i = 0; i < seats.length; i++) {
            if (seats[i] != 0) {
                continue;
            } else {
                maxLeft = i;
                maxRight = i;
            }
            while (maxRight < seats.length) {
                if (seats[maxRight] == 0) {

                    maxRight++;
                }
            }


        }

        return 1;
    }

    /* 830. 较大分组的位置 https://leetcode-cn.com/problems/positions-of-large-groups/
     在一个由小写字母构成的字符串 S 中，包含由一些连续的相同字符所构成的分组。
     例如，在字符串 S = "abbxxxxzyy" 中，就含有 "a", "bb", "xxxx", "z" 和 "yy" 这样的一些分组。
     我们称所有包含大于或等于三个连续字符的分组为较大分组。找到每一个较大分组的起始和终止位置。
     最终结果按照字典顺序输出。
     示例 1:
     输入: "abbxxxxzzy"
     输出: [[3,6]]
     解释: "xxxx" 是一个起始于 3 且终止于 6 的较大分组。
     示例 2:
     输入: "abc"
     输出: []
     解释: "a","b" 和 "c" 均不是符合要求的较大分组。*/
    public List<List<Integer>> largeGroupPositions(String S) {
        return null;
    }

    /*819. 最常见的单词 https://leetcode-cn.com/problems/most-common-word/
    给定一个段落 (paragraph) 和一个禁用单词列表 (banned)。返回出现次数最多，同时不在禁用列表中的单词。
    题目保证至少有一个词不在禁用列表中，而且答案唯一。
    禁用列表中的单词用小写字母表示，不含标点符号。段落中的单词不区分大小写。答案都是小写字母。
    示例：
    输入:
    paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
    banned = ["hit"]
    输出: "ball"
    解释:
    "hit" 出现了3次，但它是一个禁用的单词。
    "ball" 出现了2次 (同时没有其他单词出现2次)，所以它是段落里出现次数最多的，且不在禁用列表中的单词。
    注意，所有这些单词在段落里不区分大小写，标点符号需要忽略（即使是紧挨着单词也忽略， 比如 "ball,"），
    "hit"不是最终的答案，虽然它出现次数更多，但它在禁用单词列表中。*/
    public String mostCommonWord(String paragraph, String[] banned) {
        return null;
    }

    /*717. 1比特与2比特字符 https://leetcode-cn.com/problems/1-bit-and-2-bit-characters/
    有两种特殊字符。第一种字符可以用一比特0来表示。第二种字符可以用两比特(10 或 11)来表示。
    现给一个由若干比特组成的字符串。问最后一个字符是否必定为一个一比特字符。给定的字符串总是由0结束。
    示例 1:
    输入:
    bits = [1, 0, 0]
    输出: True
    解释:
    唯一的编码方式是一个两比特字符和一个一比特字符。所以最后一个字符是一比特字符。
    示例 2:
    输入:
    bits = [1, 1, 1, 0]
    输出: False
    解释:
    唯一的编码方式是两比特字符和两比特字符。所以最后一个字符不是一比特字符。*/
    public boolean isOneBitCharacter(int[] bits) {
        return false;
    }

    /*697. 数组的度 https://leetcode-cn.com/problems/degree-of-an-array/
    给定一个非空且只包含非负数的整数数组 nums, 数组的度的定义是指数组里任一元素出现频数的最大值。
    你的任务是找到与 nums 拥有相同大小的度的最短连续子数组，返回其长度。
    示例 1:
    输入: [1, 2, 2, 3, 1]
    输出: 2
    解释:
    输入数组的度是2，因为元素1和2的出现频数最大，均为2.
    连续子数组里面拥有相同度的有如下所示:
    [1, 2, 2, 3, 1], [1, 2, 2, 3], [2, 2, 3, 1], [1, 2, 2], [2, 2, 3], [2, 2]
    最短连续子数组[2, 2]的长度为2，所以返回2.
    示例 2:
    输入: [1,2,2,3,1,4,2]
    输出: 6*/
    public int findShortestSubArray(int[] nums) {
        return 1;
    }

    /*674. 最长连续递增序列 https://leetcode-cn.com/problems/longest-continuous-increasing-subsequence/
    给定一个未经排序的整数数组，找到最长且连续的的递增序列。
    示例 1:
    输入: [1,3,5,4,7]
    输出: 3
    解释: 最长连续递增序列是 [1,3,5], 长度为3。
    尽管 [1,3,5,7] 也是升序的子序列, 但它不是连续的，因为5和7在原数组里被4隔开。
    示例 2:
    输入: [2,2,2,2,2]
    输出: 1
    解释: 最长连续递增序列是 [2], 长度为1。*/
    private static int findLengthOfLCIS(int[] nums) {
        if (nums.length == 1) {
            return 1;
        }
        int right = 0;
        int max = 0;
        int left;
        for (int i = 0; i < nums.length; i++) {
            right = i + 1;
            left = nums[i];
            while (right < nums.length && nums[right] > left) {
                left = nums[right++];
            }
            max = Math.max(max, right - i);
            if (right >= nums.length) {
                return max;
            }
        }

        return max;
    }

    /*643. 子数组最大平均数 I https://leetcode-cn.com/problems/maximum-average-subarray-i/
    给定 n 个整数，找出平均数最大且长度为 k 的连续子数组，并输出该最大平均数。
    示例 1:
    输入: [1,12,-5,-6,50,3], k = 4
    输出: 12.75
    解释: 最大平均数 (12-5-6+50)/4 = 51/4 = 12.75*/
    private static double findMaxAverage(int[] nums, int k) {
        double avgNum = Integer.MIN_VALUE;
        int end;
        double sum = 0;
        for (int i = 0; i < nums.length; i++) {
            end = i + k - 1;
            if (end > nums.length - 1) {
                return avgNum;
            }
            //i=0时遍历一遍
            if (i == 0) {
                for (int j = i; j <= Math.min(end, nums.length - 1); j++) {
                    sum += nums[j];

                }
            } else {
                //i大于0时，sum加上nums[end],减去nums[i-1]即为新的sum
                sum = sum + nums[end] - nums[i - 1];
            }
            avgNum = Math.max(avgNum, sum / k);
            if (end > nums.length - 1) {
                return avgNum;
            }
        }
        return avgNum;
    }


    /*628. 三个数的最大乘积 https://leetcode-cn.com/problems/maximum-product-of-three-numbers/
    给定一个整型数组，在数组中找出由三个数组成的最大乘积，并输出这个乘积。
    示例 1:
    输入: [1,2,3]
    输出: 6
    示例 2:
    输入: [1,2,3,4]
    输出: 24*/

    //随便哪三个数，不一定连续
    private static int maximumProduct(int[] nums) {
        Arrays.sort(nums);
        return Math.max(nums[0] * nums[1] * nums[nums.length - 1],
                nums[nums.length - 1] * nums[nums.length - 2] * nums[nums.length - 3]);

    }

    /*605. 种花问题 https://leetcode-cn.com/problems/can-place-flowers/
    假设你有一个很长的花坛，一部分地块种植了花，另一部分却没有。可是，花卉不能种植在相邻的地块上，它们会争夺水源，两者都会死去。
    给定一个花坛（表示为一个数组包含0和1，其中0表示没种植花，1表示种植了花），和一个数 n 。
    能否在不打破种植规则的情况下种入 n 朵花？能则返回True，不能则返回False。
    示例 1:
    输入: flowerbed = [1,0,0,0,1], n = 1
    输出: True
    示例 2:
    输入: flowerbed = [1,0,0,0,1], n = 2
    输出: False*/

    //判断当前花盆和上一个，下一个花盆是否都为0，为0时种上花，第一个花盆和最后一个花盆要特殊处理
    private static boolean canPlaceFlowers(int[] flowerbed, int n) {
        if (n == 0) {
            return true;
        }
        if (flowerbed.length == 1) {
            if ((n == 1 && flowerbed[0] == 0)) {
                return true;
            } else {
                return false;
            }
        }
        int count = 0;
        for (int i = 0; i < flowerbed.length; i++) {
            //第一个花盆
            if (i == 0) {
                if (flowerbed[i] == 0 && flowerbed[i + 1] == 0) {
                    //能种花时，种上花，贪心算法？
                    flowerbed[i] = 1;
                    count++;
                }

                continue;
            }
            //最后一个花盆
            if (i == flowerbed.length - 1) {
                if (flowerbed[i] == 0 && flowerbed[i - 1] == 0) {
                    count++;
                }
                break;
            }

            //中间的花盆
            if (flowerbed[i - 1] == 0 && flowerbed[i] == 0 && flowerbed[i + 1] == 0) {
                flowerbed[i] = 1;
                count++;
            }

        }
        return count >= n;
    }

    /*122. 买卖股票的最佳时机 II https://leetcode-cn.com/problems/best-time-to-buy-and-sell-stock-ii/
    给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。
    设计一个算法来计算你所能获取的最大利润。你可以尽可能地完成更多的交易（多次买卖一支股票）。
    注意：你不能同时参与多笔交易（你必须在再次购买前出售掉之前的股票）。
    示例 1:
    输入: [7,1,5,3,6,4]
    输出: 7
    解释: 在第 2 天（股票价格 = 1）的时候买入，在第 3 天（股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
    随后，在第 4 天（股票价格 = 3）的时候买入，在第 5 天（股票价格 = 6）的时候卖出, 这笔交易所能获得利润 = 6-3 = 3 。
    示例 2:
    输入: [1,2,3,4,5]
    输出: 4
    解释: 在第 1 天（股票价格 = 1）的时候买入，在第 5 天 （股票价格 = 5）的时候卖出, 这笔交易所能获得利润 = 5-1 = 4 。
    注意你不能在第 1 天和第 2 天接连购买股票，之后再将它们卖出。
    因为这样属于同时参与了多笔交易，你必须在再次购买前出售掉之前的股票。*/
    public int maxProfit(int[] prices) {
        return 1;
    }

    /*974. 和可被 K 整除的子数组 https://leetcode-cn.com/problems/subarray-sums-divisible-by-k/
    给定一个整数数组 A，返回其中元素之和可被 K 整除的（连续、非空）子数组的数目。
    示例：
    输入：A = [4,5,0,-2,-3,1], K = 5
    输出：7
    解释：
    有 7 个子数组满足其元素之和可被 K = 5 整除：
    [4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]*/
    //超时
    public static int subarraysDivByK1_974(int[] A, int K) {
        int sum = 0;
        int count = 0;
        for (int i = 0; i < A.length; i++) {
            sum = A[i];
            if (sum % K == 0) {
                count++;
            }
            for (int j = i + 1; j < A.length; j++) {
                sum += A[j];
                if (sum % K == 0) {
                    count++;
                }
            }

        }
        return count;
    }

    public int subarraysDivByK2_974(int[] A, int K) {
        Map<Integer, Integer> record = new HashMap<>();
        record.put(0, 1);
        int sum = 0, ans = 0;
        for (int elem : A) {
            sum += elem;
            // 注意 Java 取模的特殊性，当被除数为负数时取模结果为负数，需要纠正
            int modulus = (sum % K + K) % K;
            int same = record.getOrDefault(modulus, 0);
            ans += same;
            record.put(modulus, same + 1);
        }
        return ans;
    }


    public static void main(String[] args) {
//        System.out.println(compressString("abbccd"));
//        System.out.println(findTheDistanceValue(new int[]{2,1,100,3},new int[]{-5,-2,10,-3,7},6));
        System.out.println(maximumProduct(new int[]{-4, -3, -2, -1, 60}));
//        System.out.println(Arrays.toString(arrayRankTransform(new int[]{37, 12, 28, 9, 100, 56, 80, 5, 12})));
//        System.out.println(Arrays.toString(relativeSortArray(new int[]{2, 3, 1, 3, 2, 4, 6, 7, 9, 2, 19}, new int[]{2, 1, 4, 3, 9, 6})));
//        duplicateZeros(new int[]{0, 1});
//        System.out.println(commonChars(new String[]{"abcss","acsds","aacsfsfsf"}).toString());
//        System.out.println(prefixesDivBy5(new int[]{1,0,0,1,0,1,0,0,1,0,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1,0,1,0,0,0,0,1,1,0,1,0,0,0,1}));
//        System.out.println(findSpecialInteger(new int[]{1, 1, 2, 2, 3, 3, 3, 3}));
//        System.out.println(minimumAbsDifference(new int[]{1,3,6,10,15}).toString());
    }


}
