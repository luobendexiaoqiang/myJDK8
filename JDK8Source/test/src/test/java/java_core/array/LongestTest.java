package java_core.array;
/*最长公共前缀
编写一个函数来查找字符串数组中的最长公共前缀。
如果不存在公共前缀，返回空字符串 ""。
示例 1:
输入: ["flower","flow","flight"]
输出: "fl"
示例 2:
输入: ["dog","racecar","car"]
输出: ""
解释: 输入不存在公共前缀。
说明:
所有输入只包含小写字母 a-z 。
*/
public class LongestTest {

    public static String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        int minLength = strs[0].length();
        //先找出最短字符串的长度
        for (int i = 0; i < strs.length-1; i++) {
            minLength = Math.min(minLength,strs[i+1].length());
        }
        String subStr = "";
        while (minLength > 0) {
            //依次对比截取出来的字符串
            subStr = strs[0].substring(0, minLength);
            boolean f = true;
            for (String str : strs) {
                if (!str.substring(0, minLength).equals(subStr)) {
                    f = false;
                    break;
                }
            }
            if (f) {
                return subStr;
            }else {
                subStr = "";
            }
            minLength--;

        }
        return subStr;
    }

    public static void main(String[] args) {
        String[] strings = {"", "flow", "floght"};
        System.out.println(longestCommonPrefix(strings));
    }
}
