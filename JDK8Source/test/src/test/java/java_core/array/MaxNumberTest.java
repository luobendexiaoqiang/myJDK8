package java_core.array;
/*
至少是其他数字两倍的最大数
在一个给定的数组nums中，总是存在一个最大元素 。
查找数组中的最大元素是否至少是数组中每个其他数字的两倍。
如果是，则返回最大元素的索引，否则返回-1。
示例 1:
输入:nums=[3,6,1,0]
输出:1
解释:6是最大的整数,对于数组中的其他整数,
6大于数组中其他元素的两倍。6的索引是1,所以我们返回1.
示例 2:
输入:nums=[1,2,3,4]
输出:-1
解释:4没有超过3的两倍大,所以我们返回-1.

提示:
nums 的长度范围在[1, 50].
每个 nums[i] 的整数范围在 [0, 100].
*/

import java.util.Arrays;

public class MaxNumberTest {
    // me: 先从小到大排序, 再取最后两个元素判断
    public int dominantIndex1(int[] nums) {
        if (nums == null) {
            return -1;
        }
        if(nums.length ==1)
            return 0;
        int[] arr = Arrays.copyOf(nums, nums.length);


        Arrays.sort(nums);
        if (nums[nums.length - 1] >= 2 * nums[nums.length - 2]) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == nums[nums.length - 1]) {
                    return i;
                }
            }
        }

        return -1;
    }

    //
    public int dominantIndex2(int[] nums) {
        int max = 0;
        boolean pin = true;
        for(int i = 1; i < nums.length; i++) {
            if(nums[max] < nums[i]) {
                if(nums[i] >= nums[max] * 2) pin = true;
                else pin = false;
                max = i;
            } else {
                if(nums[max] < nums[i] * 2) pin = false;
            }
        }
        if(pin) return max;
        else return -1;


    }

    public int dominantIndex3(int[] nums) {
        int maxIndex = 0;
        //先获取最大数的索引
        for (int i = 0; i < nums.length; ++i) {
            if (nums[i] > nums[maxIndex])
                maxIndex = i;
        }
        //再比较
        for (int i = 0; i < nums.length; ++i) {
            if (maxIndex != i && nums[maxIndex] < 2 * nums[i])
                return -1;
        }
        return maxIndex;
    }

    public static void main(String[] args) {

    }
}
