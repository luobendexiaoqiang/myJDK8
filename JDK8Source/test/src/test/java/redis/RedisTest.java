package redis;

import org.junit.Test;
import redis.clients.jedis.*;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Pipeline;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * redis测试类
 * @author ydfind
 * @date 2019.09.29
 */
public class RedisTest {
    private static Jedis jedis = new Jedis("192.168.10.150");
    //set方法nxxx参数,NX代表不存在时才设置,XX表示只有在key存在时才设置
    private final static String NOT_EXISTS = "NX";
    private final static String IS_EXISTS = "XX";

    //set方法expx参数,ex代表单位为秒,px代表单位为毫秒
    private final static String SECONDS = "EX";
    private final static String MILLI_SECONDS = "PX";



    /**
     * 创建连接
     * @return redis实例
     */
    public Jedis createJedis(){
        Jedis jedis = new Jedis("192.168.10.150");
//        jedis.auth("123456");
        return jedis;
    }

    @Test
    public void testKey() {
        //jedis.set("name1", "pangting");
        //jedis.set("age", "18", IS_EXISTS, SECONDS, 10);
        //当age不存在时,设置age的值并设置过期时间为10秒
        //System.out.println(jedis.set("age", "18", NOT_EXISTS, SECONDS, 10));
        System.out.println(jedis.type("name"));//获取key:name的类型
        //jedis.del("name");//删除key:name
        System.out.println(jedis.get("name"));//获取"name"对应的值
        System.out.println(jedis.randomKey());//返回一个随机的key
        Set<String> keys = jedis.keys("*");//返回所有的key
        System.out.println(keys.toString());

        System.out.println(jedis.ttl("name"));//返回key:name的过期时间(单位/秒);-1代表永不过期
        jedis.expireAt("name", 1588174437);//设置key:name在某个时间过期
        System.out.println(jedis.pttl("name"));//返回key:name的过期时间(单位/豪秒)
        //jedis.rename("name", "newName");//修改key:name为newName(会覆盖掉原来的newName)
        System.out.println(jedis.exists("name"));//判断key:name是否存在

        jedis.setex("name", 10, "pangting");//设置key的值和10秒的过期时间
        jedis.psetex("name", 1000L, "pangting");//设置key的值和1000毫秒的过期时间
        jedis.setnx("name", "pangting");//设置key的值,若key已存在,则设置失败并返回0,不存在则设置成功并返回1

    }

    @Test
    public void testString() {
        //jedis.append("name", "dashadiao");
        //批量设置key-value的值,当有一个key已存在时,设置失败并返回0
        //System.out.println(jedis.msetnx("name3","pangting","name4","dapangting"));
        System.out.println(jedis.incr("age"));//自增
        jedis.incrBy("age", 100);//增加100
        System.out.println(jedis.get("age"));
        jedis.decr("age");//自减
        jedis.decrBy("age", 50);//减50
        System.out.println(jedis.get("age"));
        //批量获取key的值
        List<String> mget = jedis.mget("name", "name3", "name4");
        System.out.println(mget.toString());
        System.out.println(jedis.strlen("name"));//返回value的长度

        System.out.println(jedis.getrange("name",0,-1));//根据索引截取value的值

    }
    @Test
    public void testHash() {
        jedis.hset("myHash", "name", "xiaoming");
        jedis.hset("myHash", "age", "22");
        jedis.hset("myHash", "height", "178");
        System.out.println(jedis.hget("myHash", "name"));
        System.out.println(jedis.hget("myHash", "age"));
        System.out.println(jedis.hget("myHash", "height"));
        List<String> hmget = jedis.hmget("myHash", "name", "age", "height");//根据属性获取value
        System.out.println(hmget.toString());
        Map<String, String> map = jedis.hgetAll("myHash");//返回所有键值对
        System.out.println(map.toString());
        List<String> myHash = jedis.hvals("myHash");//返回所有value
        System.out.println(myHash.toString());
        System.out.println(jedis.hexists("myHash", "age"));

    }

    @Test
    public void testList() {
        jedis.lpush("myList", "aa", "bb", "cc");//left-push  : [cc, bb, aa]
        System.out.println(jedis.lrange("myList",0,-1).toString());//根据索引范围获取List的全部元素
        jedis.rpush("myList", "dd", "ee");//right-push   [cc, bb, aa, dd, ee]
        jedis.lpop("myList");//向左移出队列一个元素  [bb, aa, dd, ee]
        jedis.rpop("myList");//向右移出队列一个元素  [bb, aa, dd]

        jedis.blpop(10, "myList");//block-left-pop  阻塞方式向左出队,10秒后超时   [aa, dd]
        jedis.brpop(10, "myList");//block-right-pop  阻塞方式向右出队,10秒后超时  [aa]

        jedis.linsert("myList", Client.LIST_POSITION.BEFORE, "aa", "abc");//在aa元素之前插入abc
        System.out.println(jedis.lrange("myList",0,-1).toString());  //[abc, aa]
        Long length = jedis.llen("myList");//获取集合长度
        String value = jedis.lindex("myList", 1);//根据索引获取元素

        /**
         * 通过改变入队和出队的方式实现
         * 先进先出队列(lpush-->rpop 或者rpush-->lpop),
         * 栈(lpush-->lpop或者rpush-->rpop)的数据结构
         */
        jedis.flushAll();
    }

    /**
     * 无序集合
     */
    @Test
    public void testSet() {
        jedis.sadd("mySet", "aa", "bb", "cc", "aa", "dd");//[dd, bb, cc, aa]
        Long setLong = jedis.scard("mySet");//获取set的长度
        Set<String> mySet1 = jedis.smembers("mySet");//获取所有set的元素
        System.out.println(mySet1.toString());

        jedis.sadd("mySet1", "aa");
        jedis.sadd("mySet2", "dd");

        Set<String> sdiff = jedis.sdiff("mySet", "mySet1", "mySet2");//取差集 mySet - mySet1 - mySet2
        System.out.println(sdiff.toString());//[bb, cc]

        Set<String> sinter = jedis.sinter("mySet", "mySet1");//取交集
        System.out.println(sinter.toString());//[aa]

        Set<String> sunion = jedis.sunion("mySet", "mySet1");//取并集
        System.out.println(sunion.toString());

        jedis.sunionstore("newSet", "mySet", "mySet1");//将mySet和mySet1的并集保存入新的newSet中

        String mySet = jedis.spop("mySet");//随机移除一个元素并返回
        jedis.srem("mySet", "aa", "bb"); //删除mySet中的aa和bb元素


        jedis.flushAll();
    }

    /**
     * 有序集合
     */
    @Test
    public void testSortSet() {
        Map<String, Double> map = new HashMap<>();
        map.put("aaa", 1.0);
        map.put("bbb", 1.2);
        map.put("ccc", -1.2);
        map.put("ddd", 0.0);
        jedis.zadd("mySortSet", map);

        Set<String> mySortSet = jedis.zrange("mySortSet", 0, -1);
        System.out.println(mySortSet);//[ccc, ddd, aaa, bbb]  从小到大排序

        jedis.flushAll();

    }

    /**
     * 基数运算
     */
    @Test
    public void testHyperLogLog() {
        jedis.flushAll();
        jedis.pfadd("myHyperLogLog", "a", "b", "c");//添加元素
        jedis.pfadd("myHyperLogLog2", "c", "d", "e");
        System.out.println(jedis.pfcount("myHyperLogLog"));//获取基数   3
        System.out.println(jedis.pfcount("myHyperLogLog2")); //3
        System.out.println(jedis.pfcount("myHyperLogLog2","myHyperLogLog"));//获取两个集合合并之后的基数    5
        //将myHyperLogLog 和myHyperLogLog2合并成为一个新的myHyperLogLog3
        jedis.pfmerge("myHyperLogLog3","myHyperLogLog2", "myHyperLogLog");//5
        System.out.println(jedis.pfcount("myHyperLogLog3"));
    }

    /**
     *
     */
    @Test
    public void testBitMap() {
        jedis.flushAll();
        for (int userId = 0; userId < 10000; userId++) {
            if (userId % 2 == 0) {
                //模拟签到,将用户ID设为偏移位offset
                jedis.setbit("sign_user_2020_05_01", userId, true);
            }
        }
        Long sign_user_2020_05_01 = jedis.bitcount("sign_user_2020_05_01");//获取1的数量,即签到用户的数量
        System.out.println(sign_user_2020_05_01);
        System.out.println(jedis.getbit("sign_user_2020_05_01",112));//查看112用户是否签到
        for (int userId = 0; userId < 10000; userId++) {
            if (userId % 2 != 0) {
                //模拟签到,将用户ID设为偏移位offset
                jedis.setbit("sign_user_2020_05_02", userId, true);
            }
        }
        //逻辑与运算:获取5.1和5.2都签到的用户
        jedis.bitop(BitOP.AND, "sign_user_2020_05_02_and_2020_05_01", "sign_user_2020_05_02", "sign_user_2020_05_01");
        //逻辑或:获取5.1和5.2签到的用户总数
        jedis.bitop(BitOP.OR, "sign_user_2020_05_02_or_2020_05_01", "sign_user_2020_05_02", "sign_user_2020_05_01");
        //逻辑非:获取5.1签到但是5.2不登录的用户数
        jedis.bitop(BitOP.NOT, "sign_user_2020_05_01_not_2020_05_02", "sign_user_2020_05_01", "sign_user_2020_05_02");

        System.out.println(jedis.bitcount("sign_user_2020_05_02_and_2020_05_01"));
    }

    @Test
    public void testScan() {
        Set<String> keys = jedis.keys("*");
        System.out.println(keys.toString());
        int count = 3;
        ScanParams scanParams = new ScanParams();
        ScanResult<String> scan = jedis.scan("0", scanParams.count(count).match("*"));
        List<String> list = new ArrayList<>(scan.getResult());
        for (; ; ) {
            String stringCursor = scan.getStringCursor();
            //当游标为0时说明遍历完成,退出循环
            if (stringCursor.equals("0")) {

                break;
            } else {
                scan = jedis.scan(stringCursor, scanParams.count(count).match("*"));
                System.out.println("---"+scan.getResult().toString());
                list.addAll(scan.getResult());
            }
        }
        System.out.println(list.toString());
//        jedis.scan("0");
//        jedis.hscan("myHash", "0");
    }


    public static void delLargeListKey(Jedis jedis){
        // 游标初始值为0
        String cursor = ScanParams.SCAN_POINTER_START;
        String key = "test:xttblog:*";
        ScanParams scanParams = new ScanParams();
        scanParams.match(key);// 匹配以 test:xttblog:* 为前缀的 key
        scanParams.count(1000);
        while (true){
            //使用scan命令获取500条数据，使用cursor游标记录位置，下次循环使用
            ScanResult<String> scanResult = jedis.scan(cursor, scanParams);
            cursor = scanResult.getStringCursor();// 返回0 说明遍历完成
            List<String> list = scanResult.getResult();
            long t1 = System.currentTimeMillis();
            for(int m = 0;m < list.size();m++){
                String mapentry = list.get(m);
                //jedis.del(key, mapentry);
                jedis.ltrim("test:xttblog:", 0 ,1);
            }
            long t2 = System.currentTimeMillis();
            System.out.println("删除" + list.size()
                    + "条数据，耗时: " + (t2-t1) + "毫秒,cursor:" + cursor);
            if ("0".equals(cursor)){
                break;
            }
        }
    }

    public static void delLargeSetKey(Jedis jedis){
        // 游标初始值为0
        String cursor = ScanParams.SCAN_POINTER_START;
        ScanParams scanParams = new ScanParams();
        scanParams.count(1000);
        String key = "test:xttblog";
        while (true){
            //使用sscan命令获取500条数据，使用cursor游标记录位置，下次循环使用
            ScanResult<String> sscanResult = jedis.sscan(key, cursor, scanParams);
            cursor = sscanResult.getStringCursor();// 返回0 说明遍历完成
            List<String> scanResult = sscanResult.getResult();
            long t1 = System.currentTimeMillis();
            for(int m = 0;m < scanResult.size();m++){
                String mapentry = scanResult.get(m);
                jedis.srem(key, mapentry);
            }
            long t2 = System.currentTimeMillis();
            System.out.println("删除" + scanResult.size()
                    + "条数据，耗时: " + (t2-t1) + "毫秒,cursor:" + cursor);
            if ("0".equals(cursor)){
                break;
            }
        }
    }

    public static void delLargeHashKey(Jedis jedis){
        // 游标初始值为0
        String cursor = ScanParams.SCAN_POINTER_START;
        ScanParams scanParams = new ScanParams();
        scanParams.count(1000);
        String key = "test:xttblog";
        while (true){
            //使用hscan命令获取500条数据，使用cursor游标记录位置，下次循环使用
            ScanResult<Map.Entry<String, String>> hscanResult =
                    jedis.hscan(key, cursor, scanParams);
            cursor = hscanResult.getStringCursor();// 返回0 说明遍历完成
            List<Map.Entry<String, String>> scanResult =
                    hscanResult.getResult();
            long t1 = System.currentTimeMillis();
            for(int m = 0;m < scanResult.size();m++){
                Map.Entry<String, String> mapentry = scanResult.get(m);
                jedis.hdel(key, mapentry.getKey());
            }
            long t2 = System.currentTimeMillis();
            System.out.println("删除" + scanResult.size()
                    + "条数据，耗时: " + (t2-t1) + "毫秒,cursor:" + cursor);
            if ("0".equals(cursor)){
                break;
            }
        }
    }

    public static void delLargeZSetKey(Jedis jedis){
        // 游标初始值为0
        String cursor = ScanParams.SCAN_POINTER_START;
        String key = "test:xttblog:*";
        ScanParams scanParams = new ScanParams();
        scanParams.match(key);// 匹配以 test:xttblog:* 为前缀的 key
        scanParams.count(1000);
        while (true){
            //使用 zscan 命令获取 500 条数据，使用cursor游标记录位置，下次循环使用
            ScanResult<Tuple> scanResult = jedis.zscan(key, cursor, scanParams);
            cursor = scanResult.getStringCursor();// 返回0 说明遍历完成
            List<Tuple> list = scanResult.getResult();
            long t1 = System.currentTimeMillis();
            for(int m = 0;m < list.size();m++){
                Tuple tuple = list.get(m);
                System.out.println("Element：" + tuple.getElement()
                        + "，Score：" + tuple.getScore());
            }
            long t2 = System.currentTimeMillis();
            System.out.println("删除" + list.size()
                    + "条数据，耗时: " + (t2-t1) + "毫秒,cursor:" + cursor);
            if ("0".equals(cursor)){
                break;
            }
        }
    }

    @Test
    public void pipeCompare() {
        Jedis redis = jedis;
        //redis.auth("12345678");//授权密码 对应redis.conf的requirepass密码
        Map<String, String> data = new HashMap<String, String>();
        redis.select(8);//使用第8个库
        redis.flushDB();//清空第8个库所有数据
        // hmset
        long start = System.currentTimeMillis();
        // 直接hmset
        for (int i = 0; i < 10000; i++) {
            data.clear();  //清空map
            data.put("k_" + i, "v_" + i);
            redis.hmset("key_" + i, data); //循环执行10000条数据插入redis
        }
        long end = System.currentTimeMillis();
        System.out.println("    共插入:[" + redis.dbSize() + "]条 .. ");
        System.out.println("1,未使用PIPE批量设值耗时" + (end - start)  + "毫秒..");
        redis.select(8);
        redis.flushDB();
        // 使用pipeline hmset
        Pipeline pipe = redis.pipelined();
        start = System.currentTimeMillis();
        //
        for (int i = 0; i < 10000; i++) {
            data.clear();
            data.put("k_" + i, "v_" + i);
            pipe.hmset("key_" + i, data); //将值封装到PIPE对象，此时并未执行，还停留在客户端
        }
        pipe.sync(); //将封装后的PIPE一次性发给redis
        end = System.currentTimeMillis();
        System.out.println("    PIPE共插入:[" + redis.dbSize() + "]条 .. ");
        System.out.println("2,使用PIPE批量设值耗时" + (end - start)+ "毫秒 ..");
//--------------------------------------------------------------------------------------------------
        // hmget
        Set<String> keys = redis.keys("key_*"); //将上面设值所有结果键查询出来
        // 直接使用Jedis hgetall
        start = System.currentTimeMillis();
        Map<String, Map<String, String>> result = new HashMap<String, Map<String, String>>();
        for (String key : keys) {
            //此处keys根据以上的设值结果，共有10000个，循环10000次
            result.put(key, redis.hgetAll(key)); //使用redis对象根据键值去取值，将结果放入result对象
        }
        end = System.currentTimeMillis();
        System.out.println("    共取值:[" + redis.dbSize() + "]条 .. ");
        System.out.println("3,未使用PIPE批量取值耗时 " + (end - start)+ "毫秒..");

        // 使用pipeline hgetall
        result.clear();
        start = System.currentTimeMillis();
        for (String key : keys) {
            pipe.hgetAll(key); //使用PIPE封装需要取值的key,此时还停留在客户端，并未真正执行查询请求
        }
        pipe.sync();  //提交到redis进行查询

        end = System.currentTimeMillis();
        System.out.println("    PIPE共取值:[" + redis.dbSize() + "]条 .. ");
        System.out.println("4,使用PIPE批量取值耗时" + (end - start)  + "毫秒 ..");

        redis.disconnect();
    }


    @Test
    public void testRedis(){
        System.out.println("-------------------测试redis实例创建-----------------");
        Jedis jedis = createJedis();
        System.out.println("connect successful!");
        System.out.println("service running: "+jedis.ping());
        System.out.println("-------------------测试字符串-----------------");
        jedis.set("name", "ydfind");
        System.out.println("key name = "+ jedis.get("name"));

        System.out.println("-------------------测试List-----------------");
        jedis.lpush("myList", "item1");
        jedis.lpush("myList", "item2");
        jedis.lpush("myList", "item3");
        // 获取存储的数据并输出
        List<String> list = jedis.lrange("myList", 0 ,2);
        for(int i=0; i<list.size(); i++) {
            System.out.println("item" + (i + 1) + " = "+list.get(i));
        }
        System.out.println("-------------------测试Set-----------------");
        Set<String> keys = jedis.keys("*");
        for (String key : keys) {
            System.out.println(key);
        }
        jedis.flushAll();

    }
}
