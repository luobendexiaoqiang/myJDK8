package current.thread;

import lombok.SneakyThrows;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

/**
 * leetCode: 1114.按序打印
 */
public class SortThreadTest {
    private static CountDownLatch countDownLatchB = new CountDownLatch(1);
    private static CountDownLatch countDownLatchC = new CountDownLatch(1);
    private static final Object obj = new Object();
    private static int type = 1;

    private static void printA() {
        synchronized (obj) {

            if (type == 1) {
                System.out.print("A");
                type = 2;
                //更新type后唤醒其他线程
                obj.notifyAll();
            }
        }
    }

    private static void printB() throws InterruptedException {
        synchronized (obj) {

            while (type != 2) {
                //当type!=2时,阻塞当前线程
                obj.wait();
            }
            if (type == 2) {
                System.out.print("B");
                type = 3;
                //更新type后唤醒其他线程
                obj.notifyAll();
            }
        }
    }

    private static void printC() throws InterruptedException {
        synchronized (obj) {

            while (type != 3) {
                //当type!=3时,阻塞当前线程
                obj.wait();
            }
            if (type == 3) {
                System.out.print("C");
            }
        }
    }

    @Test
    public void test1() throws InterruptedException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                printA();
            }
        }).start();
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                printB();
            }
        }).start();
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                printC();
            }
        }).start();
        Thread.sleep(3000);
    }

    private static void printA2() {
        System.out.print("A");
        countDownLatchB.countDown();
    }

    private static void printB2() throws InterruptedException {
        countDownLatchB.await();
        System.out.print("B");
        countDownLatchC.countDown();
    }

    private static void printC2() throws InterruptedException {
        countDownLatchC.await();
        System.out.print("C");
    }

    @Test
    public void test2() throws InterruptedException {

        new Thread(new Runnable() {
            @Override
            public void run() {
                printA2();
            }
        }).start();
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                printB2();
            }
        }).start();
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                printC2();
            }
        }).start();
        Thread.sleep(3000);
    }

}