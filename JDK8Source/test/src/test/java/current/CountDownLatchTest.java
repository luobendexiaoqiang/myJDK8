package current;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class CountDownLatchTest {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        CountDownLatch countDownLatch = new CountDownLatch(3);
        try {
            for (int i = 0; i < 2; i++) {

                executorService.submit(new Callable<List<String>>() {

                    @Override
                    public List<String> call() throws Exception {
                        try {
                            //doSomeThing()
                            Thread.sleep(1000);
                            System.out.println(Thread.currentThread().getName() + "线程开始执行");
                        } catch (Exception e) {

                        } finally {
                           countDownLatch.countDown();

                        }
                        return new ArrayList<>();

                    }

                });
            }

            Future<List<String>> submit = executorService.submit(new Callable<List<String>>() {

                @Override
                public List<String> call() throws Exception {
                    try {
                        //doSomeThing()
                        Thread.sleep(5000);
                        System.out.println(Thread.currentThread().getName() + "线程开始执行");
                    } catch (Exception e) {

                    } finally {
                        countDownLatch.countDown();

                    }
                    ArrayList<String> list = new ArrayList<>();
                    list.add("hello world !");
                    return list;

                }

            });


            executorService.submit(new Callable<List<String>>() {

                @Override
                public List<String> call() throws Exception {
                    try {
                        //doSomeThing()
                        countDownLatch.await();
                        System.out.println("测试线程开始执行");
                    } catch (Exception e) {

                    } finally {

                    }
                    return new ArrayList<>();

                }

            });
            System.out.println("-------------");
            countDownLatch.await();
            System.out.println(submit.get(1,TimeUnit.SECONDS).get(0));
            System.out.println("主线程执行");

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            executorService.shutdown();
        }

    }
}
