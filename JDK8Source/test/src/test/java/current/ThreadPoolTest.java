package current;

import lombok.SneakyThrows;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolTest {
    private static ExecutorService executorService1 = Executors.newCachedThreadPool();
    private static ExecutorService executorService2 = Executors.newSingleThreadExecutor();
    private static ExecutorService executorService3 = Executors.newFixedThreadPool(5);

    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {

            executorService3.submit(new Runnable() {
                @SneakyThrows
                @Override
                public void run() {
                    System.out.println(Thread.currentThread().getName() + "---start---" + System.currentTimeMillis() / 1000);
                    Thread.sleep(3000);
                    System.out.println(Thread.currentThread().getName() + "---end---" + System.currentTimeMillis() / 1000);
                }
            });
        }
        executorService3.shutdown();
    }

}
