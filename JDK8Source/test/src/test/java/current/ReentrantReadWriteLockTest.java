package current;

import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteLockTest {
    private static int count = 0;
    private static ReentrantReadWriteLock reenTrantReadWriteLock = new ReentrantReadWriteLock();

    static void write() throws InterruptedException {

        System.out.println(Thread.currentThread().getName()+"---write---start:  " + count);
        count++;
        System.out.println(Thread.currentThread().getName()+"---write---end:  " + count);
    }

    static int read() {
        System.out.println(Thread.currentThread().getName()+"---read---start:   " + count);
        return count;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        reenTrantReadWriteLock.writeLock().lock();
                        Thread.sleep(1000);
                        write();
                        reenTrantReadWriteLock.writeLock().unlock();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }

        for (int i = 0; i < 5; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        reenTrantReadWriteLock.readLock().lock();
                        read();
                        reenTrantReadWriteLock.readLock().unlock();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();

        }


    }

}
