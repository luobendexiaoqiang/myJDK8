package current;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SemaphoreTest extends  Semaphore{

    public SemaphoreTest(int permits) {
        super(permits);
    }

    @Override
    protected void reducePermits(int reduction) {
        // 修改信号量的数量
        super.reducePermits(reduction);
    }

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();

        //信号量，只允许 3个线程同时访问
        SemaphoreTest semaphore = new SemaphoreTest(2);

        for (int i=0;i<10;i++){
            final long num = i;
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        //获取许可
                        semaphore.acquire();
                        //semaphore.reducePermits(1);

                        //执行
                        System.out.println(Thread.currentThread().getName()+"---get()");
                        Thread.sleep(1000);
                        //释放
                        semaphore.release();
                        System.out.println(Thread.currentThread().getName()+"---release()");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        executorService.shutdown();
    }

}