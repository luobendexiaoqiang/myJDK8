package current;

import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierTest {
    // 自定义工作线程
    private static class Worker extends Thread {
        /*
        CyclicBarrier:循环栅栏
        只有当足够数量的线程运行到栅栏处(CyclicBarrier.await()方法),
        这些线程才能继续往下执行,否则这些线程将会被阻塞
        例子:小朋友们去要到校门口集合,然后一起坐校车去春游;
        那么显然,必须所有的小朋友都到齐了, 校车才会开启,
        有小朋友还没到,那么其他的小朋友将会在校门口等待
        校门口就相当于一个栅栏(await)

        --------------------------------------------

        CountDownLatch:计数器
        计数器为0时,唤醒调用CountDownLatch.await()方法的线程, 否则这些线程将被阻塞
        使用CountDownLatch.countDown()方法将计数器减一;
        常用于某些线程在其他任务执行完毕之后再执行
        例如: 三个工人完成工作之后(countDown), 老板才会来检查他们的工作(await)

        区别:CountDownLatch只能使用一次, CyclicBarrier可以循环使用
        * */
        private CyclicBarrier cyclicBarrier;

        public Worker(CyclicBarrier cyclicBarrier) {
            this.cyclicBarrier = cyclicBarrier;
        }

        @Override
        public void run() {
            super.run();

            try {
                System.out.println(Thread.currentThread().getName() + "开始等待其他线程");
                cyclicBarrier.await();
                System.out.println(Thread.currentThread().getName() + "开始执行");
                // 工作线程开始处理，这里用Thread.sleep()来模拟业务处理
                Thread.sleep(1000);
                System.out.println(Thread.currentThread().getName() + "执行完毕");
                System.out.println(Thread.currentThread().getName() + "---开始第二次等待---");
                cyclicBarrier.await();
                System.out.println(Thread.currentThread().getName() + "第二次等待完成");
                Thread.sleep(2000);
                System.out.println(Thread.currentThread().getName() + "第二次执行完成");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        int threadCount = 3;
        CyclicBarrier cyclicBarrier = new CyclicBarrier(threadCount);

        for (int i = 0; i < threadCount; i++) {
            System.out.println("创建工作线程" + i);
            Worker worker = new Worker(cyclicBarrier);
            worker.start();
        }
    }
}