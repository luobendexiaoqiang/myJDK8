package mianshi.huawei.easy;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
/*
根据输入的日期，计算是这一年的第几天。。
详细描述：
输入某年某月某日，判断这一天是这一年的第几天？。
输入描述:
输入三行，分别是年，月，日

输出描述:
成功:返回outDay输出计算后的第几天;
失败:返回-1

示例1
输入
2012
12
31
输出
366
 */
public class 日期到天数转换 {
    //
//输入年月日，返回天数
//1-31 2-28 3-31 4-30 5-31 6-30 7-31 8-31 9-30 10-31 11-30 12-31

        public static int getAllDay(int year,int month,int day) {
            int[] dayary= {31,28,31,30,31,30,31,31,30,31,30,31};
            //判断闰年
            if(year%4==0) dayary[1]=29;
            int total=0;
            for(int i=0;i<month-1;i++) {
                total+=dayary[i];
            }
            total+=day;
            return total;
        }
        public static void main(String[] args)throws IOException{
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String line="";
            while((line=br.readLine())!=null) {
                String[] lineary=line.split(" ");
                int year=Integer.parseInt(lineary[0]);
                int month=Integer.parseInt(lineary[1]);
                int day=Integer.parseInt(lineary[2]);
                System.out.println(getAllDay(year, month, day));
            }

    }
}
