package mianshi.huawei.easy;

import java.util.ArrayList;
import java.util.List;

public class 最长递增子序列 {
    public static void main(String[] args) {
        int[] arr = new int[]{-1,0,-2};
        test(arr);
    }

    public static void test(int[] nums) {
        if (nums == null || nums.length == 0) {
            return;
        }
        if (nums.length == 1) {
            System.out.println(nums[0]);
            return;
        }
        List<List<Integer>> list = new ArrayList<>();
        int maxLength = Integer.MIN_VALUE;
        for (int start = 0; start < nums.length; start++) {
            int num = nums[start];
            int end = start + 1;
            while (end < nums.length && nums[end] > num) {
                num = nums[end];
                end++;
            }
            int length = end - start;
            //大于时更新
            if (length > maxLength) {
                list.clear();
                addList(start, end - 1, nums, list);
                maxLength = length;
                //等于时添加
            } else if (maxLength == length) {
                addList(start, end - 1, nums, list);
            }

        }
        System.out.println(list.toString());

    }

    private static void addList(int start, int end, int[] nums, List<List<Integer>> list) {
        List<Integer> integers = new ArrayList<>(end - start + 1);
        for (int i = start; i <= end; i++) {
            integers.add(nums[i]);
        }
        list.add(integers);
    }

}
