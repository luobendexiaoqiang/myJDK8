package mianshi.huawei.exam;

import java.util.Scanner;

/**
 * 正整数S,能不能分成连续num个数的和
 */
public class Main2 {
    public static void main(String[] args) {
        /*Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int S = scanner.nextInt();
            int num = scanner.nextInt();
            test(S, num);
        }*/
        test(100, 5);
    }

    public static void test(int S, int num) {
        if (S < num) {
            System.out.println(-1);
        } else {

            //基数
            if (num % 2 == 1) {
                if (S % num == 0) {
                    int i = S / num;
                    int start = i - num / 2;
                    int end = i + num / 2;
                    for (int j = start; j <= end; j++) {
                        System.out.print(j);
                        System.out.print(" ");

                    }

                } else {
                    System.out.println(-1);
                }
            } else {
                //偶数
                if ((S * 2) % num == 0) {
                    int i = ((S * 2) / num + 1) / 2;
                    int end = i + num / 2 - 1;
                    int start = end - num + 1;
                    for (int j = start; j <= end; j++) {
                        System.out.print(j);
                        System.out.print(" ");
                    }

                } else {
                    System.out.println(-1);
                }


            }

        }

    }
}
