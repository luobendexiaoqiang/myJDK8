package mianshi.huawei.exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Stack;

public class Main3 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            test(line);
        }
        /*Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String str = scanner.next();
            test(str);
        }*/
        //System.out.println(-7/3);
        //test("(add 1 2)");
        //operate("(mul 20 30)");

    }

    public static void test(String str) {
        char[] chars = str.toCharArray();
        StringBuilder stringBuilder = new StringBuilder(str);
        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < chars.length; i++) {
            //加入栈
            if (chars[i] == '(') {
                Integer push = stack.push(i);
                //System.out.println("加入" + push);
            } else if (chars[i] == ')') {
                //末尾
                if (i == chars.length - 1) {
                    int result = operate(stringBuilder.toString());
                    if (result == Integer.MIN_VALUE) {
                        return;
                    }
                    System.out.println(result);

                } else {


                    int end = i;
                    //弹出值,获取(的索引值
                    Integer start = stack.pop();
                    //System.out.println("弹出" + start);
                    //截取字符串进行计算
                    String substring = stringBuilder.substring(start, end + 1);
                    int operate;

                    operate = operate(substring);
                    if (operate == Integer.MIN_VALUE) {
                        return;
                    }

                    //System.out.println(substring);

                    //补空格,防止截取之后索引变化
                    int zeroNum = end - start + 1;
                    String zeroStr = "";
                    for (int j = 0; j < zeroNum - (operate + "").length(); j++) {
                        zeroStr += " ";
                    }
                    stringBuilder.replace(start, end + 1, zeroStr + operate);
                    //System.out.println(stringBuilder.toString() + "       当前字符串");
                }
            }

        }
    }

    public static int operate(String string) {
        String[] split = string.split(" ");
        int a = 0;
        int b = 0;
        //获取两个数的值
        for (int i = split.length - 1; i >= 1; i--) {
            if (i == split.length - 1) {
                b = Integer.parseInt(split[i].substring(0, split[i].length() - 1));
            } else {
                if (split[i] != null && !split[i].equals("")) {
                    a = Integer.parseInt(split[i]);
                }
            }
        }
        //乘
        if (string.contains("mul")) {
            return a * b;
            //加
        } else if (string.contains("add")) {
            return a + b;
            //减
        } else if (string.contains("sub")) {
            return a - b;
            //除
        } else if (string.contains("div")) {
            if (b == 0) {
                System.out.println("error");
                return Integer.MIN_VALUE;
            } else if (a % b != 0 && a / b < 0) {

                return a / b - 1;
            }
            return a / b;
        }
        return 0;
    }
}
