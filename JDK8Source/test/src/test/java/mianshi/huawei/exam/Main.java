package mianshi.huawei.exam;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = scanner.nextInt();
            }
            Arrays.sort(arr);
            int num = scanner.nextInt();
            if (num * 2 > arr.length) {
                System.out.println(-1);
            } else {
                //List<Integer> list1 = new ArrayList<>(num);
                //List<Integer> list2 = new ArrayList<>(num);
                Set<Integer> set = new HashSet<>(2 * num);
                int sum = 0;
                for (int i = 0; i < num; i++) {
                    //list1.add(arr[i]);
                    //list2.add(arr[arr.length - 1 - i]);
                    if (!set.add(arr[i])||!set.add(arr[arr.length - 1 - i])){
                        System.out.println(-1);
                        return;
                    }
                }
                for (Integer integer : set) {
                    sum += integer;
                }
                /*for (Integer integer : list1) {
                    if (list2.contains(integer)) {
                        System.out.println(-1);
                        return;
                    }
                }*/

                /*for (int i = 0; i < num; i++) {
                    sum += list1.get(i);
                    sum += list2.get(i);
                }*/
                System.out.println(sum);
            }


        }
    }
}
