package mianshi.huawei.medium;


import java.util.Scanner;

public class 正整数相加 {
    /* 超长正整数相加
请设计一个算法完成两个超长正整数的加法。
接口说明
请设计一个算法完成两个超长正整数的加法。
输入参数：
String addend：加数
String augend：被加数
返回值：加法结果
public String AddLongInteger(String addend, String augend)
{
   //实现
    return null;
}
输入描述:
输入两个字符串数字
输出描述:
输出相加后的结果，string型
        示例1
输入
99999999999999999999999999999999999999999999999999
    1
输出
100000000000000000000000000000000000000000000000000
 */
    public static void main(String[] args) {
/*        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            String next1 = scanner.next();
            System.out.println(AddLongInteger(next,next1));
        }*/
        System.out.println(AddLongInteger("89","111"));
    }

    public static String AddLongInteger(String addend, String augend) {
        char[] addCharArray = addend.toCharArray();
        char[] augendCharArray = augend.toCharArray();
        int addLength = addCharArray.length;
        int augentLength = augendCharArray.length;
        int[] resultArr = new int[Math.max(addLength, augentLength)];
        if (addLength < augentLength) {
            char[] chars = addend.toCharArray();
            addCharArray = augend.toCharArray();
            augendCharArray = chars;
        }

        int count = 0;
        for (int i = augendCharArray.length - 1; i >= 0; i--) {

            char addChar = addCharArray[i + Math.abs(addLength - augentLength)];
            char augentChar = augendCharArray[i];
            int result = addChar + augentChar - '0' - '0' + count;
            if (result > 9) {
                resultArr[i + Math.abs(addLength - augentLength)] = result % 10;
                count = 1;
            } else {
                resultArr[i + Math.abs(addLength - augentLength)] = result;
                count = 0;
            }
        }

        for (int i = Math.abs(addLength - augentLength) - 1; i >= 0; i--) {
            char addChar = addCharArray[i];
            int result = addChar - '0' + count;
            if (addChar - '0' > 9 - count) {

                resultArr[i] = result % 10;
                count = 1;
            } else {
                resultArr[i] = result;
                count = 0;
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (count == 1) {
            stringBuilder.append(1);
            for (int i = 0; i < resultArr.length; i++) {
                stringBuilder.append(resultArr[i]);
            }
        } else {
            for (int i = 0; i < resultArr.length; i++) {
                stringBuilder.append(resultArr[i]);
            }
        }
        return stringBuilder.toString();


    }

}
