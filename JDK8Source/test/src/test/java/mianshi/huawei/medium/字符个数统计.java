package mianshi.huawei.medium;

import java.util.Scanner;

public class 字符个数统计 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            test(next);
        }

    }

    public static void test(String str) {
        int[] arr = new int[128];
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i)]++;
        }
        int count = 0;
        for (int i : arr) {
            if (i > 0) {
                count++;
            }
        }
        System.out.println(count);

    }

}
