package mianshi.huawei.medium;


import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

public class Main {


    @Test
    public void test1() throws UnsupportedEncodingException {

    }

    public static void test(String str, int count)  {
        for (int i = 0; i < str.length(); i++) {
            String c = str.charAt(i) + "";
            int length = 0;
            try {
                length = c.getBytes("gbk").length;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            count = count - length;
            if (count < 0) {
                System.out.println(str.substring(0, i));
                return;
            } else if (count == 0) {
                if (i == str.length() - 1) {

                    System.out.println(str);
                } else {
                    System.out.println(str.substring(0, i + 1));
                }
                return;
            }
        }
        System.out.println(str);
    }

    @Test
    public void uakdgbbad() {
        System.out.println(test2("zhangsan"));
    }

    public static int test2(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (map.get(c) == null) {
                map.put(c, 1);
            } else {
                map.put(c, map.get(c) + 1);
            }
        }
        Set<Integer> set = new TreeSet<>();
        for (Character character : map.keySet()) {
            set.add(map.get(character));
        }
        int length = map.keySet().size();
        int count = 26 - length + 1;
        int sum = 0;
        for (Integer integer : set) {
            for (Character character : map.keySet()) {
                if (map.get(character).equals(integer)) {
                    sum += integer * count;
                    count++;
                }
            }
        }
        return sum;

    }





    /*字符个数统计
    如果统计的个数相同，则按照ASCII码由小到大排序输出 。如果有其他字符，则对这些字符不用进行统计。
    实现以下接口：
    输入一个字符串，对字符中的各个英文字符，数字，空格进行统计（可反复调用）
    按照统计个数由多到少输出统计结果，如果统计的个数相同，则按照ASII码由小到大排序输出
    清空目前的统计结果，重新统计
    调用者会保证：
    输入的字符串以‘\0’结尾。
    输入描述:
    输入一串字符。
    输出描述:
    对字符中的
    各个英文字符（大小写分开统计），数字，空格进行统计，并按照统计个数由多到少输出,如果统计的个数相同，则按照ASII码由小到大
    排序输出 。如果有其他字符，则对这些字符不用进行统计。
    示例1
    输入
    aadddccddc
    输出
    dca*/
    private static String getStr(String string) {
        if (string == null || string == "") {
            return "";
        }

        //保存字符串中字符出现的次数
        int[] arr = new int[128];
        for (int i = 0; i < string.length(); i++) {
            arr[string.charAt(i)]++;
        }
        Set<Integer> set = new TreeSet<>();

        //把数量添加到LinkedHashSet中
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                set.add(arr[i]);
            }
        }

        StringBuilder stringBuilder = new StringBuilder("");
        //从小到大遍历字符出现次数
        for (Integer integer : set) {
            //ASII码从大到小遍历
            for (int i = arr.length - 1; i >= 0; i--) {
                if (arr[i] == integer) {
                    if (i >= 48 && i <= 57) {
                        stringBuilder.append(i - 48);
                    } else {
                        stringBuilder.append((char) i);
                    }
//                    arr[i] = 0;
                }
            }
        }
        //反转
        return stringBuilder.reverse().toString();

    }

    public static void main(String[] args) throws IOException {
//        System.out.println((char)0);
        //8v26ktzk069lm400061m0v965we88850o6omqi532ktir6esb55t0kqm026y8rk63aj82kcx48gd1tiylvs0xo32zem65q7z5ce2185d2ascz62a2p3ajr45h637t2p290lc043gicp5ldzzmx2
        //206583kmzct4aeil19dopqrsvx7gjybhw  正确
        //ctbhw87gjy54aeil3kmz219dopqrsvx06  错误
/*        Scanner scanner = new Scanner(System.in);
        while (true) {

            if (scanner.hasNext()) {
                String next = scanner.next();
                System.out.println(getStr(next));
            }
        }*/
        /**
         * 输入输出
         */
        /*BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        while (true) {
            try {
                if ((line = br.readLine()) == null) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(getCubeRoot(Double.parseDouble(line)));
        }*/
//        System.out.println(GetResult(6,new int[]{2,5,1,5,4,5},null));

    }

    /*
    题目描述
    •计算一个数字的立方根，不使用库函数
    原型：
    public static double getCubeRoot(double input)
    输入:double 待求解参数
    返回值:double  输入参数的立方根，保留一位小数
    输入描述:
    待求解参数 double类型
    输出描述:
    输入参数的立方根 也是double类型
    示例1
    输入
    216
    输出
    6.0
    */
    public static double getCubeRoot(double input) {
        double sum = 0;
        double min = Double.MAX_VALUE;
        double result = 0;
        if (input > 0) {

            for (double i = 0; i <= input; i += 0.1) {
                sum = i * i * i;
                if (Math.abs(input - sum) < min) {
                    min = Math.abs(input - sum);
                    result = i;
                }
            }
        } else {
            for (double i = 0; i >= input; i -= 0.1) {
                sum = i * i * i;
                if (Math.abs(input - sum) < min) {
                    min = Math.abs(input - sum);
                    result = i;
                }
            }
        }
        BigDecimal bigDecimal = new BigDecimal(3);
//        BigDecimal addResult = bigDecimal.add(new BigDecimal(3.33));
        BigDecimal multiply = bigDecimal.multiply(bigDecimal).multiply(bigDecimal);
        System.out.println(multiply.doubleValue());
        DecimalFormat decimalFormat = new DecimalFormat("#.0");
        String format = decimalFormat.format(result);
        return Double.parseDouble(format);

    }







    /*
扑克牌游戏大家应该都比较熟悉了，一副牌由54张组成，含3~A、2各4张，小王1张，大王1张。牌面从小到大用如下字符和字符串表示（其中，小写joker表示小王，大写JOKER表示大王）：
3 4 5 6 7 8 9 10 J Q K A 2 joker JOKER
输入两手牌，两手牌之间用"-"连接，每手牌的每张牌以空格分隔，"-"两边没有空格，如：4 4 4 4-joker JOKER。
请比较两手牌大小，输出较大的牌，如果不存在比较关系则输出ERROR。
基本规则：
（1）输入每手牌可能是个子、对子、顺子（连续5张）、三个、炸弹（四个）和对王中的一种，不存在其他情况，由输入保证两手牌都是合法的，顺子已经从小到大排列；
（2）除了炸弹和对王可以和所有牌比较之外，其他类型的牌只能跟相同类型的存在比较关系（如，对子跟对子比较，三个跟三个比较），不考虑拆牌情况（如：将对子拆分成个子）；
（3）大小规则跟大家平时了解的常见规则相同，个子、对子、三个比较牌面大小；顺子比较最小牌大小；炸弹大于前面所有的牌，炸弹之间比较牌面大小；对王是最大的牌；
（4）输入的两手牌不会出现相等的情况。
输入描述:
输入两手牌，两手牌之间用"-"连接，每手牌的每张牌以空格分隔，"-"两边没有空格，如 4 4 4 4-joker JOKER。
输出描述:
输出两手牌中较大的那手，不含连接符，扑克牌顺序不变，仍以空格隔开；如果不存在比较关系则输出ERROR。
示例1
输入
4 4 4 4-joker JOKER
输出
joker JOKER
     */


    /* 计算字符串的相似度(字符串编辑距离)
对于不同的字符串，我们希望能有办法判断相似程度，我们定义了一套操作方法来把两个不相同的字符串变得相同，具体的操作方法如下：
1 修改一个字符，如把“a”替换为“b”。
2 增加一个字符，如把“abdd”变为“aebdd”。
3 删除一个字符，如把“travelling”变为“traveling”。
比如，对于“abcdefg”和“abcdef”两个字符串来说，我们认为可以通过增加和减少一个“g”的方式来达到目的。上面的两种方案，
都只需要一次操作。把这个操作所需要的次数定义为两个字符串的距离，而相似度等于“距离＋1”的倒数。也就是说，“abcdefg”和“abcdef”
的距离为1，相似度为1/2=0.5.
给定任意两个字符串，你是否能写出一个算法来计算出它们的相似度呢？
请实现如下接口
/功能：计算字符串的相似度
输入：pucAExpression/ pucBExpression：字符串格式，如: "abcdef"
返回：字符串的相似度,相似度等于“距离＋1”的倒数,结果请用1/字符串的形式,如1/2
public static  String  calculateStringDistance(String expressionA, String expressionB)
{
//请实现
return null;
}
约束：
1、PucAExpression/ PucBExpression字符串中的有效字符包括26个小写字母。
2、PucAExpression/ PucBExpression算术表达式的有效性由调用者保证;
3、超过result范围导致信息无法正确表达的，返回null。
输入描述:
输入两个字符串
输出描述:
输出相似度，string类型
示例1
输入
abcdef
abcdefg
输出
1/2
     */


    @Test
    public void testGetMax() {
        System.out.println(getMax(8, 4));
    }

    /*
    求最大公因子

     */
    public static int getMax(int a, int b) {
        int min = Math.min(a, b);
        int result = 1;
        for (int i = min; i >= 1; i--) {
            if (a % i == 0 && b % i == 0) {
                return i;
            }
        }
        return result;
    }

    /*
    求最小公倍数

     */

    /*
    题目描述
    找出字符串中第一个只出现一次的字符
    输入描述:
    输入一个非空字符串
    输出描述:
    输出第一个只出现一次的字符，如果不存在输出-1
    示例1
    输入
    复制
    asdfasdfo
    输出
    o
     */
    @Test
    public void testGetFirst() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line = null;
        while (true) {
            if ((line = bufferedReader.readLine()) != null) {
                System.out.println(getFirst("bbaac"));
            }
        }

    }

    public static char getFirst(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (map.get(c) != null) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        List<Character> list = new ArrayList<>();
        for (Character character : map.keySet()) {
            if (map.get(character) == 1) {
                list.add(character);
            }
        }
        if (list.size() == 0) {
            System.out.println(-1);
        }
        for (int i = 0; i < str.length(); i++) {
            if (list.contains(str.charAt(i))) {
                return str.charAt(i);
            }
        }
        return 'a';

    }


    /*

     */


}
