package mianshi.huawei.medium;

import java.util.Scanner;

/*
功能:等差数列 2，5，8，11，14。。。。

输入:正整数N >0

输出:求等差数列前N项和

返回:转换成功返回 0 ,非法输入与异常返回-1

 */
public class 等差数列 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {

            if (scanner.hasNext()) {
                String next = scanner.next();
                try {
                    int n = Integer.parseInt(next);
                    if (n <= 0) {
                        System.out.println(-1);

                    } else {
                        int sum = 0;
                        for (int i = 1; i <= n; i++) {
                            sum += getSum(i);

                        }
                        System.out.println(sum);
                    }

                } catch (Exception e) {
                    System.out.println(-1);
                }

            } else {
                break;
            }
        }
    }

    public static int getSum(int n) {
        if (n == 1) {
            return 2;
        }
        return 3 + getSum(n - 1);

    }
}
