package mianshi.huawei.medium;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
题目描述
找出字符串中第一个只出现一次的字符
输入描述:
输入一个非空字符串
输出描述:
输出第一个只出现一次的字符，如果不存在输出-1
示例1
输入
复制
asdfasdfo
输出
o
 */
public class Main1 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while (true) {

            if ((line = bufferedReader.readLine()) != null) {
//                System.out.println(getFirst(line));
                printNum(Integer.parseInt(line));
            } else {
                break;
            }
        }
    }

    public static String getFirst(String str) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (map.get(c) != null) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        List<Character> list = new ArrayList<>();
        for (Character character : map.keySet()) {
            if (map.get(character) == 1) {
                list.add(character);
            }
        }
        if (list.size() == 0) {
            return "-1";
        }
        for (int i = 0; i < str.length(); i++) {
            if (list.contains(str.charAt(i))) {
                return str.charAt(i) + "";
            }
        }
        return "-1";

    }

    /**
     * 24点游戏算法
     * 问题描述：给出4个1-10的数字，通过加减乘除，得到数字为24就算胜利
     * 输入：
     * 4个1-10的数字。[数字允许重复，但每个数字仅允许使用一次，测试用例保证无异常数字]
     * 输出：
     * true or false
     * 输入描述:
     * 输入4个int整数
     * 输出描述:
     * 返回能否得到24点，能输出true，不能输出false
     * 示例1
     * 输入
     * 7 2 1 10
     * 输出
     * true
     */



    /*查找两个字符串a,b中的最长公共子串。
    若有多个，输出在较短串中最先出现的那个。
    输入描述:
    输入两个字符串
    输出描述:
    返回重复出现的字符
    示例1
    输入
    abcdefghijklmnop
    abcsafjklmnopqrstuvw
    输出
    jklmnop*/
    @Test
    public void testGetMaxChild() {
        getMaxChild("yrtqyfxyrmbasfmkbuudetaahxxgvcpkfhlkfxtjvguizsmwbnwamftshffyzumqfzqvirxgjjuocobvhvgstvrynduavkvntvxgnravjyfjkycguqyrnbnwnoqvhh",
                "xxzjrwyqtgzfgxyitvszmltcsdjweeycqgzsazahpqrvlgvwexcfwkusmuyltvtbjftkvwebmjctwbfcxfimoevbquznojlzkxygruhebhostshenguhymzjxhkjstiwzgyudtfeddgqlegxesngnlbubkhzfmspalfajiqsvohghxhswjiimnyazfmgqazdewfptldiilrwkhuntvseohykutjecuhg"
        );
    }

    public static void getMaxChild(String string1, String string2) {
        if (string1.length() < string2.length()) {
            String str = string2;
            string2 = string1;
            string1 = str;//长串

        }
        String maxStr = "";
        //int right = 0;
        for (int left = 0; left < string2.length(); left++) {
            int right = left;
            String childStr = string2.substring(left, right);
            while (right < string2.length() && string1.contains(childStr)) {

                maxStr = maxStr.length() >= childStr.length() ? maxStr : childStr;
                childStr = string2.substring(left, right);
                right++;
            }


        }
        System.out.println(maxStr);

    }


    /* 查找组成一个偶数最接近的两个素数
    任意一个偶数（大于2）都可以由2个素数组成，组成偶数的2个素数有很多种情况，本题目要求输出组成指定偶数的两个素数差值最小的素数对
    输入描述:
    输入一个偶数
    输出描述:
    输出两个素数
    示例1
    输入
    20
    输出
    7
    13*/
//    @Test
    public void testPrintNum() throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while (true) {
            if ((line = bufferedReader.readLine()) == null) {
                printNum(Integer.parseInt(line));
            } else {
                break;
            }
        }

    }

    public static void printNum(int number) {
        int num = number / 2;
        int left = num;
        int right = num;
        while (left > 0) {
            if (isPri(left) && isPri(right)) {
                System.out.println(left);
                System.out.println(right);
                break;
            }
            left--;
            right++;
        }
    }

    public static boolean isPri(int num) {
        int count = 0;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                count++;
            }
        }
        return count == 2 || num == 1 || num == 2;
    }
}

