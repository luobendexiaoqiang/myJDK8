package mianshi.huawei.medium;

import java.util.Scanner;
/* n = 5
1 3 6 10 15
2 5 9 14
4 8 13
7 12
11
 */
public class 蛇形矩阵 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            test(n);
        }

    }


    public static void test(int n) {
        for (int i = 1; i <= n; i++) {
            printNum(getWidthNum(i), n, i);
            System.out.println();
        }


    }

    /**
     * 获取每一行首位的值
     * @param n
     * @return
     */
    public static int getWidthNum(int n) {
        if (n == 1) {
            return 1;
        }
        return n - 1 + getWidthNum(n - 1);
    }

    /**
     *
     * @param a 每一行的第一位的值
     * @param b 打印的总行数
     * @param j 第j行
     */
    public static void printNum(int a, int b, int j) {
        System.out.print(a);
        System.out.print(" ");
        for (int i = j + 1; i <= b; i++) {
            a = i + a;
            System.out.print(a);
            if (i != b) {
                System.out.print(" ");
            }
        }
    }
}
