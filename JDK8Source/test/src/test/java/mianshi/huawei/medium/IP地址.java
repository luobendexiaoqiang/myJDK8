package mianshi.huawei.medium;


import java.util.Scanner;

public class IP地址 {
    /*
现在IPV4下用一个32位无符号整数来表示，一般用点分方式来显示，点将IP地址分成4个部分，
每个部分为8位，表示成一个无符号整数（因此不需要用正号出现），如10.137.17.1，是我们非常熟悉的IP地址，
一个IP地址串中没有空格出现（因为要表示成一个32数字）。
现在需要你用程序来判断IP是否合法。
输入描述:
输入一个ip地址
输出描述:
返回判断的结果YES or NO
示例1
输入
复制
10.138.15.1
输出
YES
 */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            System.out.println(isIpAddress(next));
        }
    }

    public static String isIpAddress(String str) {
        String[] split = str.split("\\.");
        if (split.length > 4) {
            return "NO";
        }
        for (String number : split) {
            try {
                Integer integer = Integer.valueOf(number);
                if (integer > 255 || integer < 0) {
                    return "NO";
                }
            } catch (Exception e) {
                return "NO";
            }

        }

        return "YES";
    }
}
