package mianshi.huawei.medium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 合并表记录_输入多行 {

    public static void main(String[] args) throws IOException {
        /*BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;*/
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {
                Integer length = scanner.nextInt();
                Map<Integer, Integer> map = new HashMap<>(length);
                for (int i = 0; i < length; i++) {
                    /*String str = bufferedReader.readLine();
                    String[] arr = str.split(" ");
                    int key = Integer.parseInt(arr[0]);
                    int value = Integer.parseInt(arr[1]);*/
                    int key = scanner.nextInt();
                    int value = scanner.nextInt();
                    if (map.get(key) == null) {
                        map.put(key, value);
                    } else {
                        map.put(key, map.get(key) + value);
                    }

                }
                for (Integer integer : map.keySet()) {
                    System.out.println(integer + " " + map.get(integer));
                }
            }
        }


    }
