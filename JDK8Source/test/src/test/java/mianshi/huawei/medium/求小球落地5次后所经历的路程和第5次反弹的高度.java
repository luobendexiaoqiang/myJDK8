package mianshi.huawei.medium;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Scanner;

/**
 * 假设一个球从任意高度自由落下，每次落地后反跳回原高度的一半; 再落下, 求它在第5次落地时，共经历多少米?第5次反弹多高？
 * 最后的误差判断是小数点6位
 * 输入描述:
 * 输入起始高度，int型
 * 输出描述:
 * 分别输出第5次落地时，共经过多少米第5次反弹多高
 * 示例1
 * 输入
 * 复制
 * 1
 * 输出
 * 复制
 * 2.875
 * 0.03125
 */
public class 求小球落地5次后所经历的路程和第5次反弹的高度 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int height = scanner.nextInt();
            test(height);
        }
    }

    public static void test(int height) {
        //BigDecimal bigDecimal = new BigDecimal(height);
        double finalHeight = height;
        double sum = 0;

        for (int i = 1; i <= 5; i++) {
            sum += finalHeight;
            finalHeight = finalHeight / 2.0;
            if (i != 5) {

                sum += finalHeight;
            }
        }
        DecimalFormat decimalFormat = new DecimalFormat("0.######");
        System.out.println(decimalFormat.format(sum));
        System.out.println(decimalFormat.format(finalHeight));

    }

}
