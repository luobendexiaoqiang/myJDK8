package mianshi.huawei.medium;

import org.junit.Test;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class 表示数字 {

    /* 表示数字
将一个字符中所有出现的数字前后加上符号“*”，其他字符保持不变
public static String MarkNum(String pInStr)
{
return null;
}
注意：输入数据可能有多行
输入描述:
输入一个字符串
输出描述:
字符中所有出现的数字前后加上符号“*”，其他字符保持不变
示例1
输入
Jkdi234klowe90a3
输出
Jkdi*234*klowe*90*a*3*
     */
    public static void main(String[] args) {

//        StringBuilder stringBuilder = new StringBuilder("aaaaa");
//        stringBuilder.replace(1, 1, "hello");
//        System.out.println(stringBuilder.toString());
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            System.out.println(MarkNum(next));
        }

    }

    public static String MarkNum(String pInStr) {
        Set<Integer> set = new TreeSet<>();

        for (int i = 0; i < pInStr.length(); i++) {
            char c = pInStr.charAt(i);
            if (Character.isDigit(c)) {
                set.add(i);
                while (Character.isDigit(c)) {
                    i++;
                    if (i < pInStr.length()) {
                        c = pInStr.charAt(i);
                    } else {
                        break;
                    }
                }
                set.add(i);
            }

        }
        StringBuilder stringBuilder = new StringBuilder(pInStr);
        int count = 0;
        for (Integer integer : set) {
            stringBuilder.replace(integer + count, integer + count, "*");
            count++;
        }

        return stringBuilder.toString();
    }

}
