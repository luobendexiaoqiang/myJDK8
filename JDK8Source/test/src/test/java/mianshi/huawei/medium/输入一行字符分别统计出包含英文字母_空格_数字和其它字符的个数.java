package mianshi.huawei.medium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class 输入一行字符分别统计出包含英文字母_空格_数字和其它字符的个数 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while (true) {
            if ((line = bufferedReader.readLine()) != null) {
                test2(line);
            } else {
                break;
            }
        }

        /*Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String next = scanner.next();
            test(next);
        }*/
    }

    /**
     * 输入一行字符分别统计出包含英文字母_空格_数字和其它字符的个数
     * @param str
     */
    public static void test(String str) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (Character.isDigit(c)) {
                map.put("number", map.get("number") == null ? 1 : map.get("number") + 1);
            } else if (Character.isLetter(c)) {
                map.put("letter", map.get("letter") == null ? 1 : map.get("letter") + 1);
            } else if (c == ' ') {
                map.put("null", map.get("null") == null ? 1 : map.get("null") + 1);
            } else {
                map.put("other", map.get("other") == null ? 1 : map.get("other") + 1);
            }
        }
        System.out.println(map.get("letter")==null?0:map.get("letter"));
        System.out.println(map.get("null")==null?0:map.get("null"));
        System.out.println(map.get("number")==null?0:map.get("number"));
        System.out.println(map.get("other")==null?0:map.get("other"));
    }

    /**
     * 将字符串中的字符按照ASCII码值从小到大排序好
     */
    public static void test2(String str) {
        int[] arr = new int[128];
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i)]++;
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > 0) {
                for (int j = 0; j < arr[i]; j++) {
                    stringBuilder.append((char) i);
                }
            }
        }
        System.out.println(stringBuilder.toString());

    }

}
