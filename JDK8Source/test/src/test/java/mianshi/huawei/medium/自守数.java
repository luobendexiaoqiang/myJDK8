package mianshi.huawei.medium;

import java.util.Scanner;

/*
题目描述
自守数是指一个数的平方的尾数等于该数自身的自然数。例如：25^2 = 625，76^2 = 5776，9376^2 = 87909376。请求出n以内的自守数的个数
接口说明
功能: 求出n以内的自守数的个数
输入参数：
int n
返回值：
n以内自守数的数量。
public static int CalcAutomorphicNumbers( int n)
{
//在这里实现功能
return 0;
}
本题有多组输入数据，请使用while(cin>>)等方式处理
输入描述:
int型整数
输出描述:
n以内自守数的数量。
示例1
输入
2000
输出
8
 */
public class 自守数 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int n = scanner.nextInt();
            System.out.println(CalcAutomorphicNumbers(n));
        }

    }

    public static int CalcAutomorphicNumbers(int n) {
        int count = 0;
        for (int i = 0; i <= n; i++) {

            int num = i * (i - 1);
            int length = (i + "").length();
//        System.out.println(num);
            String substring = (num + "").substring((num + "").length() - length);
//        System.out.println(substring);
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < length; j++) {
                stringBuilder.append("0");
            }
            if (stringBuilder.toString().equals(substring)) {
                count++;
            }
        }

//在这里实现功能
        return count;
    }
}
