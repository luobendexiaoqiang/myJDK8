package mianshi.leetcode.medium;

public class 盛最大水的容器11 {
    /**
     * 盛最多水的容器
     *
     * @param height
     * @return
     */
    public static int maxArea(int[] height) {

        if (height == null || height.length <= 1) {
            return 0;
        }
        int max = 0;
        int s;

        for (int i = 0; i < height.length; i++) {
            for (int j = i + 1; j < height.length; j++) {
                s = Math.min(height[i], height[j]) * (j - i);
                max = Math.max(s, max);

            }
        }
        return max;

    }

}
