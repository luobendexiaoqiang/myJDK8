package mianshi.leetcode.easy;

public class x的平方根69 {
    /**
     * 超时
     * @param x
     * @return
     */
    public static int sqrt(int x) {
        if (x == 0) {
            return 0;
        }
        if (x <= 3) {
            return 1;
        }
        if (x <= 5) {
            return 2;
        }
        for (int i = 1; i <= x / 2; i++) {
            if (i * i == x) {
                return i;
            } else if (i * i > x) {
                return i - 1;
            }
        }
        return 0;

    }


}
