package mianshi.leetcode.dp.medium;

import java.util.ArrayList;
import java.util.List;

public class 二叉树遍历 {

    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> list = new ArrayList<>();
        preTest(root, list);

        return list;
    }

    /**
     * 先序遍历(递归)
     *
     * @param root
     * @return
     */
    private void preTest(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        preTest(root.left, list);
        preTest(root.right, list);
    }

    /**
     * 中序遍历(递归)
     *
     * @param root
     * @return
     */
    private void midTest(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        midTest(root.left, list);
        list.add(root.val);
        midTest(root.right, list);
    }

    /**
     * 后序遍历(递归)
     *
     * @param root
     * @return
     */
    private void lastTest(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        lastTest(root.left, list);
        lastTest(root.right, list);
        list.add(root.val);
    }


    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }

}
