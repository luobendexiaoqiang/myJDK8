package mianshi.leetcode.dp.easy;

import java.util.HashMap;
import java.util.Map;

/*
70. 爬楼梯
假设你正在爬楼梯。需要 n 阶你才能到达楼顶。

每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

注意：给定 n 是一个正整数。

示例 1：

输入： 2
输出： 2
解释： 有两种方法可以爬到楼顶。
1.  1 阶 + 1 阶
2.  2 阶
示例 2：

输入： 3
输出： 3
解释： 有三种方法可以爬到楼顶。
1.  1 阶 + 1 阶 + 1 阶
2.  1 阶 + 2 阶
3.  2 阶 + 1 阶
 */
public class 爬楼梯70 {
    /**
     * 线性动态规划
     * @param n
     * @return
     */
    public int climbStairs(int n) {


        if (n <= 0) {
            return 0;
        }
        if (n <= 2) {
            return n;
        }
        int[] dp = new int[n];
        //爬到第一格时,只有一种方法
        dp[0] = 1;
        //爬到第二格时,有两种方法
        dp[1] = 2;
        //爬到第n(n>2)格时,可以从n-1格和n-2格,直接到达,因此,总共有dp[n-1]+dp[n-2]中方法
        for (int i = 2; i < n; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n - 1];
    }


    public static void main(String[] args) {
        System.out.println(climbStairs1(45, new HashMap<>()));
        System.out.println(climbStairs2(45));
    }

    /**
     * 备忘录算法，将计算过的值存入map中，防止重复计算
     * @param n
     * @param map
     * @return
     */
    public static int climbStairs1(int n, Map<Integer, Integer> map) {
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        if (map.get(n) != null) {
            return map.get(n);
        } else {
            int value = climbStairs1(n - 1, map) + climbStairs1(n - 2, map);
            map.put(n, value);
            return value;
        }
    }

    /**
     * 迭代方式
     * @param n
     * @return
     */
    public static int climbStairs2(int n) {

        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        int a = 1;
        int b = 2;
        int c = 0;
//        int sum = a + b;
        for (int i = 3; i <= n; i++) {
            c = a + b;
//            sum += c;
            a = b;
            b = c;
        }
        return c;

    }
}
