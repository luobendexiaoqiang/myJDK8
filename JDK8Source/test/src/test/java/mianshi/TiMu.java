package mianshi;

public class TiMu {
        /*
        为什么String定义为final。
    描述下java集合，项目中用到哪些。
    StringBuilder和StringBuffer的区别。
    HashMap中常用的方法有哪些，什么时候会触发树化，jdk1.7和1.8实现的差异，1.7的实现为什么会出现死锁，画图说明下。HashMap和TreeMap的区别。
    说下枚举类型，底层实现原理，项目中是如何使用的。
    详细描述Error和Exception(运行期和编译期)的区别。
    创建线程的方式，线程的生命周期。
    ThrealLocal实现原理，为什么会出现内存泄漏。
    volatile关键字原理，项目中是如何使用的。
    synchronized和lock的区别，底层实现原理。AQS队列实现原理，用了哪些设计模式。公平锁和非公平锁、独占锁和共享锁、读写锁分别是如何实现的，为什么说非公平锁比公平锁性能高。
    序列化和反序列化。
    深拷贝和浅拷贝区别。
    java内部类的区别(成员内部类、静态嵌套类、方法内部类、匿名内部类 )。
    java线程池参数描述，线程池工作原理，线程池如何调优。
    主线程到达一个条件，需要多个子线程去执行任务，等子任务都执行完后再往下走，如何编写代码(CountDownLatch)。
    写个程序，两个线程交叉打印1到100的数字，需要多种实现方式。
    JVM
    JVM运行时数据区域和内存模型描述，jdk8为什么移除方法区。
    垃圾回收算法和垃圾回收器描述，在工作中，新生代和老年代分别用的什么垃圾回收器。
    新生代和老年代什么时候会触发GC。
    四种引用区别。
    CMS垃圾回收过程描述，CMS有哪些缺点，对比G1。
    GC调优步骤，有实操过吗。
    描述下JVM类加载过程，如何自定义类加载器。
    描述下双亲委派模型，为什么需要双亲委派模型。
    泛型是如何实现的，逃逸分析知道吗，说下。
    OOM、内存泄漏如何排查，用到哪些工具，如果不用工具如何进行定位。
    机器负载变高如何排查，如果发现是jvm进程引起的，如何定位到代码行。
    Spring框架
    Spring框架用到了哪些设计模式。
    Spring生命周期详细描述。
    Spring是如何解决循环依赖的。
    Spring扩展点有哪些，项目中是如何应用的。
    Spring IOC、AOP描述。
    Spring事务和MySQL事务的区别，Spring事务传播机制介绍，Spring事务失效和解决方案。
    Spring全局异常捕获如何编写。
    AOP动态代理实现：jdk动态代理和cglib实现差异，cglib性能为什么比jdk动态代理性能高，Fastclass机制描述下，哪些方法不能被动态代理。
    AOP失效举例，为什么会失效，如何解决。
    BeanFactory和FactoryBean的区别。
    Spring创建了单例对象，如果多线程并发对属性赋值，造成相互覆盖的情况，如何处理。
    SpringMVC和SpringBoot的区别。
    MySQL
    事务描述，ACID讲解。
    事务隔离级别描述，脏读、不可重复读、幻读区别，MVCC机制讲解。
    Innodb如何解决幻读，间隙锁实现详细描述。
    left join和inner join的区别，嵌套子查询如何优化。
    如果线上出现慢sql，如何定位和解决，有实际动手优化过慢sql吗。
    binlog机制描述，binlog日志格式有哪些。
    MySQL主从架构(读写分离)，主从数据复制过程，数据复制过程丢失如何处理。
    分库分表如何实现，用过哪些分库分表插件，底层原理是怎样的。
    索引有哪些种类，建立索引的原则，聚簇索引和非聚簇索引实现区别，联合索引如何使用。
    mysql写入数据的时候，是先把数据写到缓冲区，然后再flush到磁盘的，如何在flush过程中发生了宕机，数据如何恢复。
    Redis
    redis数据类型，说下跳跃表是如何实现的，可以用什么数据结构替换。
    删除过期key策略有哪些，内存淘汰策略有哪些，分别什么时候触发。
    redis线程模型和内存模型。
    redis持久化机制。
    redis集群方案。
    让你设计一个redis，你会怎么做，有看过redis源码吗。
    了解一致性hash算法吗，描述下。
    用redis实现一个分布式锁。
    缓存穿透、缓存击穿、缓存雪崩区别和解决方案。
    布隆过滤器知道吗，说下原理。
    Dubbo
    描述一下rpc调用过程。
    让你实现一个rpc框架，你会怎么做。
    链路跟踪和熔断机制了解吗，框架层如何实现的。
    了解哪些序列化协议，有什么区别，项目中用的是什么协议。
    说下Netty，bio、nio、aio区别，select、poll、epoll区别，什么是零拷贝机制。
    Kafka
    Kafka、RabbitMQ、RocketMQ区别，为什么RabbitMQ时延最低，知道事务消息吗。
    Kafka生产者、消费者、协调者、服务端工作机制，描述数据从生产端到消费端到过程。
    如果出现数据丢失或者数据重复消费如何处理。
    Kafka为什么高吞吐量。
    Kafka是如何实现exactly once语义的。
    让你设计一个消息队列，你会怎么设计。
    Zookeeper
    zookeeper节点类型、服务器角色，watch机制。
    描述下ZAB协议。
    应用场景。
    使用zookeeper实现分布式锁和读写锁。
    算法编程
    无重复字符的最长子串
    二叉树的直径
    二叉树最大宽度
    寻找旋转排序数组中的最小值
    旋转链表
    LRU缓存机制
    数据流的中位数
    搜索旋转排序数组
    设计模式
    单例模式：多种实现方式，double check实现原理，枚举类实现(枚举类为什么不能被反射)
    模版方法设计模式：工程中的应用
    静态代理和动态代理设计模式
    装饰器模式
    适配器模式
    工程方法模式
    责任链模式

    1.拿到一个慢sql，你是怎么做sql优化的
    3.什么时候要分库分表，分库分表会造成哪些问题？应该怎么解决
    4.有一个比较长的String字段要存到mysql里，并在该列建立索引。你会怎么做？
    5.如果这个String列，不同的string之间有大量的重复片段，区分度不高。你会如何优化？
    6.uuid做主键，有什么坏处？
    7.你平时用什么java集合类，都有哪些map子类？
    8.说下treemap使用场景，说下hashmap原理
    9.说下GC吧。什么情况会触发minor gc，什么情况会触发full gc？
    10.hashcode值到底代表什么，gc后hashcode值会改变吗？为什么
    1.sql优化说一下，你是怎么做的
    2.说一下你曾经做的sql优化案例，踩了哪些坑？
    9.concurrentHashmap底层，他是怎么实现的
    10.treemap底层是什么结构？
    2.我要建一个10个线程的线程池，一般怎么建立？有什么注意点？
    code：简单版本的编辑距离，输入两个字符串如abcdeee与abceee，从任意一个字符串中增/删/改一个
    字符，能否使两个字符串相等？输出true/false
    1.你说下你怎么做sql优化的
    2.你说到了not in，为什么not in很慢？那么该怎么优化呢，为什么这么改？
    3.jvm调优有做过吗？项目中用的什么收集器？
    4.用过哪些jvm参数
    7.redis如何搜索以某一后缀结尾的所有key？
    8.你提到的命令keys，为什么会造成卡顿5.jvm中哪里可能出现OOM，遇到OOM怎么排查？
    6.项目中踩坑经历，问细节
    1.追问项目细节，sql调优
    2.分区表有哪些注意点？适用于什么情况？
    3.死锁如何形成的？mysql怎么排查死锁，怎么处理死锁。
    4.四个隔离级别说一下，InnoDB引擎如何保障RR下不出现幻读的？（追问）什么是MVCC，什么是next-key锁？
    5.redis缓存穿透是什么，如何避免
    6.redis如何避免缓存击穿，分布式锁怎么做，怎么保证原子性？
    7.springboot了解吧，某一个Service类使用了注解开启事务，那么该类下的两个方法A，B。A方法调用了B方法，会走事务逻辑吗？为什么。
    8.springboot环境下，设计一个线程池，要求传入的任务有优先级。优先级高的先加载，优先级一样的先进先出

*/

}
