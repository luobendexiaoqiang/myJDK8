/***** Lobxxx Translate Finished ******/
/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 * <p>
 *  由Doug Lea在JCP JSR-166专家组成员的帮助下撰写,并发布到公共领域,如http://creativecommons.org/publicdomain/zero/1.0/
 * 
 */

package java.util;

/**
 *  集合设计用于在处理之前保持元素。除了基本的{@link java.util.Collection Collection}操作,队列还提供额外的插入,提取和检查操作。
 * 这些方法中的每一种都以两种形式存在：一个在操作失败时抛出异常,另一个返回特殊值(根据操作,{@code null}或{@code false})。
 * 插入操作的后一种形式是专为使用容量限制的{@code Queue}实现而设计的;在大多数实现中,插入操作不会失败。

 * <p>队列通常(但不一定)以FIFO(先进先出)方式对元素排序。在例外中,优先级队列,其根据提供的比较器或元素的自然排序的顺序元素,以及排序元素LIFO(后进先出)的LIFO队列(或堆栈)。
 * 无论使用什么排序,队列的<em>头</em>都是通过调用{@link #remove()}或{@link #poll()}删除的元素。在FIFO队列中,所有新元素都插入到队列的<em>尾</em>。
 * 其他类型的队列可以使用不同的布局规则。每个{@code Queue}实施必须指定其排序属性。
 * 
 *  <p> {@link #offer offer}方法尽可能插入元素,否则返回{@code false}。
 * 这与{@link java.util.Collection#add Collection.add}方法不同,后者可能无法仅通过抛出未检查的异常来添加元素。
 *  {@code offer}方法被设计用于当故障是例如在固定容量(或"有界")队列中的正常而不是异常事件时使用。
 * 
 * <p> {@link #remove()}和{@link #poll()}方法移除并返回队列头。从队列中删除哪个元素是队列的排序策略的函数,其从实现到实现是不同的。
 *  {@code remove()}和{@code poll()}方法的区别仅在于队列为空时的行为：{@code remove()}方法引发异常,而{@code poll方法返回{@code null}。
 * <p> {@link #remove()}和{@link #poll()}方法移除并返回队列头。从队列中删除哪个元素是队列的排序策略的函数,其从实现到实现是不同的。
 */
public interface Queue<E> extends Collection<E> {
    /**
     *  <p> {@link #element()}和{@link #peek()}方法返回,但不删除,队列头。
     *  <p> {@code Queue}接口未定义<i>阻塞队列方法</i>,这在并发编程中很常见。
     * 这些方法等待元素出现或空间变得可用,在{@link java.util.concurrent.BlockingQueue}接口中定义,这扩展了此接口。
     *  <p> {@ code Queue}实现通常不允许插入{@code null}元素,但某些实现(例如{@link LinkedList})不会禁止插入{@code null}。
     * 即使在允许它的实现中,{@code null}也不应该插入{@code Queue},因为{@code null}也被{@code poll}方法用作特殊返回值,表示队列不包含任何元素。
     * <p> {@ code Queue}实现通常不定义方法{@code equals}和{@code hashCode}的基于元素的版本,而是继承类{@code Object}的基于身份的版本,因为基于元素
     * 的等式并不总是为具有相同元素但排序属性不同的队列定义良好。
     */
    boolean add(E e);

    /**
     *  如果可以在不违反容量限制的情况下立即执行此操作,则将指定的元素插入到此队列中,
     * 如果成功则返回{@code true},如果当前没有可用空间,则抛出{@code IllegalStateException}
     */
    boolean offer(E e);

    /**
     *  如果可以在不违反容量限制的情况下立即执行,将指定的元素插入此队列。当使用容量限制队列时,此方法通常优先于{@link #add},这可能无法仅通过抛出异常插入元素。
     */
    E remove();

    /**
     *  检索并删除此队列的头。此方法与{@link #poll poll}不同之处仅在于,如果此队列为空,它会抛出异常。
     */
    E poll();

    /**
     *  检索并删除此队列的头部,如果此队列为空,则返回{@code null}。
     */
    E element();

    /**
     *  检索,但不删除,这个队列的头。此方法与{@link #peek peek}的区别仅在于,如果此队列为空,它会抛出异常。
     */
    E peek();
}
