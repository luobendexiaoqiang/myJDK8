/***** Lobxxx Translate Finished ******/
/*
 * ORACLE PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

/*
 *
 *
 *
 *
 *
 * Written by Doug Lea with assistance from members of JCP JSR-166
 * Expert Group and released to the public domain, as explained at
 * http://creativecommons.org/publicdomain/zero/1.0/
 * <p>
 *  由Doug Lea在JCP JSR-166专家组成员的帮助下撰写,并发布到公共领域,如http://creativecommons.org/publicdomain/zero/1.0/
 *
 */

package java.util.concurrent.locks;
import java.util.concurrent.TimeUnit;
import java.util.Collection;

/**
 *  <p>此类别不强制读取者或写入者偏好顺序锁定访问。但是,它支持可选的公平性</em>政策。
 *
 * <dl>
 *  <dt> <b> <i>非公平模式(默认)</i> </b> <dd>当构造为非公平(默认)时,读写锁的顺序未指定,受到重入限制。
 * 持续竞争的非锁定可以无限期地推迟一个或多个读取器或写入器线程,但通常将具有比公平锁定更高的吞吐量。
 *
 *  <dt> <b> <i>公平模式</i> </b> <dd>当构建为公平时,线程使用大致到达顺序策略来争用条目。
 * 当当前持有的锁被释放时,最长等待的单个写入程序线程将被分配写锁定,或者如果有一组读取程序线程等待的时间比所有等待的写程序线程长,则该组将被分配读锁定。
 *
 * <p>尝试获取公平读取锁(不可重入)的线程将阻塞,如果写锁定被保持,或有一个等待写线程。线程将不会获取读锁,直到最老的当前等待的写线程已经获取并释放写锁定。
 * 当然,如果等待的写入器放弃等待,留下一个或多个读取器线程作为队列中的最长等待者,并且写锁定是空闲的,则那些读取器将被分配读取锁定。
 *
 *  <p>尝试获取公平写锁定(不可重写)的线程将阻塞,除非读锁和写锁都是空闲的(这意味着没有等待的线程)。
 *  (请注意,非阻塞{@link ReadLock#tryLock()}和{@link WriteLock#tryLock()}方法不遵守此公平设置,如果可能,将立即获取锁,而不管等待的线程。
 * <p>
 * </dl>
 *
 *  <li> <b>重入</b>
 *
 *  <p>此锁定允许读者和作者以{@link ReentrantLock}的风格重新获取读取或写入锁定。在写线程持有的所有写锁都被释放之前,不允许非可重入读写器。
 *
 *  <p>此外,写入器可以获取读取锁,但反之亦然。在其他应用程序中,如果在调用期间保持写锁定或在执行读锁定下执行读取的方法的回调时,可重入性会很有用。如果读取器尝试获取写锁定,它将永远不会成功。
 *
 * <li> <b>锁定降级</b> <p>重入还允许通过获取写锁定,然后读取锁定,然后释放写锁定,从写锁降级为读锁。但是,从读取锁升级到写入锁是<b>不</b>可能。
 *
 *  <li> <b>锁获取中断</b> <p>读锁和写锁都支持在锁获取期间中断。
 *
 *  <li> <b> {@ link Condition}支持</b> <p>写锁定提供了一个{@link Condition}实现,其行为方式与写锁定相同,如{@link Condition } {@link ReentrantLock#newCondition}
 * 提供的实现适用于{@link ReentrantLock}。
 * 当然,{@link Condition}只能用于写锁定。
 *
 *  <p>读锁不支持{@link Condition}和{@code readLock()。
 *  <li> <b>测试</b> <p>此类支持确定锁是否被持有或被争用的方法。这些方法被设计用于监视系统状态,而不是用于同步控制。
 *  <p>此类的序列化与内置锁的行为相同：反序列化锁处于解锁状态,无论序列化时的状态如何。
 *
 *  <p> <b>示例用法</b>。这里是一个代码草图,显示了如何在更新缓存后执行锁定降级(当以非嵌套方式处理多个锁定时,异常处理特别棘手)：
 *  ReentrantReadWriteLocks可以用来提高某些类型的集合的一些使用中的并发性。
 * 这通常是值得的,只有当预期集合是大的,由更多的读取器线程访问,而不是写线程,并且需要超过同步开销的开销的操作。例如,这里是一个使用TreeMap的类,它期望大并且被并发访问。
 *  <h3>实施注意事项</h3>
 *
 *  <p>此锁最多支持65535个递归写锁和65535个读锁。尝试超过这些限制会导致{@link Error}抛出锁定方法。
 *
 *
 * @since 1.5
 * @author Doug Lea
 */
public class ReentrantReadWriteLock
        implements ReadWriteLock, java.io.Serializable {
    private static final long serialVersionUID = -6992448646407690164L;
    /** Inner class providing readlock */
    private final ReentrantReadWriteLock.ReadLock readerLock;
    /** Inner class providing writelock */
    private final ReentrantReadWriteLock.WriteLock writerLock;
    /** Performs all synchronization mechanics */
    final Sync sync;

    /**
     *  使用默认(非正式)排序属性创建新的{@code ReentrantReadWriteLock}。
     *
     */
    public ReentrantReadWriteLock() {
        this(false);
    }

    /**
     *  根据给定的公平策略创建新的{@code ReentrantReadWriteLock}。
     *
     *
     * @param fair {@code true} if this lock should use a fair ordering policy
     */
    public ReentrantReadWriteLock(boolean fair) {
        sync = fair ? new FairSync() : new NonfairSync();
        readerLock = new ReadLock(this);
        writerLock = new WriteLock(this);
    }

    public ReentrantReadWriteLock.WriteLock writeLock() { return writerLock; }
    public ReentrantReadWriteLock.ReadLock  readLock()  { return readerLock; }

    /**
     *  ReentrantReadWriteLock的同步实现。细分为公平和非商业版本。
     *
     */
    abstract static class Sync extends AbstractQueuedSynchronizer {
        private static final long serialVersionUID = 6317671515068378041L;

        /*
         *  读取vs写入计数提取常数和函数。锁定状态在逻辑上分为两个无符号短路：下面的一个代表独占(写)器锁定保持计数,而上部是共享(读取器)保持计数。
         */

        static final int SHARED_SHIFT   = 16;
        static final int SHARED_UNIT    = (1 << SHARED_SHIFT);
        static final int MAX_COUNT      = (1 << SHARED_SHIFT) - 1;
        static final int EXCLUSIVE_MASK = (1 << SHARED_SHIFT) - 1;

        /** Returns the number of shared holds represented in count  */
        static int sharedCount(int c)    { return c >>> SHARED_SHIFT; }
        /** Returns the number of exclusive holds represented in count  */
        static int exclusiveCount(int c) { return c & EXCLUSIVE_MASK; }//低16位为写锁数量

        /**
         *  用于每线程读取保持计数的计数器。保持为ThreadLocal;缓存在cachedHoldCounter
         */
        static final class HoldCounter {
            int count = 0;
            final long tid = getThreadId(Thread.currentThread());
        }

        /**
         *  ThreadLocal子类。最简单明确定义为反序列化力学。
         *
         */
        static final class ThreadLocalHoldCounter
            extends ThreadLocal<HoldCounter> {
            public HoldCounter initialValue() {
                return new HoldCounter();
            }
        }

        /**
         *  当前线程持有的可重入读锁的数量。仅在构造函数和readObject中初始化。在线程的读取保持计数下降到0时移除。
         */
        private transient ThreadLocalHoldCounter readHolds;

        /**
         * 成功获取readLock的最后一个线程的保持计数。这会在常见的情况下保存ThreadLocal查找,下一个要释放的线程是最后一个要获取的线程。
         * 这是非易失性的,因为它只是用作启发式的,并且对于线程缓存是很好的。
         *  <p>可以延长线程,它正在缓存读取保持计数,但通过不保留对线程的引用来避免垃圾回收。
         *  <p>通过良性数据竞赛访问;依赖于内存模型的最终字段和超薄空气保证。
         */
        private transient HoldCounter cachedHoldCounter;

        /**
         *  firstReader是获取读取锁的第一个线程。 firstReaderHoldCount是firstReader的保持计数。
         *  <p>更精确地说,firstReader是最后一次将共享计数从0更改为1,并且自那以后未释放读锁定的唯一线程; null如果没有这样的线程。
         *  <p>除非线程终止而不放弃其读取锁,否则不能导致垃圾保留,因为tryReleaseShared将其设置为null。
         *  <p>通过良性数据竞赛访问;依赖于内存模型对参考的超薄空气保证。
         *  <p>这允许跟踪非保留读锁的读保持是非常便宜的。
         */
        private transient Thread firstReader = null;
        private transient int firstReaderHoldCount;

        Sync() {
            readHolds = new ThreadLocalHoldCounter();
            setState(getState()); // ensures visibility of readHolds
        }

        /*
         * Acquires and releases use the same code for fair and
         * nonfair locks, but differ in whether/how they allow barging
         * when queues are non-empty.
         * <p>
         *  获取和释放使用相同的代码公平和非公平锁,但不同是否/如何允许驳船时队列非空。
         *
         */

        /**
         * Returns true if the current thread, when trying to acquire
         * the read lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * <p>
         *  返回true如果当前线程,当试图获取读取锁,否则有资格这样做,应该阻止,因为超越其他等待线程的策略。
         *
         */
        abstract boolean readerShouldBlock();

        /**
         * Returns true if the current thread, when trying to acquire
         * the write lock, and otherwise eligible to do so, should block
         * because of policy for overtaking other waiting threads.
         * <p>
         * 返回true,如果当前线程,当试图获取写锁定,否则有资格这样做,应该阻止,因为超越其他等待线程的策略。
         *
         */
        abstract boolean writerShouldBlock();

        /*
         * Note that tryRelease and tryAcquire can be called by
         * Conditions. So it is possible that their arguments contain
         * both read and write holds that are all released during a
         * condition wait and re-established in tryAcquire.
         * <p>
         *  注意tryRelease和tryAcquire可以被条件调用。因此,它们的参数可能包含读取和写入保持,这些都在条件等待期间释放,并在tryAcquire中重新建立。
         *
         */

        protected final boolean tryRelease(int releases) {
            if (!isHeldExclusively())//判断当前锁是否是当前线程持有
                throw new IllegalMonitorStateException();
            int nextc = getState() - releases;
            boolean free = exclusiveCount(nextc) == 0;
            if (free)//重入次数为0时释放锁
                setExclusiveOwnerThread(null);
            //更新state的值
            setState(nextc);
            return free;
        }

        protected final boolean tryAcquire(int acquires) {

            Thread current = Thread.currentThread();
            int c = getState();
            int w = exclusiveCount(c);//获取写锁数量
            //c!=0表示此时有读锁或者写锁被获取
            if (c != 0) {
                //w==0代表此时的读锁数量不为0, 读锁不能升级成为写锁
                if (w == 0 ||
                        //写锁被其他线程占有
                        current != getExclusiveOwnerThread())//锁被其他线程占有
                    //获取锁失败
                    return false;
                //到这说明是重入
                if (w + exclusiveCount(acquires) > MAX_COUNT)//不能超过重入次数最大值(一般不会超过)
                    throw new Error("Maximum lock count exceeded");

                //更新state的值(ReentrantReadWriteLock中state代表锁重入的次数)
                setState(c + acquires);
                return true;
            }
            //到此处说明锁空闲
            if (writerShouldBlock() ||
                !compareAndSetState(c, c + acquires))
                return false;
            //将当前线程绑定该锁
            setExclusiveOwnerThread(current);
            return true;
        }

        protected final boolean tryReleaseShared(int unused) {
            Thread current = Thread.currentThread();
            if (firstReader == current) {
                // assert firstReaderHoldCount > 0;
                if (firstReaderHoldCount == 1)
                    firstReader = null;
                else
                    firstReaderHoldCount--;
            } else {
                HoldCounter rh = cachedHoldCounter;
                if (rh == null || rh.tid != getThreadId(current))
                    rh = readHolds.get();
                int count = rh.count;
                if (count <= 1) {
                    readHolds.remove();
                    if (count <= 0)
                        throw unmatchedUnlockException();
                }
                --rh.count;
            }
            for (;;) {
                int c = getState();
                int nextc = c - SHARED_UNIT;
                if (compareAndSetState(c, nextc))
                    // Releasing the read lock has no effect on readers,
                    // but it may allow waiting writers to proceed if
                    // both read and write locks are now free.
                    return nextc == 0;
            }
        }

        private IllegalMonitorStateException unmatchedUnlockException() {
            return new IllegalMonitorStateException(
                "attempt to unlock read lock, not locked by current thread");
        }

        //获取共享锁
        protected final int tryAcquireShared(int unused) {
            /*
             *  演练：1.如果写锁由另一个线程持有,失败。
             * 2.否则,此线程适合锁定wrt状态,因此请询问是否应该由于队列策略而阻止。如果没有,尝试通过CASing状态和更新计数授予。
             * 注意,步骤不检查可重入获取,其被推迟到完整版本,以避免在更典型的不可重入的情况下检查保持计数。
             * 3.如果步骤2失败,或者因为线程显然不合格,或者CAS失败或计数饱和,则使用完全重试循环链接到版本。
             *
             */
            Thread current = Thread.currentThread();
            int c = getState();

            if (exclusiveCount(c) != 0 //写锁已被获取
                    && getExclusiveOwnerThread() != current)//不是当前线程获取的写锁
                // 获取锁失败
                return -1;
            //到这里说明写锁未被获取或者是当前线程获取的写锁
            int r = sharedCount(c);// c >>> 16  高16位记录的是读锁的重入次数

            // !readerShouldBlock() 根据公平锁和非公平锁的策略来判断是否要阻塞线程;公平锁和写锁一样,如果队列中有节点,那么必须要排队;
            //但是非公平锁和写锁不太一样;写锁直接返回false,即表示不管队列中有哪些节点,队列外的线程都可以抢占写锁;而读锁的非公平锁还是有一定的公平性的;
            //即当队列中首个即将要唤醒的节点是请求写锁的时候,队列外的线程不能抢占锁;如果请求的是读锁,则可以抢占
            if (!readerShouldBlock()
                    //CAS增加读锁的数量
                    && r < 65535 && compareAndSetState(c, c + 65536)) {
                // 如果读锁是空闲的， 获取锁成功(非重入)。
                if (r == 0) {
                    // 将当前线程设置为第一个读锁线程
                    firstReader = current;
                    // 计数器为1
                    firstReaderHoldCount = 1;

                }// 如果读锁不是空闲的，且第一个读线程是当前线程。获取锁成功(重入)。
                else if (firstReader == current) {
                    // 将计数器加一
                    firstReaderHoldCount++;
                    // 如果不是第一个线程，获取锁成功(共享锁)。
                } else {
                    // cachedHoldCounter 代表的是最后一个获取读锁的线程的计数器。
                    HoldCounter rh = cachedHoldCounter;
                    // 如果最后一个线程计数器是 null 或者不是当前线程，那么就新建一个 HoldCounter 对象
                    if (rh == null || rh.tid != getThreadId(current))
                        // 给当前线程新建一个 HoldCounter
                        cachedHoldCounter = rh = readHolds.get();
                        // 如果不是 null，且 count 是 0，就将上个线程的 HoldCounter 覆盖本地的。
                    else if (rh.count == 0)
                        readHolds.set(rh);
                    // 对 count 加一
                    rh.count++;
                }
                return 1;
            }
            // 死循环获取读锁。包含锁降级策略。
            return fullTryAcquireShared(current);
        }

        /**
         *  获取读取的完整版本,用于处理在tryAcquireShared中未处理的CAS未命中和可重入读取。
         *
         */
        final int fullTryAcquireShared(Thread current) {
            /*
             * 这个代码在tryAcquireShared中是部分冗余的,但是通过不使tryAcquireShared
             与重试和延迟读取保持计数之间的交互复杂化而更加简单。
             * 
             */
            HoldCounter rh = null;
            for (;;) {
                int c = getState();
                //写锁被占有
                if (exclusiveCount(c) != 0) {
                    if (getExclusiveOwnerThread() != current)//拥有写锁的不是当前线程
                        return -1;
                } else if (readerShouldBlock()) {
                    // Make sure we're not acquiring read lock reentrantly
                    if (firstReader == current) {
                        // assert firstReaderHoldCount > 0;
                    } else {
                        if (rh == null) {
                            rh = cachedHoldCounter;
                            if (rh == null || rh.tid != getThreadId(current)) {
                                rh = readHolds.get();
                                if (rh.count == 0)
                                    readHolds.remove();
                            }
                        }
                        if (rh.count == 0)
                            return -1;
                    }
                }
                if (sharedCount(c) == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    if (sharedCount(c) == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                    } else {
                        if (rh == null)
                            rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                        cachedHoldCounter = rh; // cache for release
                    }
                    return 1;
                }
            }
        }

        /**
         *  执行tryLock写入,在两种模式下启用驳接。这与tryAcquire的效果相同,除了对writerShouldBlock的调用不足。
         *
         */
        final boolean tryWriteLock() {
            Thread current = Thread.currentThread();
            int c = getState();
            if (c != 0) {
                int w = exclusiveCount(c);
                if (w == 0 || current != getExclusiveOwnerThread())
                    return false;
                if (w == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
            }
            if (!compareAndSetState(c, c + 1))
                return false;
            setExclusiveOwnerThread(current);
            return true;
        }

        /**
         *  执行tryLock读取,在两种模式下启用驳船。这与tryAcquireShared的效果相同,除了缺少对readerShouldBlock的调用。
         */
        final boolean tryReadLock() {
            Thread current = Thread.currentThread();
            for (;;) {
                int c = getState();
                if (exclusiveCount(c) != 0 &&
                    getExclusiveOwnerThread() != current)
                    return false;
                int r = sharedCount(c);
                if (r == MAX_COUNT)
                    throw new Error("Maximum lock count exceeded");
                if (compareAndSetState(c, c + SHARED_UNIT)) {
                    if (r == 0) {
                        firstReader = current;
                        firstReaderHoldCount = 1;
                    } else if (firstReader == current) {
                        firstReaderHoldCount++;
                    } else {
                        HoldCounter rh = cachedHoldCounter;
                        if (rh == null || rh.tid != getThreadId(current))
                            cachedHoldCounter = rh = readHolds.get();
                        else if (rh.count == 0)
                            readHolds.set(rh);
                        rh.count++;
                    }
                    return true;
                }
            }
        }

        protected final boolean isHeldExclusively() {
            return getExclusiveOwnerThread() == Thread.currentThread();
        }

        // Methods relayed to outer class

        final ConditionObject newCondition() {
            return new ConditionObject();
        }

        final Thread getOwner() {
            // Must read state before owner to ensure memory consistency
            return ((exclusiveCount(getState()) == 0) ?
                    null :
                    getExclusiveOwnerThread());
        }

        final int getReadLockCount() {
            return sharedCount(getState());
        }

        final boolean isWriteLocked() {
            return exclusiveCount(getState()) != 0;
        }

        final int getWriteHoldCount() {
            return isHeldExclusively() ? exclusiveCount(getState()) : 0;
        }

        final int getReadHoldCount() {
            if (getReadLockCount() == 0)
                return 0;

            Thread current = Thread.currentThread();
            if (firstReader == current)
                return firstReaderHoldCount;

            HoldCounter rh = cachedHoldCounter;
            if (rh != null && rh.tid == getThreadId(current))
                return rh.count;

            int count = readHolds.get().count;
            if (count == 0) readHolds.remove();
            return count;
        }

        /**
         *  从流重构实例(即,反序列化它)。
         */
        private void readObject(java.io.ObjectInputStream s)
            throws java.io.IOException, ClassNotFoundException {
            s.defaultReadObject();
            readHolds = new ThreadLocalHoldCounter();
            setState(0); // reset to unlocked state
        }

        final int getCount() { return getState(); }
    }

    /**
     * Nonfair version of Sync
     * <p>
     *  非法版本的同步
     *
     */
    static final class NonfairSync extends Sync {
        private static final long serialVersionUID = -8159625535654395037L;
        final boolean writerShouldBlock() {
            return false; // writers can always barge
        }
        final boolean readerShouldBlock() {
            /*
             *  阻塞如果暂时显示为队列头的线程(如果存在)是等待写入器。这只是一个概率效应,
             * 因为如果在其他已启用的读取器之后还有一个等待写入器尚未从队列中耗尽,新读取器将不会阻塞。
             */
            return apparentlyFirstQueuedIsExclusive();
        }
    }

    /**
     * Fair version of Sync
     * <p>
     *  公平版的同步
     *
     */
    static final class FairSync extends Sync {
        private static final long serialVersionUID = -2274990926593161451L;
        final boolean writerShouldBlock() {
            return hasQueuedPredecessors();
        }
        final boolean readerShouldBlock() {
            return hasQueuedPredecessors();
        }
    }

    /**
     * The lock returned by method {@link ReentrantReadWriteLock#readLock}.
     * <p>
     *  由方法{@link ReentrantReadWriteLock#readLock}返回的锁。
     *
     */
    public static class ReadLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -5992448646407690164L;
        private final Sync sync;

        /**
         * Constructor for use by subclasses
         *
         * <p>
         *  子类使用的构造方法
         *
         *
         * @param lock the outer lock object
         * @throws NullPointerException if the lock is null
         */
        protected ReadLock(ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         * Acquires the read lock.
         *
         * <p>Acquires the read lock if the write lock is not held by
         * another thread and returns immediately.
         *
         * <p>If the write lock is held by another thread then
         * the current thread becomes disabled for thread scheduling
         * purposes and lies dormant until the read lock has been acquired.
         * <p>
         *  获取读锁。
         *
         *  <p>如果写锁没有被另一个线程占用并立即返回,则获取读锁。
         *
         *  <p>如果写锁定由另一个线程保持,则当前线程变为禁用以用于线程调度目的,并且处于休眠状态直到获取读锁定。
         *
         */
        public void lock() {
            sync.acquireShared(1);
        }

        /**
         *  获取读锁定,除非当前线程是{@linkplain Thread#interrupt interrupted}。
         *  <p>如果写锁没有被另一个线程占用并立即返回,则获取读锁。
         * <p>如果写锁定由另一个线程持有,那么当前线程变为禁用以用于线程调度目的,并处于休眠状态,直到发生以下两种情况之一：
         *  <li>读取锁定由当前线程获取;要么
         *  <li>一些其他线程{@linkplain线程#中断中断}当前线程。
         *  <p>如果当前线程：
         *  <li>在进入此方法时设置了中断状态;要么
         *  <li>是{@linkplain线程#中断}获取读取锁时,
         *  那么将抛出{@link InterruptedException},并清除当前线程的中断状态。
         *  在该实现中,因为该方法是显式中断点,所以优选在正常或可重入获取锁时响应中断。
         */
        public void lockInterruptibly() throws InterruptedException {
            sync.acquireSharedInterruptibly(1);
        }

        /**
         *  仅当写锁定在调用时未被另一个线程占用时才获取读锁定。
         * <p>如果写锁定未被另一个线程占用,并立即返回值{@code true},则获取读锁定。
         * 即使此锁已设置为使用公平的排序策略,调用{@code tryLock()} <em>将立即获取读取锁(如果可用),无论其他线程是否正在等待为读锁。
         * 这种"驳船"行为在某些情况下可能是有用的,即使它破坏公平。
         *  <p>如果写锁定由另一个线程持有,那么此方法将立即返回值{@code false}。
         */
        public boolean tryLock() {
            return sync.tryReadLock();
        }

        /**
         *  如果在给定的等待时间内写锁没有被另一个线程占用,并且当前线程尚未{@linkplain线程#中断中断},则获取读锁。
         *  <p>如果写锁定未被另一个线程占用,并立即返回值{@code true},则获取读锁定。如果这个锁被设置为使用公平的排序策略,则如果任何其他线程正在等待锁,则不会获取可用的锁<em>。
         * 这与{@link #tryLock()}方法形成对比。如果你想要一个定时的{@code tryLock}允许在公平锁定上驳船,然后结合定时和非定时形式在一起：。
         *  <pre> {@code if(lock.tryLock()|| lock.tryLock(timeout,unit)){...}} </pre>
         * <p>如果写锁定由另一个线程持有,则当前线程变为禁用以用于线程调度目的,并且处于休眠状态,直到发生以下三种情况之一：
         *  <li>读取锁定由当前线程获取;要么
         *  <li>一些其他线程{@linkplain线程#中断中断}当前线程;要么
         *  <li>经过指定的等待时间。
         *  <p>如果获取了读取锁,则返回值{@code true}。
         *  <p>如果当前线程：
         *  <li>在进入此方法时设置了中断状态;要么
         *  <li>是{@linkplain线程#中断}获取读取锁时,
         *  </ul>,然后{@link InterruptedException}被抛出,当前线程的中断状态被清除。
         *  <p>如果经过指定的等待时间,则返回值{@code false}。如果时间小于或等于零,则该方法将不会等待。
         *  <p>在该实现中,由于该方法是明确的中断点,所以优选在正常或可重入获取锁的情况下响应于中断,并且超过报告等待时间的流逝。
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireSharedNanos(1, unit.toNanos(timeout));
        }

        /**
         *  尝试释放此锁定。
         *  <p>如果读取器的数量现在为零,则锁可用于写锁尝试。
         */
        public void unlock() {
            sync.releaseShared(1);
        }

        public Condition newCondition() {
            throw new UnsupportedOperationException();
        }

        /**
         * 返回标识此锁的字符串及其锁状态。括号中的状态包括字符串{@code"Read locks ="},后跟保持的读取锁的数目。
         */
        public String toString() {
            int r = sync.getReadLockCount();
            return super.toString() +
                "[Read locks = " + r + "]";
        }
    }

    public static class WriteLock implements Lock, java.io.Serializable {
        private static final long serialVersionUID = -4992448646407690164L;
        private final Sync sync;

        protected WriteLock(ReentrantReadWriteLock lock) {
            sync = lock.sync;
        }

        /**
         *  获取写锁定。
         *  <p>如果读取和写入锁定都未被另一个线程占用,并立即返回,将写入锁定保持计数设置为1,则获取写入锁定。
         *  <p>如果当前线程已经持有写锁定,则保持计数增加1,并且该方法立即返回。
         *  <p>如果锁由另一个线程保持,则当前线程变为禁用,以用于线程调度目的,并且在获取写锁定之前处于休眠状态,此时写锁定保持计数设置为1。
         */
        public void lock() {
            sync.acquire(1);
        }

        /**
         *  获取写锁定,除非当前线程为{@linkplain Thread#interrupt interrupted}。
         *
         *  <p>如果读取和写入锁定都未被另一个线程占用,并立即返回,将写入锁定保持计数设置为1,则获取写入锁定。
         *
         *  <p>如果当前线程已经持有此锁,则保持计数增加1,并且该方法立即返回。
         *
         *  <p>如果锁由另一个线程持有,那么当前线程将变为禁用以进行线程调度,并处于休眠状态,直到发生以下两种情况之一：
         *  <li>写锁定由当前线程获取;要么
         * <li>一些其他线程{@linkplain线程#中断中断}当前线程。
         *  <p>如果写锁定由当前线程获取,则锁定保持计数设置为1。
         *  <p>如果当前线程：
         *  <li>在进入此方法时设置了中断状态;要么
         *  <li>是{@linkplain线程#中断}获取写锁定时,
         *  那么将抛出{@link InterruptedException},并清除当前线程的中断状态。
         *  在该实现中,因为该方法是显式中断点,所以优选在正常或可重入获取锁时响应中断。
         */
        public void lockInterruptibly() throws InterruptedException {
            sync.acquireInterruptibly(1);
        }

        /**
         *  获取写锁定,只有在调用时它没有被另一个线程占用。
         *
         *  <p>如果读取和写入锁定都未被另一个线程占用,则获取写入锁定,并立即返回值{@code true},将写入锁定保持计数设置为1。
         * 即使此锁已设置为使用公平的排序策略,调用{@code tryLock()} <em>将立即获取锁,如果可用,无论是否其他线程当前正在等待写锁。这种"驳船"行为在某些情况下可能是有用的,即使它破坏公平。
         *
         * <p>如果当前线程已经持有此锁,则保持计数增加1,并且该方法返回{@code true}。
         *
         *  <p>如果锁由另一个线程持有,那么此方法将立即返回值{@code false}。

         */
        public boolean tryLock( ) {
            return sync.tryWriteLock();
        }

        /**
         *  如果在给定的等待时间内没有被另一个线程占用,并且当前线程尚未{@linkplain线程#中断中断},则获取写锁定。
         *
         *  <p>如果读取和写入锁定都未被另一个线程占用,则获取写入锁定,并立即返回值{@code true},将写入锁定保持计数设置为1。
         * 如果该锁被设置为使用公平的排序策略,则如果任何其他线程正在等待写锁定,则不会获取可用的锁</em>。这与{@link #tryLock()}方法形成对比。
         * 如果你想要一个定时的{@code tryLock}允许在公平锁定上驳船,然后结合定时和非定时形式在一起：。
         *
         *  <pre> {@code if(lock.tryLock()|| lock.tryLock(timeout,unit)){...}} </pre>
         *
         *  <p>如果当前线程已经持有此锁,则保持计数增加1,并且该方法返回{@code true}。
         *
         *  <p>如果锁由另一个线程持有,那么当前线程将被禁用以进行线程调度,并处于休眠状态,直到发生以下三种情况之一：
         *
         * <ul>
         *
         *  <li>写锁定由当前线程获取;要么
         *
         *  <li>一些其他线程{@linkplain线程#中断中断}当前线程;要么
         *
         *  <li>经过指定的等待时间
         *
         * </ul>
         *
         * <p>如果获得写锁定,则返回值{@code true},写锁定保持计数设置为1。
         *
         *  <p>如果当前线程：
         *
         * <ul>
         *
         *  <li>在进入此方法时设置了中断状态;要么
         *
         *  <li>是{@linkplain线程#中断}获取写锁定时,
         *
         * </ul>
         *
         *  那么将抛出{@link InterruptedException},并清除当前线程的中断状态。
         *
         *  <p>如果经过指定的等待时间,则返回值{@code false}。如果时间小于或等于零,则该方法将不会等待。
         *
         *  <p>在该实现中,由于该方法是明确的中断点,所以优选在正常或可重入获取锁的情况下响应于中断,并且超过报告等待时间的流逝。
         */
        public boolean tryLock(long timeout, TimeUnit unit)
                throws InterruptedException {
            return sync.tryAcquireNanos(1, unit.toNanos(timeout));
        }

        /**
         *  尝试释放此锁定。
         *  <p>如果当前线程是这个锁的持有者,则保持计数递减。如果保持计数现在为零,则锁定被释放。
         * 如果当前线程不是这个锁的持有者,那么会抛出{@link IllegalMonitorStateException}。
         */
        public void unlock() {
            sync.release(1);
        }

        /**
         *  返回与此{@link Lock}实例一起使用的{@link Condition}实例。
         * 和{@link Object#notifyAll notifyAll})当与内置的监视器锁一起使用时。
         *  返回与此{@link Lock}实例一起使用的{@link Condition}实例。
         *
         * <ul>
         *
         * <li>如果在调用任何{@link Condition}方法时未锁定此写锁定,则会抛出{@link IllegalMonitorStateException}。
         *  (读锁与写锁无关,因此不会被检查或影响,但是当当前线程也获取了读锁时,调用条件等待方法本质上总是一个错误,因为可以解锁它的其他线程将不会能够获得写锁定。
         *
         *
         *  <li>如果在等待时线程{@linkplain线程#中断},则等待将终止,将抛出一个{@link InterruptedException},并且线程的中断状态将被清除。
         *
         *  <li>等待线程以FIFO顺序发出信号。
         *
         *  <li>从等待方法返回的线程的锁重新捕获的排序与初始获取锁的线程相同,这是默认情况下未指定的,但是<em> </em>锁优先考虑那些具有等待时间最长。
         *
         * </ul>
         *
         *
         * @return the Condition object
         */
        public Condition newCondition() {
            return sync.newCondition();
        }

        /**
         * Returns a string identifying this lock, as well as its lock
         * state.  The state, in brackets includes either the String
         * {@code "Unlocked"} or the String {@code "Locked by"}
         * followed by the {@linkplain Thread#getName name} of the owning thread.
         *
         * <p>
         *  返回标识此锁的字符串及其锁状态。
         * 括号中的状态包括字符串{@code"Unlocked"}或String {@code"Locked by"},后跟拥有线程的{@linkplain Thread#getName name}。
         *
         *
         * @return a string identifying this lock, as well as its lock state
         */
        public String toString() {
            Thread o = sync.getOwner();
            return super.toString() + ((o == null) ?
                                       "[Unlocked]" :
                                       "[Locked by thread " + o.getName() + "]");
        }

        /**
         * Queries if this write lock is held by the current thread.
         * Identical in effect to {@link
         * ReentrantReadWriteLock#isWriteLockedByCurrentThread}.
         *
         * <p>
         * 查询此写锁是否由当前线程持有。与{@link ReentrantReadWriteLock#isWriteLockedByCurrentThread}效果相同。
         *
         *
         * @return {@code true} if the current thread holds this lock and
         *         {@code false} otherwise
         * @since 1.6
         */
        public boolean isHeldByCurrentThread() {
            return sync.isHeldExclusively();
        }

        /**
         *  查询当前线程对此写锁定的保持数。线程对于每个锁定动作都有一个锁定的保持,它不与解锁动作匹配。
         * 与{@link ReentrantReadWriteLock#getWriteHoldCount}效果相同。
         */
        public int getHoldCount() {
            return sync.getWriteHoldCount();
        }
    }

    // Instrumentation and status

    /**
     *  如果此锁定的公平性设置为true,则返回{@code true}。
     */
    public final boolean isFair() {
        return sync instanceof FairSync;
    }

    /**
     *  返回当前拥有写锁定的线程,如果不拥有则返回{@code null}。当此方法由不是所有者的线程调用时,返回值反映当前锁状态的尽力而为近似值。
     * 例如,所有者可能会暂时{@code null},即使有线程试图获取锁,但尚未这样做。该方法被设计为便于构建提供更广泛的锁监视设施的子类。
     */
    protected Thread getOwner() {
        return sync.getOwner();
    }

    /**
     *  查询此锁所持有的读锁数。此方法设计用于监视系统状态,而不是用于同步控制。
     */
    public int getReadLockCount() {
        return sync.getReadLockCount();
    }

    /**
     *  查询写锁是否由任何线程持有。此方法设计用于监视系统状态,而不是用于同步控制。
     */
    public boolean isWriteLocked() {
        return sync.isWriteLocked();
    }

    /**
     *  查询写锁是否由当前线程持有。
     */
    public boolean isWriteLockedByCurrentThread() {
        return sync.isHeldExclusively();
    }

    /**
     *  查询当前线程对此锁的可重入写保持数。写线程对于不与解锁动作匹配的每个锁定动作保持锁定。
     */
    public int getWriteHoldCount() {
        return sync.getWriteHoldCount();
    }

    /**
     * 查询当前线程对此锁定的可重入读取保持数。读取器线程对于不与解锁动作匹配的每个锁定动作保持锁定。
     */
    public int getReadHoldCount() {
        return sync.getReadHoldCount();
    }

    /**
     *  返回包含可能正在等待获取写锁定的线程的集合。因为实际的线程集合可能在构造此结果时动态地改变,所返回的集合仅是最大努力估计。返回的集合的元素没有特定的顺序。
     * 该方法被设计为便于构建提供更广泛的锁监视设施的子类。
     *
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedWriterThreads() {
        return sync.getExclusiveQueuedThreads();
    }

    /**
     *  返回包含可能正在等待获取读取锁的线程的集合。因为实际的线程集合可能在构造此结果时动态地改变,所返回的集合仅是最大努力估计。返回的集合的元素没有特定的顺序。
     * 该方法被设计为便于构建提供更广泛的锁监视设施的子类。

     */
    protected Collection<Thread> getQueuedReaderThreads() {
        return sync.getSharedQueuedThreads();
    }

    /**
     *  查询任何线程是否正在等待获取读取或写入锁定。注意,因为取消可能在任何时候发生,{@code true}返回不保证任何其他线程将获得锁。该方法主要用于监视系统状态。
     */
    public final boolean hasQueuedThreads() {
        return sync.hasQueuedThreads();
    }

    /**
     * 查询给定线程是否正在等待获取读取或写入锁定。注意,因为取消可能在任何时候发生,{@code true}返回不保证此线程将获得锁。该方法主要用于监视系统状态。
     */
    public final boolean hasQueuedThread(Thread thread) {
        return sync.isQueued(thread);
    }

    /**
     *  返回等待获取读取或写入锁定的线程数的估计值。该值只是一个估计值,因为线程的数量可能会在此方法遍历内部数据结构时动态更改。此方法设计用于监视系统状态,而不是用于同步控制。

     */
    public final int getQueueLength() {
        return sync.getQueueLength();
    }

    /**
     *  返回包含可能正在等待获取读取或写入锁定的线程的集合。因为实际的线程集合可能在构造此结果时动态地改变,所返回的集合仅是最大努力估计。返回的集合的元素没有特定的顺序。
     * 该方法被设计为便于构建提供更广泛的监测设施的子类。
     *
     *
     * @return the collection of threads
     */
    protected Collection<Thread> getQueuedThreads() {
        return sync.getQueuedThreads();
    }

    /**
     * Queries whether any threads are waiting on the given condition
     * associated with the write lock. Note that because timeouts and
     * interrupts may occur at any time, a {@code true} return does
     * not guarantee that a future {@code signal} will awaken any
     * threads.  This method is designed primarily for use in
     * monitoring of the system state.
     *
     * <p>
     *  查询任何线程是否正在等待与写锁定相关联的给定条件。注意,因为超时和中断可能在任何时候发生,{@code true}返回不保证未来的{@code signal}将唤醒任何线程。
     * 该方法主要用于监视系统状态。
     *
     *
     * @param condition the condition
     * @return {@code true} if there are any waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public boolean hasWaiters(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.hasWaiters((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns an estimate of the number of threads waiting on the
     * given condition associated with the write lock. Note that because
     * timeouts and interrupts may occur at any time, the estimate
     * serves only as an upper bound on the actual number of waiters.
     * This method is designed for use in monitoring of the system
     * state, not for synchronization control.
     *
     * <p>
     * 返回等待与写锁定相关联的给定条件的线程数的估计。注意,因为超时和中断可以在任何时间发生,所以估计仅用作实际数量的服务者的上限。此方法设计用于监视系统状态,而不是用于同步控制。
     *
     *
     * @param condition the condition
     * @return the estimated number of waiting threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    public int getWaitQueueLength(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitQueueLength((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a collection containing those threads that may be
     * waiting on the given condition associated with the write lock.
     * Because the actual set of threads may change dynamically while
     * constructing this result, the returned collection is only a
     * best-effort estimate. The elements of the returned collection
     * are in no particular order.  This method is designed to
     * facilitate construction of subclasses that provide more
     * extensive condition monitoring facilities.
     *
     * <p>
     *  返回包含可能在与写锁定相关联的给定条件下等待的线程的集合。因为实际的线程集合可能在构造此结果时动态地改变,所返回的集合仅是最大努力估计。返回的集合的元素没有特定的顺序。
     * 该方法被设计为便于构建提供更广泛的状态监测设施的子类。
     *
     *
     * @param condition the condition
     * @return the collection of threads
     * @throws IllegalMonitorStateException if this lock is not held
     * @throws IllegalArgumentException if the given condition is
     *         not associated with this lock
     * @throws NullPointerException if the condition is null
     */
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        if (condition == null)
            throw new NullPointerException();
        if (!(condition instanceof AbstractQueuedSynchronizer.ConditionObject))
            throw new IllegalArgumentException("not owner");
        return sync.getWaitingThreads((AbstractQueuedSynchronizer.ConditionObject)condition);
    }

    /**
     * Returns a string identifying this lock, as well as its lock state.
     * The state, in brackets, includes the String {@code "Write locks ="}
     * followed by the number of reentrantly held write locks, and the
     * String {@code "Read locks ="} followed by the number of held
     * read locks.
     *
     * <p>
     *  返回标识此锁的字符串及其锁状态。
     * 括号中的状态包括字符串{@code"Write locks ="},后跟可重复保持的写锁的数目,以及String {@code"Read locks ="},后跟保持的读锁的数目。
     *
     *
     * @return a string identifying this lock, as well as its lock state
     */
    public String toString() {
        int c = sync.getCount();
        int w = Sync.exclusiveCount(c);
        int r = Sync.sharedCount(c);

        return super.toString() +
            "[Write locks = " + w + ", Read locks = " + r + "]";
    }

    /**
     * Returns the thread id for the given thread.  We must access
     * this directly rather than via method Thread.getId() because
     * getId() is not final, and has been known to be overridden in
     * ways that do not preserve unique mappings.
     * <p>
     */
    static final long getThreadId(Thread thread) {
        return UNSAFE.getLongVolatile(thread, TID_OFFSET);
    }

    // Unsafe mechanics
    private static final sun.misc.Unsafe UNSAFE;
    private static final long TID_OFFSET;
    static {
        try {
            UNSAFE = sun.misc.Unsafe.getUnsafe();
            Class<?> tk = Thread.class;
            TID_OFFSET = UNSAFE.objectFieldOffset
                (tk.getDeclaredField("tid"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

}
